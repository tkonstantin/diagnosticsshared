function visibleIframeStyle() {
    return "position:fixed; width:100%; height:100%; padding:0; margin:0; border:none; top:0px; left:0px; right:0px; bottom:0px; background-position:no-repeat; scrolling:no; overflow: hidden;";
}

function invisibleIframeStyle() {
    return "position:fixed; width:0; height:0; padding:0; margin:0; border:none; top:0px; left:0px; right: 0px; bottom: 0px; background-position:no-repeat; scrolling:no; overflow: hidden;";
}

function applyCurrentRules(id, code, min, max) {
    var frame = document.getElementsByName(id)[0];
    if (frame && frame.contentWindow.Answer_setCheckRulesData) {
        frame.contentWindow.Answer_setCheckRulesData({ AllowSymbolsCode: code, AnswerMinLength: min, AnswerMaxLength: max})
    }
}

function addMobileHelper(id, callback) {
    var doc = document.getElementsByName(id)[0].contentWindow.document
    var script_tag = doc.createElement("script");
	script_tag.onload = callback;
    script_tag.type = 'text/javascript';
    script_tag.src = '../../scriptsapp/mobilehelper.js';
    doc.head.appendChild(script_tag);
}

function setOnloadHandler(id) {
    document.getElementsByName(id)[0].onload =
    function() {
        window.webkit.messageHandlers.iFrameLoadingHandler.postMessage({"id":id});
    }
}

function setOnErrorHandler(id) {
    document.getElementsByName(id)[0].onerror =
    function() {
        window.webkit.messageHandlers.iFrameErrorHandler.postMessage({"id": id})
    }
}

function setIframeSrc(id, link) {
    document.getElementsByName(id)[0].src = link;
}

function getAllFrames() {
    var allElements = document.getElementsByTagName('iframe')
    var allNames = [];
    for (var i = 0, n = allElements.length; i < n; ++i) {
        var el = allElements[i];
        if (el.name) { allNames.push(el.name); }
    }
    return allNames
}

function hideIframes() {
    Array.prototype.slice.call(document.getElementsByTagName('iframe')).forEach(function(item) {
        item.style = invisibleIframeStyle();
    });
}

function showIframe(id) {
    document.getElementsByName(id).forEach(function(item){
           item.style = visibleIframeStyle();
    });
}


function getIframe(id, isVisible) {
    var styleString = "";
    if (isVisible) {
        styleString = visibleIframeStyle();
    } else {
        styleString = invisibleIframeStyle();
    }
    return "<iframe src='about:blank' name=" + id + " id=" + id + " style='" + styleString + "'></iframe>\n"
}


function loadFrames(src, sBinaryParam) {
    
    var sDecodedParam = window.atob(sBinaryParam);
    var answers = JSON.parse(sDecodedParam);
    
    window.webkit.messageHandlers.iFrameBeginLoadProcess.postMessage({});
    
    function loadProcess(answer) {
        if (!answer)
            answer = answers[0];
        var id = answer.TaskTextHtmlId;
        var nextAnswer = answers[answers.indexOf(answer) + 1];
        
        var element = document.getElementById(id);
        if (element) {
            document.body.removeChild(element);
        }
        
        var f = document.createElement('iframe');
        f.onload = function () {
            f.onload = function () {
                console.log(id + ' reloaded!');
                addMobileHelper(id, function () {
                                if (f.contentWindow.mobileHelper && typeof f.contentWindow.mobileHelper.onload === 'function') {
                                f.contentWindow.mobileHelper.onload(answer);
                                }
                                });
                
                window.webkit.messageHandlers.iFrameLoadCallback.postMessage({ "id": id });
            };
            console.log(document.cookie);
            console.log(id + ' loaded!');
            addMobileHelper(id, function () {
                            if (f.contentWindow.mobileHelper && typeof f.contentWindow.mobileHelper.onload === 'function') {
                            f.contentWindow.mobileHelper.onload(answer);
                            }
                            });
            
            if (f.contentWindow.document.body.innerHTML.length > 0) {
                window.webkit.messageHandlers.iFrameLoadCallback.postMessage({ "id": id });
            } else {
                window.webkit.messageHandlers.iFrameErrorHandler.postMessage({ "id": id });
            }
            
            if (nextAnswer)
                loadProcess(nextAnswer);
        };
        f.onerror = function (e) {
            window.webkit.messageHandlers.iFrameErrorHandler.postMessage({ "error": e, "id": id });
            console.error(e);
        };
        //set attrs
        f.setAttribute('style',
                       'position:fixed; width:0; height:0; padding:0; margin:0; border:none; top:0px; left:0px; right: 0px; bottom: 0px; background-position:no-repeat; scrolling:no; overflow: hidden;');
        f.name = id;
        f.id = id;
        f.src = src + id;
        console.log('!!! f.src = ' + src + id);
        
        document.body.append(f);
    }
    
    loadProcess();
}


function iframeErrorCallback() {
    window.webkit.messageHandlers.iFrameErrorHandler.postMessage({});
}

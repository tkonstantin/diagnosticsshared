//
//  Alerts.swift
//  Diagnostics
//
//  Created by Константин on 30/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

func showAlert(title: String?=nil, body: String?=nil, needsOKAction: Bool = true, from: UIViewController?=nil, completion: (()->Void)?=nil) -> UIAlertController {
    let alert = UIAlertController(title: title, message: body, preferredStyle: .alert)
    if needsOKAction {
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            completion?()
        }))
    }
    UIApplication.presentOnTop(vc: alert, animated: false, completion: {
        if !needsOKAction {
            completion?()
        }
    })
    return alert
}

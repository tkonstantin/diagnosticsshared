//
//  UIApplication+Extension.swift
//  Diagnostics
//
//  Created by Константин on 16.04.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit


extension UIApplication {
   
    //PK
    //
    func goToResults() {
        DiagnosticsShared.instance.delegate?.diagnostics_finishWorkWithExamWorkId(with: SessionService.shared.currentExamWork?.id)
    }
    //
    /* //OLD
    func goToResults() {
        if ConnectionService.shared.hasConnection {
            DiagnosticsShared.instance.delegate?.diagnostics_shouldShowHUD(with: "Загрузка результатов...", progress: nil)
            ResultsHTMLViewController.makeOne { resultsController in
                guard let resultsController = resultsController else {
                    DiagnosticsShared.instance.delegate?.diagnostics_shouldChangeRoot(to: ResultsPlaceholderViewController.makeOne())
                    return;
                }
                DiagnosticsShared.instance.delegate?.diagnostics_shouldChangeRoot(to: resultsController)

            }
        } else {
            DiagnosticsShared.instance.delegate?.diagnostics_shouldChangeRoot(to: ResultsPlaceholderViewController.makeOne())
        }
    }
    */
    
}

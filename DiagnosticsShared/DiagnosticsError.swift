//
//  DiagnosticsError.swift
//  DiagnosticsShared
//
//  Created by Константин on 14/03/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation

public struct DiagnosticsError: Error {
    
    var customDescription: String
    
    var localizedDescription: String {
        get {
            return customDescription
        }
    }
}

//
//  DiagnosticsShared.h
//  DiagnosticsShared
//
//  Created by Константин on 14/03/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for DiagnosticsShared.
FOUNDATION_EXPORT double DiagnosticsSharedVersionNumber;

//! Project version string for DiagnosticsShared.
FOUNDATION_EXPORT const unsigned char DiagnosticsSharedVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DiagnosticsShared/PublicHeader.h>

#import "IINK.h"
#import "MyCertificate.h"
#import "EditorViewController.h"
#import "NSFileManager+Additions.h"

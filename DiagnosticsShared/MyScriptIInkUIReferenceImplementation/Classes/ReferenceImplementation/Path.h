// Copyright MyScript. All right reserved.

#import "IINKIPath.h"
#import <UIKit/UIKit.h>

@interface Path : NSObject <IINKIPath>

@property (strong, nonatomic) UIBezierPath *bezierPath;

@end

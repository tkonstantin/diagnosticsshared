//
//  DiagnosticsShared.swift
//  DiagnosticsShared
//
//  Created by Константин on 14/03/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation


public class DiagnosticsShared {
    
    static let bundle = Bundle(identifier: "com.kostantin-cherkashin.DiagnosticsShared")
    
    weak var delegate: DiagnosticsSharedDelegate?
    
    static let instance = DiagnosticsShared()
    private(set) var commonBaseUrl: String!
    private(set) var base_url: String!
    private var isConfigured = false
    private var variantID: String!
    
    //
    public class func setDelegate(_ delegate: DiagnosticsSharedDelegate?) {
        instance.delegate = delegate
    }
    
    public class func setBaseURL(_ baseURL: String) throws {
       instance.commonBaseUrl = baseURL
    }
    
    ///baseURL should look like "http://185.15.175.210/api/mobile/v1.0/"
    public class func set(baseURL: String) throws {
        guard URL(string: baseURL) != nil else { throw DiagnosticsError(customDescription: "baseURL is invalid") }

        instance.base_url = baseURL
    }
    
    ///setting cookies and DiagnosticsSharedDelegate
    public class func configure(cookies: [HTTPCookie]=[], delegate _delegate: DiagnosticsSharedDelegate?) throws {
        guard instance.base_url != nil else { throw DiagnosticsError(customDescription: "baseURL isn't set yet. Use 'set(baseURL...'") }
        
        instance.delegate = _delegate
        
        if cookies.count != 0 {
            _ = Server.shared.setCookie(cookie: cookies, for: Server.url_paths.baseURL)
        }
        
        instance.isConfigured = true
    }
    
    public class func getCookie() throws -> [HTTPCookie]? {
        guard instance.base_url != nil else { throw DiagnosticsError(customDescription: "baseURL isn't set yet. Use 'set(baseURL...'") }
        
        return Server.shared.getCookie(forURL: Server.url_paths.baseURL)
    }
    
    public class func deleteCookie() throws {
        guard instance.base_url != nil else { throw DiagnosticsError(customDescription: "baseURL isn't set yet. Use 'set(baseURL...'") }
        
        Server.shared.deleteCookie(forURL: Server.url_paths.baseURL)
    }
    
    ///applies variant id and starts loading | loadingProgress = (progressValue: CGFloat, isLoaded: Bool, isFailed: Bool)
    public class func runTest(variantID id: String, loadingProgress: ((CGFloat, Bool, Bool)->Void)?=nil, completion: @escaping (Bool)->Void) throws {
        SessionService.shared.clearCurrentTest()
        let testVC = TestViewController_New.make()
        let _ = DiagnosticsShared.instance.delegate?.diagnostics_shouldChangeRoot(to: testVC)
        testVC.view.isHidden = true
        //PK - debug
        //testVC.view.isHidden = false
        //testVC.webSuperview.backgroundColor = UIColor.yellow
        
        guard instance.isConfigured else { throw DiagnosticsError(customDescription: "DiagnosticsShared isn't configured yet. Use 'configure(cookies...'") }
        guard !id.isEmpty else { throw DiagnosticsError(customDescription: "variantID cannnot be empty") }
        
        if let progress = loadingProgress {
            testVC.webSuperview.subcsribeForLoading(progress)
        }
        
        instance.variantID = id
        
        Server.shared.applyVariant(with: id, completion: { success in
            if success {
                createExamWorkManually {
                    testVC.webSuperview.startLoadingWebView(test: SessionService.shared.currentTest, completion: {
                        completion(success)
                    })
                }
            } else {
                completion(success)
            }
        })
    }
    
    public class func continueTest(examWorkId: String, loadingProgress: ((CGFloat, Bool, Bool)->Void)?=nil, completion: @escaping (Bool)->Void) throws {
        SessionService.shared.clearCurrentTest()
        let testVC = TestViewController_New.make()
        let _ = DiagnosticsShared.instance.delegate?.diagnostics_shouldChangeRoot(to: testVC)
        testVC.view.isHidden = true
        
        guard instance.isConfigured else { throw DiagnosticsError(customDescription: "DiagnosticsShared isn't configured yet. Use 'configure(cookies...'") }
        
        if let progress = loadingProgress {
            testVC.webSuperview.subcsribeForLoading(progress)
        }
        
        continueExamWork(examWorkId) {
            testVC.webSuperview.startLoadingWebView(test: SessionService.shared.currentTest, completion: {
                completion(true)
            })
        }
    }
    
    ///for usage in 'Диагностика' app only
    public class func subcsribeForTestLoading(_ completion: @escaping (CGFloat, Bool, Bool)->Void) {
        //TODO: PK - нужно переделывать
        //WebSuperview.subcsribeForLoading(completion)
    }
    
    ///for usage in 'Диагностика' app only
    public class func createExamWorkManually(completion: (()->Void)?=nil) {
        instance.delegate?.diagnostics_shouldShowHUD(with: nil, progress: nil)
        
        Server.shared.newExamWork { (success) in
            guard success else {
                instance.delegate?.diagnostics_shouldHideHUD()
                _ = showAlert(title: "Не удалось начать работу", body: "Попробуйте начать сначала", needsOKAction: true, completion: {
                    WebSuperview.resetOnLogoutNotification()
                    ConnectionService.shared.logout()
                    SessionService.shared.logout()
                    instance.delegate?.diagnostics_completed()
                })
                completion?()
                return;
            }
            SessionService.shared.currentTest.setStarted()
            completion?()
        }
    }
    
    public class func continueExamWork(_ examWorkId: String, completion: (()->Void)?=nil) {
        Server.shared.loadExamWork(workID: examWorkId) { (success) in
            guard success else {
                instance.delegate?.diagnostics_shouldHideHUD()
                _ = showAlert(title: "Не удалось начать работу", body: "Попробуйте начать сначала", needsOKAction: true, completion: {
                    WebSuperview.resetOnLogoutNotification()
                    ConnectionService.shared.logout()
                    SessionService.shared.logout()
                    instance.delegate?.diagnostics_completed()
                })
                completion?()
                return;
            }
            SessionService.shared.currentTest.setStarted()
            completion?()
        }
    }
    
    //PK: MyScript
    public class func runMyScript() {
        
        let myscriptVC = TestMyScriptViewController.make()
        let _ = DiagnosticsShared.instance.delegate?.diagnostics_shouldChangeRoot(to: myscriptVC)
        
    }
    
    
}

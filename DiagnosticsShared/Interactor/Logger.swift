//
//  Logger.swift
//  Diagnostics
//
//  Created by Константин on 20/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import Alamofire


func printAndLog(_ items: Any?..., isError: Bool = true) {
//#if DEBUG
    var str = String()
    for item in items {
        if item is String {
            str += (item as! String + "\n")
        }
    }
    printAndLog(str)
//#endif
}

func printAndLog(_ str: String) {
    printDiagnosticsShared2(str)
}

func printDiagnosticsShared2(_ str: String, isError: Bool = true) {
 //#if DEBUG
    print(str)

    let dir = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory,
                                       in: FileManager.SearchPathDomainMask.userDomainMask).last! as NSURL
    if let fileurl =  dir.appendingPathComponent("DiagnosticsSharedLog.txt") {
        let string = "\(NSDate()): \(str)\n"
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)!
        
        if FileManager.default.fileExists(atPath: fileurl.path) {
            do {
                let fileHandle = try FileHandle(forWritingTo: fileurl)
                fileHandle.seekToEndOfFile()
                fileHandle.write(data)
                fileHandle.closeFile()
            } catch let err as NSError {
                print("Can't open fileHandle \(err)")
            }
        }
        else {
            do {
                try data.write(to: fileurl, options: Data.WritingOptions.atomicWrite)
            } catch let err as NSError  {
                print("Can't write \(err)")
            }
        }
    }
//#endif
}

extension DataResponse {
    func log() {
        
        printAndLog(
            "url: " + (self.request?.url?.absoluteString ?? "request url lost"),
            "statusCode: " + {
                if let value = self.response?.statusCode { return String(value) } else { return "request url lost" }
                }(),
            "request: " + {
                if let value = self.request { return "\(value)" } else { return "request lost" }
                }(),
            "response: " + {
                if let value = self.response { return "\(value)" } else { return "response lost" }
                }()
        )
    }
}
extension DefaultDataResponse {
    
    func log() {
        printAndLog(
            "url: " + (self.request?.url?.absoluteString ?? "request url lost"),
            "statusCode: " + {
                if let value = self.response?.statusCode { return String(value) } else { return "request url lost" }
                }(),
            "request: " + {
                if let value = self.request { return "\(value)" } else { return "request lost" }
                }(),
            "response: " + {
                if let value = self.response { return "\(value)" } else { return "response lost" }
                }()
        )
    }
}

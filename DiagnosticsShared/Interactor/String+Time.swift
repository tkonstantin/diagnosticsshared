//
//  String+Time.swift
//  Diagnostics
//
//  Created by Константин on 06.02.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation


extension String {
    init(seconds: Int) {
        let minutesCount = seconds / 60
        let secondsCount = seconds % 60
        
        let mString = minutesCount < 10 ? "0\(minutesCount)" : String(minutesCount)
        let sString = secondsCount < 10 ? "0\(secondsCount)" : String(secondsCount)
        
        self = mString + ":" + sString
        
    }
}

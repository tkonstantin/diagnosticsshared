//
//  Date+Extensions.swift
//  Diagnostics
//
//  Created by Константин on 13.02.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation

public extension Date {
    
    public init(dateFromVeryStrangeString veryStrangeString: String) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddHHmmss"
        self = dateFormatter.date(from: veryStrangeString) ?? Date()
    }
    
    public func dateToComponents() -> (year: Int, month: Int, day: Int, weekDay: String, monthName: String) {
        let calendar = Calendar.current
        let comps = (calendar as NSCalendar).components([.year, .month, .day, .weekday], from: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EE"
        let dayString = dateFormatter.string(from: self).capitalized(with: Locale(identifier: "ru_RU"))
        dateFormatter.dateFormat = "MMMM"
        
        let monthString = dateFormatter.string(from: self).capitalized(with: Locale(identifier: "ru_RU"))
        return (year: comps.year!, month: comps.month!, day: comps.day!, weekDay: dayString, monthName: monthString)
    }
    
    
    public func hour() -> Int {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone.init(secondsFromGMT: 11*3600)!
        let comps = (calendar as NSCalendar).components([.hour], from: self)
        return comps.hour ?? 0
    }
    
    
    public func raw_hour() -> Int {
        let comps = (Calendar.current as NSCalendar).components([.hour], from: self)
        return comps.hour ?? 0
    }
    
    public func minute() -> Int {
        let comps = (Calendar.current as NSCalendar).components([.minute], from: self)
        return comps.minute ?? 0
    }
    
    public func second() -> Int {
        let comps = (Calendar.current as NSCalendar).components([.second], from: self)
        return comps.second ?? 0
    }
    
    public func dateToMonth() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        return dateFormatter.string(from: self).capitalized(with: Locale(identifier: "ru_RU"))
        
        
    }
    
    public func parseTime() -> String {
        let calendar = Calendar.current
        let components = (calendar as NSCalendar).components([.hour, .minute], from: self)
        var hour = String(describing: components.hour!)
        if hour.count == 1 {
            hour = "0" + hour
        }
        var minute = String(describing: components.minute!)
        if minute.count == 1 {
            minute = "0" + minute
        }
        return hour + ":" + minute
    }
    
    public func parseDate() -> String {
        let dateFormatter = DateFormatter()
        let calendar = Calendar.current
        let components = (calendar as NSCalendar).components([.month, .day, .weekday], from: self)
        let day = components.day!
        dateFormatter.dateFormat = "EE"
        let dayString = dateFormatter.string(from: self).capitalized(with: Locale(identifier: "ru_RU"))
        return String(describing: day) + " " + dayString.lowercased()
    }
    
    public func parseDateShort(dots: Bool = true) -> String {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.month, .day, .year], from: self)
        let day = components.day! < 10 ? "0\(components.day!)" : "\(components.day!)"
        let month = components.month! < 10 ? "0\(components.month!)" : "\(components.month!)"
        let year = String(components.year!)
        return [day, month, year].joined(separator: dots ? "." : "-")
        
    }
    
    
    public var dateToZero: Date {
        return Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: self)!
    }
    
    public var dateToZeroPlus3: Date {
        return Calendar.current.date(bySettingHour: 3, minute: 0, second: 0, of: self)!
    }
    
    public func dateToString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        return dateFormatter.string(from: self)
    }
    
    public func dateToNoSecondsString() -> String {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        return df.string(from: self)
    }
    
    public func dateToInt() -> Int {
        return Int(self.timeIntervalSince1970 * 1000)
    }
    
    public func scrollDatetoSevenOClock() -> Date {
        let newDate = self
        let calendar = Calendar.current
        var components = (calendar as NSCalendar).components([.hour, .minute], from: newDate)
        components.hour = 7
        components.minute = 0
        components.second = 0
        
        return newDate
    }
    
    public func startOfWeek() -> Date? {
        var cal = Calendar.current
        var comp: DateComponents = cal.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)
        comp.to12pm()
        cal.firstWeekday = 2
        return cal.date(from: comp) == nil ? nil : cal.date(from: comp)!
    }
    
    
    public func endOfWeek() -> Date? {
        let cal: Calendar = Calendar.current
        var comp: DateComponents = (cal as NSCalendar).components([ .weekOfYear, .day], from: self)
        comp.weekOfYear = 1
        comp.day = -1
        if let date = (cal as NSCalendar).date(byAdding: comp, to: self.startOfWeek()!, options: []) {
            return date
        } else {
            return nil
        }
    }
    
    public func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    public func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
    
    public func nextWeek() -> Date? {
        let cal = Calendar.current
        var comp: DateComponents = (cal as NSCalendar).components([.weekOfYear], from: self)
        comp.weekOfYear = 1
        if let date = (cal as NSCalendar).date(byAdding: comp, to: self.startOfWeek()!, options: []) {
            return date.dateToStartOfDay()
        } else {
            return nil
        }
    }
    public func previousWeek() -> Date? {
        let cal = Calendar.current
        var comp: DateComponents = (cal as NSCalendar).components([.weekOfYear], from: self)
        comp.weekOfYear = -1
        if let date = (cal as NSCalendar).date(byAdding: comp, to: self.startOfWeek()!, options: []) {
            return date.dateToStartOfDay()
        } else {
            return nil
        }
    }
    
    public func isInToday() -> Bool {
        return Calendar.current.isDateInToday(self)
    }
    
    public func isInTomorrow() -> Bool {
        return Calendar.current.isDateInTomorrow(self)
        
    }
    
    public func isInThisWeek() -> Bool {
        if let end = Date().endOfWeek()?.dateToEndOfDay() {
            return self > Date().dateToEndOfDay() && !self.isInTomorrow() && self < end
        }
        return false
    }
    
    public func isInNextWeek() -> Bool {
        if let plusWeek = Date().nextWeek() {
            
            let startOfNextWeek = plusWeek.startOfWeek()
            let endOfNextWeek = plusWeek.endOfWeek()
            if startOfNextWeek != nil && endOfNextWeek != nil {
                return self > startOfNextWeek! && self < endOfNextWeek!
            }
        }
        return false
        
    }
    
    public func isLateThanNextWeek() -> Bool {
        if let plusWeek = Date().nextWeek() {
            if let endOfNextWeek = plusWeek.endOfWeek() {
                return self > endOfNextWeek
            }
        }
        return false
    }
    
    
    
    public func setMonthFromDate(_ date: Date) -> Date {
        let cal = Calendar.current
        var comps1 = (cal as NSCalendar).components([.month], from: self)
        let comps2 = (cal as NSCalendar).components([.month], from: date)
        comps1.month = comps2.month
        if let resultDate = cal.date(from: comps1) {
            return resultDate
        } else {
            return self
        }
    }
    public func setDayFromDate(_ date: Date) -> Date {
        let cal = Calendar.current
        var comps1 = (cal as NSCalendar).components([.day], from: self)
        let comps2 = (cal as NSCalendar).components([.day], from: date)
        comps1.day = comps2.day
        if let resultDate = cal.date(from: comps1) {
            return resultDate
        } else {
            return self
        }
    }
    public func setYearFromDate(_ date: Date) -> Date {
        let cal = Calendar.current
        var comps1 = (cal as NSCalendar).components([.year], from: self)
        let comps2 = (cal as NSCalendar).components([.year], from: date)
        comps1.year = comps2.year
        if let resultDate = cal.date(from: comps1) {
            return resultDate
        } else {
            return self
        }
    }
    
   public func setTimeFromDate(_ date: Date) -> Date {
        let comps = (Calendar.current as NSCalendar).components([.hour, .minute, .second], from: date)
        if let resultDate = Calendar.current.date(bySettingHour: comps.hour!, minute: comps.minute!, second: comps.second!, of: self) {
            return resultDate
        } else {
            return self
        }
        
    }
    
    public static func >(fdate: Date, sdate: Date) -> Bool {
        if fdate.compare(sdate) == .orderedAscending || fdate.compare(sdate) == .orderedSame {
            return false
        } else{
            return true
        }
    }
    public static func <(fdate: Date, sdate: Date) -> Bool {
        if sdate.compare(fdate) == .orderedAscending || sdate.compare(fdate) == .orderedSame {
            return false
        } else{
            return true
        }
    }
    
    public func dateToStartOfDay() -> Date {
        let cal = Calendar.current
        return (cal as NSCalendar).date(bySettingHour: 0, minute: 0, second: 0, of: self, options: [])!
    }
    
    public func dateToEndOfDay() -> Date {
        let cal = Calendar.current
        let zeroDay = self.dateToStartOfDay()
        return cal.date(byAdding: .day, value: 1, to: zeroDay)!
    }
}


internal extension DateComponents {
    mutating func to12pm() {
        self.hour = 12
        self.minute = 0
        self.second = 0
    }
}


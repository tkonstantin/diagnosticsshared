//
//  ConnectionService.swift
//  Diagnostics
//
//  Created by Константин on 01.03.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import Alamofire
import Reachability

public enum connectionStates {
    case preparing
    case noNetwork
    case connected
    case disconnected
    case error
    case alert
}


public class ConnectionService {
    
   public static let shared = ConnectionService()
    
   public var hasConnection: Bool {
        get {
            return currentState == .connected || currentState == .alert
        }
    }
    
    private var currentState: connectionStates = .preparing {
        didSet {
            subscriptions.forEach({ $0(currentState) })
        }
    }
    
    private var subscriptions: [((connectionStates)->Void)] = []
    
    public func subscribeForUpdates(_ block: ((connectionStates)->Void)?) {
        guard block != nil else { return }
        subscriptions.append(block!)
        block?(currentState)
    }
    
    public func logout() {
        subscriptions = []
    }
    
    public init() {
        startListening()
    }

    private func startListening() {

        PingingService.shared.whenStateChanged = { state in
            self.currentState = state
        }
        
    }
    
}



class PingingService: NSObject {
    static let shared = PingingService()
    
    private var isPinging = false
    private var currentState: connectionStates = .preparing {
        didSet {
            whenStateChanged?(currentState)
        }
    }
    
    var whenStateChanged: ((connectionStates)->Void)?=nil
    
    override init() {
        super.init()
        continuePinging()
    }
    
    func continuePinging() {
        guard !isPinging else { return }
        isPinging = true
        makeRequest()
    }
    
    func pausePinging() {
        isPinging = false
        self.cancelPerforms()
    }
    
    @objc private func makeRequest() {
        
        if Reachability.forInternetConnection()?.isReachable() == false {
            self.currentState = .disconnected
            self.perform(#selector(self.makeRequest), with: nil, afterDelay: 3)
            return
        }
        
        guard var req = try? URLRequest(url: Server.url_paths.base_url_raw, method: .head) else { currentState = .error; return }
        req.timeoutInterval = 7
        req.setValue("myskillsMobileAgent_v2", forHTTPHeaderField: "User-Agent")
        let startDate = Date()
        

        self.cancelPerforms()
        self.perform(#selector(self.reportProblems), with: nil, afterDelay: 3)
        
        Alamofire.request(req).response { (response) in
            self.cancelPerforms()

            if response.response?.statusCode == 200 {
                if Date().timeIntervalSince1970 - startDate.timeIntervalSince1970 >= 3 {
                    self.currentState = .alert
                    self.perform(#selector(self.makeRequest), with: nil, afterDelay: 3)

                } else {
                    self.currentState = .connected
                    self.perform(#selector(self.makeRequest), with: nil, afterDelay: 3)

                }
            } else {
                self.currentState = .disconnected
                self.perform(#selector(self.makeRequest), with: nil, afterDelay: 1)
            }
        }
    }
    
    @objc private func reportProblems() {
        if currentState == .connected {
            self.currentState = .alert
        }
    }
    
    private func cancelPerforms() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.makeRequest), object: nil)
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reportProblems), object: nil)
    }
    
}

//
//  Server.swift
//  Diagnostics
//
//  Created by Константин on 07.02.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

public class Server: NSObject {
    
    public static let shared = Server()
    
    private static let defaultUserAgent = "myskillsMobileAgent_v2"
    
    public var isAuthorized: Bool {
        get {
            return cookies.count != 0
        }
    }
    
    public private(set) var currentUser: User!
    public var cookies: [HTTPCookie] = []
    private var variantID: String!
    private let headers: [String: String] = [ "User-Agent" : defaultUserAgent ]
    
    private var sendingAnswersObservation: NSKeyValueObservation?
    private var pendingFinishExamWorkForced: (id: String, completion: ()->Void)?
    
    lazy private var saveAnswersQueue: OperationQueue = {
        let q = OperationQueue()
        q.qualityOfService = .userInitiated
        q.maxConcurrentOperationCount = 1
        return q
    }()
    
    
    public func login(code: [String], completion: @escaping (Bool, LoginError?, String?)->Void) {
        
        guard ConnectionService.shared.hasConnection else {
            completion(false, .no_network, nil);
            return;
        }
        
        guard code.count == 6, let encodedCode = code.joined().addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) else { completion(false, .invalid_code, nil); return }
        
        var params = [String: String]()
        params["authenticationCode"] = code.joined()
        
        Alamofire.request(url_paths.base_url + url_paths.account.login + "?authenticationCode=\(encodedCode)", method: .post, parameters: nil, encoding: URLEncoding.default, headers: headers).response { (response) in
            
            guard response.response?.statusCode == 200 else {
                response.log()
                
                if let data = response.data {
                    if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary, let modelState = json?["ModelState"] as? NSDictionary {
                        completion(false, .invalid_code, (modelState.allValues.first as? [String])?.first);
                        return
                    } else if let dataString = String(data: data, encoding: .utf8), !dataString.isEmpty {
                        completion(false, .invalid_code, dataString);
                        return
                    } else {
                        completion(false, .invalid_code, nil)
                    }
                }
                completion(false, .invalid_code, nil);
                return
            }
            
            if self.getCookie(from: response) {
                self.getUserInfo(completion: { success in
                    if success {
                        self.getTestDescription(completion: { (_success) in
                            completion(_success, nil, nil)
                        })
                    }
                })
            } else {
                completion(false, .invalid_code, nil)
            }
        }
    }
    
    public func checkActivationCode(code: [String], completion: @escaping (Bool)->Void) {
        guard code.count == 6 else { completion(false); return }
        
        var params = [String: String]()
        params["activationCode"] = code.joined()
        
        Alamofire.request(url_paths.base_url + url_paths.examTest.checkActivationCode, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            completion((response.response?.statusCode ?? 0)/100 == 2)
        }
    }
    
    
    public func logout(completion: @escaping ()->Void) {
        currentUser = nil
        var sent = false
        ConnectionService.shared.subscribeForUpdates { (_) in
            if ConnectionService.shared.hasConnection, !sent {
                sent = true
                Alamofire.request(url_paths.base_url + url_paths.account.logout, method: .post, parameters: nil, encoding: URLEncoding.default, headers: self.headers).responseJSON { (response) in
                    completion()
                }
            }
        }
        
    }
    
    
    public func newExamWork(completion: @escaping (Bool)->Void) {
        guard let id = self.variantID else { completion(false); return }
        Alamofire.request(url_paths.base_url +  url_paths.examTest.newExamWork, method: .post, parameters: ["VariantId": id], encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            let value = response.result.value
            let workID = value as? String ?? String(value as? Int ?? 0)
            if workID == "0" || workID.isEmpty || (response.response?.statusCode ?? 500)/100 != 2 {
                response.log()
                completion(false);
                return;
            }
            self.loadExamWork(workID: workID, completion: completion)
        }
    }
    
    
    public func saveCurrentUser(completion:  @escaping ()->Void) {
        guard self.currentUser != nil else { completion(); return }
        var params: [String: Any] = [:]
        params[User.fields.Surname] = currentUser.Surname
        params[User.fields.Name] = currentUser.Name
        params[User.fields.SecondName] = currentUser.SecondName
        params[User.fields.EmailContact] = currentUser.EmailContact
        params[User.fields.DocumentSeries] = currentUser.DocumentSeries
        params[User.fields.DocumentNumber] = currentUser.DocumentNumber
        params[User.fields.Login] = currentUser.Login
        Alamofire.request(url_paths.base_url + url_paths.user.save, method: .put, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            completion()
        }
    }
    

    
    func saveAnswer(with question: Question, isEmpty: Bool, number: Int, retry: Bool = false, completion: @escaping ()->Void) {
        
        if let answerDict = question.cachedAnswer, let answerID = SessionService.shared.currentExamWork?.html_answer_ids[question.id] {
            let params = makeAnswerParameters(from: answerDict, isEmpty: isEmpty, answerID: answerID, number: question.numberInKim)
            
            saveAnswersQueue.operations.compactMap({$0 as? SaveAnswerOperation}).filter({$0.number == number}).forEach({ $0.cancel() })

            saveAnswersQueue.addOperation(SaveAnswerOperation(params: params, number: number, waitLess: true, completion: { [weak self] (saved) in
                if !saved {
                    asyncAfter(3000) { [weak self] in
                        self?.saveAnswer(with: question, isEmpty: isEmpty, number: number, retry: true, completion: completion)
                    }
                }
            }))
        }
        asyncAfter(50) {
            if !retry {
                completion()
            }
        }
    }

    
    public func finishExamWork(with id: String, status: Int, completion: @escaping ()->Void) {
        SessionService.shared.currentTest.setEnded()
        if !ConnectionService.shared.hasConnection {
            DiagnosticsShared.instance.delegate?.diagnostics_shouldHideHUD()
            _ = showAlert(title: "Отсутствует подключение к сети", body: "Результаты работы будут отправлены при восстановлении подключения", needsOKAction: true)
            
            var sent = false
            ConnectionService.shared.subscribeForUpdates { [weak self] (_) in
                if ConnectionService.shared.hasConnection && !sent {
                    sent = true
                    self?.finishExamWorkForced(with: id, status: status, completion: {
                        completion()
                    })
                }
            }
            return
        } else {
            self.finishExamWorkForced(with: id, status: status, completion: completion)
        }
        
    }
    
    public func applyVariant(with id: String, completion: @escaping (Bool)->Void) {
        variantID = id
        let variant = Variant(with: [:])
        variant.VariantId = id
        SessionService.shared.currentVariant = variant
        completion(true)
    }
    
    public func getUserInfo(completion: @escaping (Bool)->Void) {
        
        Alamofire.request(url_paths.base_url + url_paths.user.getInfo, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            
            if let json = response.result.value as? NSDictionary {
                self.currentUser = User(with: json)
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    public func clearUser() {
        self.currentUser = nil
    }
    
}

//MARK: - Loading data after login
fileprivate extension Server {
    
    func getTestDescription(completion: @escaping (Bool)->Void) {
        Alamofire.request(url_paths.base_url + url_paths.examTest.getTestDescription, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            
            SessionService.shared.currentTest = Test(with: response.result.value as? NSDictionary ?? [:])
        
            self.resolveVariant(for: SessionService.shared.currentTest.SubjectTestId) { success in
                completion(success)
            }
        }
    }
    
    func resolveVariant(for id: String, completion: @escaping (Bool)->Void) {
        Alamofire.request(url_paths.base_url + url_paths.examTest.resolveVariant, method: .get, parameters: ["subjectTestId": id], encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            
            if let json = response.result.value as? NSDictionary {
                SessionService.shared.currentVariant = Variant(with: json)
                self.getTasksTextsHtmlByVariant(SessionService.shared.currentVariant!.VariantId, completion: completion)
            } else {
                response.log()
                completion(false)
            }
        }
    }
    
    
    
    func getTasksTextsHtmlByVariant(_ id: String, completion: @escaping (Bool)->Void) {
        Alamofire.request(url_paths.base_url + url_paths.examTest.getTaskTextsHtmlByVariant, method: .get, parameters: ["variantId": id], encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            var text_ids: [String] = []
            
            if let arr = response.result.value as? [String] {
                text_ids = arr
            } else if let arr = response.result.value as? [Int] {
                text_ids = arr.map({String($0)})
            }
            
            if text_ids.count == 0 {
                response.log()
            }
            
            for text_id in text_ids {
                
                    let test = SessionService.shared.currentTest
                    let question = Question(id: text_id)
                    test.questions.append(question)
                    SessionService.shared.currentTest = test
                    
                    if test.questionsCount == text_ids.count {
                        if text_ids.count != 0 {
                            NotificationCenter.default.post(Notification(name: kNotificationStartCurrentTest))
                        } else {
                            completion(text_ids.count != 0)
                        }
                    }
            }
            
            self.variantID = id
        }
    }
    
    
    
//    func getTaskHtml(with id: String, completion: @escaping (String, String)->Void) {
//        htmlTaskRequest(with: id).response { (response) in
//            if let data = response.data {
//                if let string = String(data: data, encoding: .utf8) {
//                    completion(string, id)
//                }
//            } else {
//                response.log()
//            }
//        }
//    }
    
}


//MARK: - Loading examwork
extension Server {
    
    func loadExamWork(workID id: String, completion: @escaping (Bool)->Void) {
        Alamofire.request(url_paths.base_url + url_paths.examTest.loadExamWork + "/\(id)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            
            //let str = String(data: response.data!, encoding: String.Encoding.utf8)
            let dir = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory,
                                                      in: FileManager.SearchPathDomainMask.userDomainMask).last! as NSURL
            if let fileurl = dir.appendingPathComponent("log_loadExamWork.txt") {
                if FileManager.default.fileExists(atPath: fileurl.path) {
                    try? FileManager.default.removeItem(at: fileurl)
                }
                
                try? response.data!.write(to: fileurl, options: Data.WritingOptions.atomicWrite)
                
            }
            
            //print(String(data: response.data!, encoding: String.Encoding.utf8)!);
            if let work = ExamWork(with: id, json: response.result.value as? NSDictionary), (response.response?.statusCode ?? 500)/100 == 2 {
                SessionService.shared.currentExamWork = work
                self.loadExamResult()
                completion(true)
            } else {
                response.log()
                completion(false)
            }
        }
    }
    
}


//MARK: - Loading results info
fileprivate extension Server {
    
    func loadExamResult() {
        guard let workID = SessionService.shared.currentExamWork?.id else { return }
        Alamofire.request(url_paths.base_url + url_paths.examResult.get, method: .get, parameters: ["examWorkId": workID], encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            if let json = response.result.value as? NSDictionary {
                SessionService.shared.currentTest.results = TestResult(with: json)
            }
        }
    }
    
}

//MARK: - Saving answer
fileprivate extension Server {
    
    func makeAnswerParameters(from dict: NSDictionary, isEmpty: Bool, answerID: String, number: Int) -> [String: Any] {
        var result: [String: Any] = [:]
        var answersLayer: [String: Any] = [:]
        
        if let answers = dict["Answers"] as? [NSDictionary] {
            answersLayer["Answers"] = answers.map({$0 as! [String: Any]})
        }
        if let type = dict["TaskType"] as? NSDictionary {
            answersLayer["TaskType"] = type
        }
        
        result["Answers"] = isEmpty ? [:] : answersLayer
        result["NumberInKim"] = number
        result["HtmlText"] = isEmpty ? nil : String(data: try! JSONSerialization.data(withJSONObject: answersLayer, options: []), encoding: .utf8)
        if let intID = Int(answerID) {
            result["AnswerId"] = intID
        } else {
            result["AnswerId"] = answerID
        }
        return result
        
    }
    
}


//MARK: - Finishing answers
fileprivate  extension Server {
    
    func waitForAnswersUpload(completion: @escaping ()->Void) {
        if saveAnswersQueue.operations.filter({!$0.isCancelled && !$0.isFinished}).count == 0 {
            completion();
            return
        }
        let initialCount = saveAnswersQueue.operations.filter({!$0.isCancelled && !$0.isFinished}).count
        sendingAnswersObservation = saveAnswersQueue.observe(\.operationCount, options: [.new]) { [unowned self] (queue, change) in
            let newValue = self.saveAnswersQueue.operations.filter({!$0.isCancelled && !$0.isFinished}).count
            let currentIndex = max(0, initialCount - newValue)
            CompletionStatusLabel.set(status: "Отправка ответов (\(currentIndex)/\(max(1, initialCount)))", progress: Float(initialCount - newValue)/Float(initialCount))
            if newValue == 0 {
                completion()
                self.sendingAnswersObservation = nil
            }
        }
    }
    
}


//MARK: - Finishing examwork
fileprivate extension Server {
    
    func loadMobileHelper(trial: Int=0, completion: @escaping ()->Void) {
        guard trial < 3 else { completion(); return }
        guard SessionService.shared.mobileHelperJS == nil || SessionService.shared.mobileHelperJS!.isEmpty else { completion(); return }
        
        let path = Server.url_paths.base_url_raw + "scriptsapp/mobilehelper.js"
        
        Alamofire.SessionManager.default.requestWithoutCache(path).responseString { (responseString) in
            if let value = responseString.result.value, !value.isEmpty {
                SessionService.shared.mobileHelperJS = value
                completion();
                return;
            } else {
                asyncAfter(3000, {
                    self.loadMobileHelper(trial: trial + 1, completion: completion)
                })
            }
        }
    }
    
    
    func finishExamWorkForced(with id: String, status: Int, completion: @escaping ()->Void) {
        if CompletionStatusLabel.instance == nil {
            DiagnosticsShared.instance.delegate?.diagnostics_shouldShowHUD(with: "Завершение работы...", progress: nil)
        }
        
        self.loadMobileHelper {
        
            self.waitForAnswersUpload {
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.retryFinish), object: nil)
                CompletionStatusLabel.set(status: "Завершение работы...")
                
                Alamofire.request(url_paths.base_url + url_paths.examTest.finishExamWork + "/\(id)?status=\(status)", method: .put, parameters: nil, encoding: URLEncoding.default, headers: self.headers).responseJSON { (response) in
                    let code = response.response?.statusCode ?? 0
                    if code / 100 == 2 {
                        SessionService.shared.currentExamWork?.isFinished = true
                        self.pendingFinishExamWorkForced = nil
                        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.retryFinish), object: nil)
                        completion()
                    } else {
                        CompletionStatusLabel.set(status: "Ошибка завершения работы. Пробуем еще раз...")
                        
                        self.pendingFinishExamWorkForced = (id: id, completion: completion)
                        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.retryFinish), object: nil)
                        self.perform(#selector(self.retryFinish), with: nil, afterDelay: 3)
                        return;
                    }
                }
            }
        }
    }
    
    
    @objc func retryFinish() {
        guard let pending = pendingFinishExamWorkForced else { return }
        let status = SessionService.shared.currentExamWork!.getStatus()

        finishExamWorkForced(with: pending.id, status: status, completion: pending.completion)
    }
    
}

//MARK: - Cookies
extension Server {
    
    public func clearCookies() {
        cookies = []
        Alamofire.SessionManager.default.session.configuration.httpCookieStorage?.setCookies([], for: Server.url_paths.baseURL, mainDocumentURL: Server.url_paths.baseURL)
    }
    
    public func getCookie(from response: DefaultDataResponse) -> Bool {
        if let res = response.response, let url =  res.url, let headers = res.allHeaderFields as? [String : String] {
            let cookies = HTTPCookie.cookies(withResponseHeaderFields: headers, for: Server.url_paths.baseURL)
            printAndLog("getCookies at login", "cookies: \(cookies)", isError: false)
            return setCookie(cookie: cookies, for: url)
        }
        return false
    }
    
    func setCookie(cookie: [HTTPCookie], for url: URL) -> Bool {
        cookies = cookie
        
        if let existingCookie = cookie.first(where: {$0.name == ".AspNet.MobileApi" }) {
            
            var properties: [HTTPCookiePropertyKey: Any] = [HTTPCookiePropertyKey.name : ".AspNet.Cookies",
                                                            HTTPCookiePropertyKey.version: existingCookie.version,
                                                            HTTPCookiePropertyKey.value: existingCookie.value,
                                                            HTTPCookiePropertyKey.domain: existingCookie.domain,
                                                            HTTPCookiePropertyKey.path: existingCookie.path
            ]
            
            if existingCookie.isSecure {
                properties[HTTPCookiePropertyKey.secure] = "TRUE"
            }
            
            if existingCookie.expiresDate != nil {
                properties[HTTPCookiePropertyKey.expires] = existingCookie.expiresDate!
            }
            
            if let secondCookie = HTTPCookie(properties: properties) {
                cookies.append(secondCookie)
            }
        }
        
        Alamofire.SessionManager.default.session.configuration.httpCookieStorage?.setCookies(cookies, for: Server.url_paths.baseURL, mainDocumentURL: Server.url_paths.baseURL)
        
        let cookieStorage = HTTPCookieStorage.shared
        cookieStorage.setCookies(cookies,
                                 for: url,
                                 mainDocumentURL: nil)
        
        printAndLog("cookies changed", "cookies: \(cookies)", isError: false)
        
        return true
    }
    
    func getCookie(forURL url: URL) -> [HTTPCookie]? {
        let cookieStorage = HTTPCookieStorage.shared
        return cookieStorage.cookies(for: url) 
    }
    
    func deleteCookie(forURL url: URL) {
        let cookieStorage = HTTPCookieStorage.shared
        if let cookies = getCookie(forURL: url) {
            for cookie in cookies {
                cookieStorage.deleteCookie(cookie)
            }
        }
    }
    
}





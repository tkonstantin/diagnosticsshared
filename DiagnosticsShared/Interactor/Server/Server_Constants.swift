//
//  Url_paths.swift
//  Diagnostics
//
//  Created by Константин on 30/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import Alamofire

extension Server {
        
    enum url_paths {
        static let base_url = DiagnosticsShared.instance.base_url!
        static let base_url_raw: String = {
            let url = URL(string: base_url)!
            return url.scheme! + "://" + url.host! + "/"
        }()
        static let baseURL: URL = {
            URL(string: base_url_raw)!
        }()
        
        enum account {
            ///Выход участника
            static let logout = "Account/Logout"
            ///Авторизация участника по коду диагностики
            static let login = "Account/Login"
        }
        
        enum examResult {
            ///Получить результаты по работе
            static let get = "ExamResult/Get"
            static let examResultHTML = "\(Server.url_paths.base_url_raw)examresults/show/%@/"
        }
        
        enum examTest {
            ///Получить время окончания паузы
            static let getPause = "ExamTest/GetPause"
            ///Загрузка работы участника /id
            static let loadExamWork = "ExamTest/LoadExamWork"
            ///Получение данных для тестирования по текущему участнику
            static let getTestDescription = "ExamTest/GetTestDescription"
            ///Получить данные по текущему участнику диагностики
            static let getCurrentParticipantTest = "ExamTest/GetCurrentParticipantTest"
            ///Получение html содержимого задания
            static let getTaskHTML = "ExamTest/GetTaskHtml"
            ///Получение варианта для указанного subjectTestId
            static let resolveVariant = "ExamTest/ResolveVariant"
            ///Получение id html содержимого варианта по id варианта
            static let getTaskTextsHtmlByVariant = "ExamTest/GetTaskTextsHtmlByVariant"
            ///Проверка кода активации организатора
            static let checkActivationCode = "ExamTest/CheckActivationCode"
            ///Сохранение ответа участника по работе
            static let saveAnswer = "ExamTest/SaveAnswer"
            ///Завершить работу участника по id
            static let finishExamWork = "ExamTest/FinishExamWork"
            ///Установить время окончания паузы
            static let setPause = "ExamTest/SetPause"
            ///Создание работы участнику
            static let newExamWork = "ExamTest/NewExamWork"
        }
        
        enum user {
            ///Получение информации по текущему пользователю
            static let getInfo = "User/GetInfo"
            ///Сохранение инфрмации об участнике тестирования
            static let save = "User/SaveParticipantUser"
        }
        
    }
//
//    func htmlTaskRequest(with id: String) -> DataRequest {
//        return Alamofire.request(url_paths.base_url + url_paths.examTest.getTaskHTML, method: .get, parameters: ["htmlTextId": id], encoding: URLEncoding.default, headers: nil)
//    }
    
    func htmlTaskURL(with id: String="") -> String {
        return url_paths.base_url_raw + "api/KimTemplate/\(id)"
    }
    
    func getTaskURL(with id: String) -> String {
        return url_paths.base_url + url_paths.examTest.getTaskHTML + "?htmlTextId=\(id)"
    }
    
    func getResultURL(id examWorkId: String?) -> URL? {
        guard let examWorkId = examWorkId else { return nil }
        return URL(string: String(format: url_paths.examResult.examResultHTML, SessionService.shared.currentExamWork?.resultsRouteURL ?? "preprof") + examWorkId)
    }
    
}

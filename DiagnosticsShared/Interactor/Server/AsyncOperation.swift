//
//  AsyncOperation.swift
//  Diagnostics
//
//  Created by Константин on 13/12/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import Alamofire

fileprivate let asyncStateQueue = DispatchQueue(label: "stateQueue", qos: .background)

class AsyncOperation: Operation {
    
    
    enum State: String {
        case ready, executing, finished
        
        fileprivate var keyPath: String {
            return "is" + rawValue.capitalized
        }
    }
    
    fileprivate var state = State.ready {
        willSet {
            willChangeValue(forKey: newValue.keyPath)
            willChangeValue(forKey: state.keyPath)
        }
        didSet {
            didChangeValue(forKey: oldValue.keyPath)
            didChangeValue(forKey: state.keyPath)
        }
    }
    
    override var isReady: Bool {
        return super.isReady && state == .ready
    }
    
    override var isExecuting: Bool {
        return state == .executing
    }
    
    override var isFinished: Bool {
        return state == .finished
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    override func start() {
        asyncStateQueue.async {
            if self.isCancelled {
                self.state = .finished
                return
            }
            
            self.state = .executing
            self.main()
        }
    }
    
    override func cancel() {
        super.cancel()
        
        asyncStateQueue.async {
            if self.state == .executing {
                self.state = .finished
            }
        }
    }
    
}


class SaveAnswerOperation: AsyncOperation {
    
    var params: [String: Any]!
    var number: Int!
    var waitLess: Bool!
    var completion: ((Bool)->Void)!
    var requestSent = false
    
    convenience init(params p: [String: Any], number n: Int, waitLess w: Bool, completion c: @escaping (Bool)->Void) {
        self.init()
        params = p
        number = n
        waitLess = w
        completion = c
    }
    
    override func cancel() {
        super.cancel()
        completion = nil
    }
    
    override func main() {
        ConnectionService.shared.subscribeForUpdates { [weak self] (_) in
            guard let `self` = self else { return }
            guard ConnectionService.shared.hasConnection, !self.requestSent else { return }
            self.requestSent = true
            
            guard !self.isFinished && !self.isCancelled else { return }
            
            print("sending answer №\(self.number!)")
            
            var req = URLRequest(url: URL(string: Server.url_paths.base_url + Server.url_paths.examTest.saveAnswer)!)
            req.timeoutInterval = self.waitLess ? 3 : 10
            req.httpMethod = "PUT"
            req.setValue("myskillsMobileAgent_v2", forHTTPHeaderField: "User-Agent")
            req.setValue("application/json", forHTTPHeaderField: "Content-Type")
            req.httpBody = try? JSONSerialization.data(withJSONObject: self.params, options: [])
            
            Alamofire.request(req).response { [weak self] (response) in
                let success = (response.response?.statusCode ?? 0) / 100 == 2
                if !success {
                    response.log()
                }
                
                self?.completion?(success)
                
                asyncStateQueue.async {
                    self?.state = .finished
                }
            }
        }
    }
}

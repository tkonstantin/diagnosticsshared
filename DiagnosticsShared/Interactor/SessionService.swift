//
//  SessionService.swift
//  Diagnostics
//
//  Created by Константин on 31.01.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation


public class SessionService {
    
    public static let shared = SessionService()
    
    public var mobileHelperJS: String?
    
    public var code: [String] = []
    public var activationCode: [String] = []
    
    public var knownAnswers: [String: String] = [:]
    public var answerShuffleParams: [String: String] = [:]
    
    public var currentExamWork: ExamWork?
    
    var currentVariant: Variant?
    
    func clearCurrentTest() {
        self.test = Test(with: [:])
    }
    
    public var currentTest: Test {
        get {
            return test
        } set {
            test = newValue
        }
    }
    
    public func setConfirmed(with activation: [String], completion: @escaping (Bool)->Void) {
        activationCode = activation
        Server.shared.checkActivationCode(code: activation) { (success) in
            self.currentTest.isConfirmed = success
            self.notifySubscribers()
            completion(success)
        }
    }
    
    func handleResultDict(for question: Question, isEmpty: Bool, dict: NSDictionary, number: Int, completion: (()->Void)?=nil) {
        question.cachedAnswer = dict
        question.isEmpty = isEmpty
        if SessionService.shared.currentExamWork!.isHandMadeWorks.contains(question.id) == false {
            Server.shared.saveAnswer(with: question, isEmpty: isEmpty, number: number) {
                completion?()
            }
        } else {
            completion?()
        }
    }
    
    public func auth(completion: @escaping (Bool, LoginError?, String?)->Void) {
        Server.shared.login(code: self.code, completion: completion)
    }
    
    public func subscribeForChanges(completion: @escaping ()->Void) {
        subscriptions.append(completion)
    }
    
    public func logout() {
        printAndLog("Logged out", isError: false)

        subscriptions = []
        code = []
        activationCode = []
        currentExamWork = nil
        currentVariant = nil
        currentTest = Test(with: [:])
    }
    
    fileprivate var test: Test = Test(with: [:])
    fileprivate var subscriptions: [(()->Void)] = []
    
    public func notifySubscribers() {
        subscriptions.forEach({ $0() })
    }
    

    
}

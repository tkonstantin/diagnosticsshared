//
//  PauseManager.swift
//  Diagnostics
//
//  Created by Константин on 02.07.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import UIKit

class PauseManager {
    
    static let shared = PauseManager()
    
    var duration: Double = 300
    var frequency: Double = 25
    var isEnabled = false
    
    private var testStartDate: Date?
    private var lastPauseDate: Date?
    private var pauseRequestTimer: Timer!
    
    func testStarted() {
        guard isEnabled else { return }
        testStartDate = Date()
        pauseRequestTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.checkIfPauseNeeded), userInfo: nil, repeats: true)
    }
    
    func testEnded() {
        pauseRequestTimer?.invalidate()
        pauseRequestTimer = nil
        testStartDate = nil
        lastPauseDate = nil
    }
    
    func pauseStarted() {
        lastPauseDate = Date()
        SessionService.shared.currentTest.setEndDate(SessionService.shared.currentTest.endDate.addingTimeInterval(duration))
    }
    
    
    @objc private func checkIfPauseNeeded() {
        guard isEnabled else { return }
        
        if let last = lastPauseDate {
            if Date().timeIntervalSince1970 > last.timeIntervalSince1970 + frequency + duration {
                showPause()
            }
        } else if let testStart = testStartDate {
            if Date().timeIntervalSince1970 > testStart.timeIntervalSince1970 + frequency {
                showPause()
            }
        }
    }
    
    @objc private func showPause() {
        lastPauseDate = Date()
        
        if Double(SessionService.shared.currentTest.secondsLeft) > PauseManager.shared.duration + 330 {
            guard !PauseViewController.isPresented else { return }
            let pauseVC = PauseViewController.makeOne()
            UIApplication.presentOnTop(vc: pauseVC, animated: false)
        }
    }
    
}

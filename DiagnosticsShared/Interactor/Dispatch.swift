//
//  Dispatch.swift
//  Diagnostics
//
//  Created by Константин on 06.02.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation

///миллисекунды
public func asyncAfter(_ milliseconds: Int=0, _ block: @escaping (()->Void)) {
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DispatchTimeInterval.milliseconds(milliseconds), execute: block)
}

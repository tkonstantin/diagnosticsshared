//
//  FirebaseService.swift
//  UPS-Admin
//
//  Created by Константин on 23.05.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase
import FireLogging
import Alamofire

class FirebaseService: FirebaseServiceProtocol {
    
    
    static let shared = FirebaseService()
    
    func config() {
        FireLogging.config(service: self)
    }
    
    private lazy var dayFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy"
        return df
    }()
    
    var rootRef: Any {
        get {
            return Database.database().reference().child(dayFormatter.string(from: Date()))
        }
    }
    
    var rootDBRef: DatabaseReference {
        get {
            return rootRef as! DatabaseReference
        }
    }
    
    var currentUUID: String {
        get {
            if let uuid = KeychainService.loadString(for: "aasoasdkjjnn"), !uuid.isEmpty {

                return uuid
            } else {
                let uuid = String.random(6)
                KeychainService.save(string: uuid, for: "aasoasdkjjnn")
                return uuid
            }
        }
    }
    
    var rootUserRef: DatabaseReference {
        get {
            return rootDBRef.child(currentUUID)
        }
    }
    
    func observe(child: String?=nil, single: Bool, _ block: @escaping (NSDictionary)->Void) -> UInt? {
        var ref = rootDBRef
        if child != nil {
            ref = ref.child(child!)
        }
        
        return ref.observe(.value) { (snap) in
            block(snap.value as? NSDictionary ?? [:])
        }
    }
    
    func deobserve(_ handle: UInt) {
        rootDBRef.removeObserver(withHandle: handle)
    }
    
    
    func log(type: EventTypes, customFields: FirParams?=nil) {
        updateCurrentUser()
        rootUserRef.childByAutoId().setValue(LogEvent(type: type, customFields: customFields).dict)
    }
    
    func logCustom(_ title: String, customFields: FirParams) {
        updateCurrentUser()
        rootUserRef.childByAutoId().setValue(LogEvent(title: title, customFields: customFields).dict)
    }
    
    func updateCurrentUser() {
        let user = LogUser(email: nil, userID: Server.shared.currentUser?.UserUid)
        for case let key as String in user.dict.allKeys {
            rootUserRef.child(key).setValue(user.dict[key])
        }
        
    }
    
}




extension String {
    
    static func random(_ length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
}

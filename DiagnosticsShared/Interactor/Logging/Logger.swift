//
//  Logger.swift
//  Diagnostics
//
//  Created by Константин on 20/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import Alamofire
import Crashlytics
import Firebase


func printAndLog(_ items: Any?..., isError: Bool = true) {
    print(items)
    
    guard items.count != 0 else { return }
    
    var loggingDict: [String: String] = [:]
    for index in 0..<items.count {
        loggingDict[String(index)] = "\(items[index] ?? "nil")"
    }
    
    
    if isError {
        FirebaseService.shared.logCustom("Error", customFields: loggingDict)

        let error = NSError(domain: "PrintableError", code: isError ? 0 : 1, userInfo: loggingDict)
        Crashlytics.sharedInstance().recordError(error)
        Analytics.logEvent("PrintableError", parameters: loggingDict)
    } else {
        FirebaseService.shared.logCustom("Record", customFields: loggingDict)

        Answers.logCustomEvent(withName: "Record", customAttributes: loggingDict)
        Analytics.logEvent("Record", parameters: loggingDict)
    }
}


func printAndLog(_ type: eventTypes, _ items: Any?...) {
    var params = [String: String]()
    items.forEach({ params[String(params.keys.count)] = "\($0 ?? "nil")" })
    
    let eventName = "\(type.rawValue)\(items)"
    
    
    FirebaseService.shared.logCustom(eventName, customFields: params)

    
    Answers.logCustomEvent(withName: eventName, customAttributes: params)
    Analytics.logEvent(eventName, parameters: params)
}



enum eventTypes: String {
    case iframesLost
    case setSrcError
}


extension DataResponse {
    func log() {
        
        printAndLog(
            "url: " + (self.request?.url?.absoluteString ?? "request url lost"),
            "statusCode: " + {
                if let value = self.response?.statusCode { return String(value) } else { return "request url lost" }
                }(),
            "request: " + {
                if let value = self.request { return "\(value)" } else { return "request lost" }
                }(),
            "response: " + {
                if let value = self.response { return "\(value)" } else { return "response lost" }
                }()
        )
    }
}
extension DefaultDataResponse {
    func log() {
        printAndLog(
            "url: " + (self.request?.url?.absoluteString ?? "request url lost"),
            "statusCode: " + {
                if let value = self.response?.statusCode { return String(value) } else { return "request url lost" }
                }(),
            "request: " + {
                if let value = self.request { return "\(value)" } else { return "request lost" }
                }(),
            "response: " + {
                if let value = self.response { return "\(value)" } else { return "response lost" }
                }()
        )
    }
}

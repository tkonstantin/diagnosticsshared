//
//  DiagnosticsSharedDelegate.swift
//  DiagnosticsShared
//
//  Created by Константин on 14/03/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation
import UIKit

public protocol DiagnosticsSharedDelegate: class {
    
    func diagnostics_shouldChangeRoot(to: UIViewController) -> UIViewController?
    func diagnostics_completed()
    func diagnostics_shouldShowHUD(with status: String?, progress: Float?)
    func diagnostics_shouldHideHUD()
    
    func diagnostics_finishWorkWithExamWorkId(with examWorkId: String?)
    
}

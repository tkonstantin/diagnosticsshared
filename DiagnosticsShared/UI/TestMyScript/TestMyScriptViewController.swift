//
//  TestMyScriptViewController.swift
//  DiagnosticsShared
//
//  Created by Pavel Kurta on 13/09/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import UIKit

class TestMyScriptViewController : UIViewController {
    
    @IBOutlet weak var testTextField: UITextField!
    
    class func make() -> TestMyScriptViewController {
        return UIStoryboard(name: "TestMyScript", bundle: DiagnosticsShared.bundle).instantiateViewController(withIdentifier: "TestMyScriptViewController") as! TestMyScriptViewController
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "myScriptVC" {
            self.editorViewController = segue.destination as! EditorViewController
            self.editorViewController.engine = self.engine
            self.editorViewController.inputMode = .forcePen
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        do {
            if let package = try createPackage(packageName: "New") {
                try self.editorViewController.editor.part = package.getPartAt(0)
            }
        } catch {
            print("Error while creating package : " + error.localizedDescription)
        }
        
        //self.editorViewController.editor.theme = ".greenThickPen { color: #00FF00FF; -myscript-pen-width: 1.5; }"
        //self.editorViewController.editor.penStyleClasses = "greenThickPen"
        //self.editorViewController.editor.penStyle = "color: #00FF00FF; -myscript-pen-width: 1.5"
    }
    
    func createPackage(packageName: String) throws -> IINKContentPackage? {
        // Create a new content package with name
        var resultPackage: IINKContentPackage?
        let fullPath = FileManager.default.pathForFile(inDocumentDirectory: packageName) + ".iink"
        if let engine = self.engine {
            resultPackage = try engine.createPackage(fullPath.decomposedStringWithCanonicalMapping)
            
            // Add a blank page type Text Document
            if let part = try resultPackage?.createPart("Text Document") /* Options are : "Diagram", "Drawing", "Math", "Text Document", "Text" */ {
                self.title = "Type: " + part.type
            }
        }
        return resultPackage
    }
    
    /**
     * IINK Engine, lazy loaded.
     *
     * @return the iink engine.
     */
    weak var editorViewController: EditorViewController!
    
    var engineErrorMessage: String?
    
    lazy var engine: IINKEngine? = {
        // Check that the MyScript certificate is present
        if myCertificate.length == 0
        {
            self.engineErrorMessage = "Please replace the content of MyCertificate.c with the certificate you received from the developer portal"
            return nil
        }
        
        // Create the iink runtime environment
        NSLog("Bundle: %@", Bundle.main.bundleIdentifier!)
        let data = Data(bytes: myCertificate.bytes, count: myCertificate.length)
        guard let engine = IINKEngine(certificate: data) else
        {
            self.engineErrorMessage = "Invalid certificate"
            return nil
        }
        
        // Configure the iink runtime environment
        let configurationPath = Bundle(for: self.classForCoder).bundlePath.appending("/recognition-assets/conf/")
        let us = configurationPath.appending("en_US.conf")
        NSLog("us_US exist: %i", FileManager.default.fileExists(atPath: us))
        let ru = configurationPath.appending("ru_RU.conf")
        NSLog("ru_RU exist: %i", FileManager.default.fileExists(atPath: ru))

        do {
            try engine.configuration.setStringArray([configurationPath], forKey:"configuration-manager.search-path") // Tells the engine where to load the recognition assets from.
            try engine.configuration.setString("lang", forKey: "ru_RU");
        } catch {
            print("Should not happen, please check your resources assets : " + error.localizedDescription)
            return nil
        }
        
        // Set the temporary directory
        do {
            try engine.configuration.setString(NSTemporaryDirectory(), forKey: "content-package.temp-folder")
        } catch {
            print("Failed to set temporary folder: " + error.localizedDescription)
            return nil
        }
        
        return engine
    }()
    
    @IBAction func clearButtonWasTouchedUpInside(_ sender: Any) {
        editorViewController.editor.clear()
    }
    
    @IBAction func undoButtonWasTouchedUpInside(_ sender: Any) {
        editorViewController.editor.undo()
    }
    
    @IBAction func redoButtonWasTouchedUpInside(_ sender: Any) {
        editorViewController.editor.redo()
    }
    
    @IBAction func convertButtonWasTouchedUpInside(_ sender: Any) {
        do {
            let supportedTargetStates = editorViewController.editor.getSupportedTargetConversionState(nil)
            try editorViewController.editor.convert(nil, targetState: supportedTargetStates[0].value)
        } catch {
            print("Error while converting : " + error.localizedDescription)
        }
    }
    
    
    @IBAction func exportButtonWasTouchedUpInside(_ sender: Any) {
        do {
            let text = try editorViewController.editor.export_(nil, mimeType: IINKMimeType.text)
           
            let alert = UIAlertController(title: "MyScript", message: text, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.destructive, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } catch {
            print("Error while export : " + error.localizedDescription)
        }
    }
    
}

//
//  ResultsHTMLViewController.swift
//  Diagnostics
//
//  Created by Константин on 19.07.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import Alamofire
import WebKit

class ResultsHTMLViewController: UIViewController, WKNavigationDelegate, UIScrollViewDelegate {
    
    class func makeOne(_ onWebloaded: @escaping (ResultsHTMLViewController?)->Void)  {
        let new = UIStoryboard(name: "Results", bundle: DiagnosticsShared.bundle).instantiateViewController(withIdentifier: "ResultsHTMLViewController") as! ResultsHTMLViewController
        new.modalTransitionStyle = .crossDissolve
        new.modalPresentationStyle = .overCurrentContext
        new.onWebloaded = onWebloaded
        new.finishExamworkIfNeeded {
            new.createWeb {
                new.load()
            }
        }
    }
    
    private func finishExamworkIfNeeded(completion: @escaping ()->Void) {
        guard let workId = SessionService.shared.currentExamWork?.id, SessionService.shared.currentExamWork?.isFinished == false else { completion(); return }
        let status = SessionService.shared.currentExamWork!.getStatus()
        
        Server.shared.finishExamWork(with: workId, status: status, completion: {
                completion()
            return
        })
    }
    
    @IBOutlet weak var webSuperview: UIView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var fioLabel: UILabel!
    @IBOutlet weak var startNewButton: DUIButton!
    
    fileprivate var webView: Web!
    private var estimatedProgressObserver: NSKeyValueObservation?
    
    
    func createWeb(completion: @escaping ()->Void) {
        guard webView == nil else { completion(); return }
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        asyncAfter(50) { [weak self] in
            printAndLog("results self?.webSuperview.bounds = \(self?.webSuperview.bounds)", isError: false)
            Web.make(frame: self!.webSuperview.bounds, changeMetas: false, onCookiesSet: { [weak self] (createdWeb) in
    
                self?.webView = createdWeb
                printAndLog("results onCookiesSet called", isError: false)

                asyncAfter { [weak self] in
                    self?.webSuperview.addSubview(self!.webView)
                    self?.webView.navigationDelegate = self
                    self?.webView.scrollView.delegate = self
                    self?.webView.scrollView.bounces = false
                    self?.webView.scrollView.alwaysBounceVertical = false
                    self?.webView.scrollView.alwaysBounceHorizontal = false
                    self?.webView.scrollView.showsHorizontalScrollIndicator = false
                    self?.webView.setZoomEnabled(enabled: false)
                    self?.webView.scrollView.delegate = self
                    
                    self?.estimatedProgressObserver = self!.webView.observe(\.estimatedProgress, options: [.new]) { [weak self] (webView, _) in
                        let prrogress = Float(webView.estimatedProgress)
                        self?.progressView.setProgress(prrogress, animated: true)
                        self?.progressView.isHidden = prrogress >= 1
                        self?.onWebloaded?(self)
                        self?.onWebloaded = nil
                    }
                    completion()
                }
            })
        }
        
    }
    
    private var onWebloaded: ((ResultsHTMLViewController?)->Void)?=nil
    
    public var currentUser: User {
        get {
            return Server.shared.currentUser
        }
    }

    
    func load() {
        WebSuperview.resetOnLogoutNotification()
        
        if let url = Server.shared.getResultURL(id: SessionService.shared.currentExamWork?.id) {
            DiagnosticsShared.instance.delegate?.diagnostics_shouldShowHUD(with: "Загрузка результатов...", progress: nil)

            let request = URLRequest(url: url)
            printAndLog("Results load started", "url: \(url)", "request: \(request)", "headers: \(request.allHTTPHeaderFields)")
            webView.load(request)
        } else {
            showErrorAlert()
            printAndLog("can't create results url", "examwork id = \(SessionService.shared.currentExamWork?.id)")
        }
    }
    
    @IBAction func startNewTapped(_ sender: DUIButton) {
        webView?.clear()
        webView?.removeFromSuperview()
        webView = nil
        WebSuperview.resetOnLogoutNotification()
        ConnectionService.shared.logout()
        SessionService.shared.logout()
        DiagnosticsShared.instance.delegate?.diagnostics_completed()
    }
    
    
    private func showErrorAlert() {
        let alert = CustomAlertController.make(title: "Не удалось загрузить результаты", body: "Попробуйте обновить страницу", action: CustomAlertController.CustomAlertAction(title: "Обновить", type: .normal, action: { [weak self] in
            self?.load()
            return
        }), needsCancel: false)
        UIApplication.presentOnTop(vc: alert, animated: true)
    }
    
    @IBAction func reloadTapped(_ sender: UIButton) {
        load()
    }
    

    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.webView?.setZoomEnabled(enabled: false)
        
        if let js = SessionService.shared.mobileHelperJS {
            printAndLog("mobile helper found", isError: false)
            webView.evaluateJavaScript(js) { [weak webView] (response, error) in
                printAndLog("mobile helper evaluatejs callback called", "response: \(response)", "error: \(error)", isError: false)
                asyncAfter {
                    webView?.evaluateJavaScript(JSRequests.mobileHelper + JSRequests.disableSelection.rawValue)
                    webView?.evaluateJavaScript(JSRequests.mobileHelper + JSRequests.updateAppearance.rawValue)
                    asyncAfter(1000, {
                        DiagnosticsShared.instance.delegate?.diagnostics_shouldHideHUD()
                    })
                }
            }
        } else {
            printAndLog("mobile helper not found")
            asyncAfter(1000, {
                DiagnosticsShared.instance.delegate?.diagnostics_shouldHideHUD()
            })
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        printAndLog("results loading failed", "error = \(error.localizedDescription)")
        
        asyncAfter { [weak self] in
            DiagnosticsShared.instance.delegate?.diagnostics_shouldHideHUD()
            self?.showErrorAlert()
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return nil
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        webView.setZoomEnabled(enabled: false)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentOffset.y), animated: false)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
}



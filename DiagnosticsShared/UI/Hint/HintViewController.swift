//
//  HintViewController.swift
//  DiagnosticsShared
//
//  Created by Pavel Kurta on 21.12.2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import UIKit
import WebKit

class HintViewController: UIViewController {

    @IBOutlet var webView: WKWebView!
    
    @IBOutlet var closeButton: UIButton!
    @IBOutlet var inOutButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.layer.borderColor = UIColor(hex: "326AA2").cgColor
        self.view.layer.masksToBounds = true
        self.view.layer.borderWidth = 1.0
        self.view.layer.cornerRadius = 5.0
        
        self.webView.layer.borderColor = UIColor(hex: "326AA2").withAlphaComponent(0.25).cgColor
        self.webView.layer.masksToBounds = true
        self.webView.layer.borderWidth = 1.0
        self.webView.layer.cornerRadius = 5.0

    }

}

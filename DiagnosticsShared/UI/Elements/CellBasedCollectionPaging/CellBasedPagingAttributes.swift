//
//  CellBasedPagingAttributes.swift
//  CulTourist
//
//  Created by Константин on 18.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit


class CellBasedPagingAttributes: UICollectionViewLayoutAttributes {
    
    
    override func copy(with zone: NSZone? = nil) -> Any {
        let attribute = super.copy(with: zone) as! CellBasedPagingAttributes
        return attribute
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let object = object as? CellBasedPagingAttributes else { return false }
        return super.isEqual(object)
    }
}


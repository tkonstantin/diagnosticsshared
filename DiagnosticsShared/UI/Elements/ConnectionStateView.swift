//
//  ConnectionStateView.swift
//  Diagnostics
//
//  Created by Константин on 01.03.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

extension connectionStates {
    fileprivate var title: String {
        get {
            switch self {
            case .connected: return "соединение\nс сервером установлено"
            case .disconnected: return "соединение\nс сервером потеряно"
            case .error: return "ошибка\nсоединения"
            case .noNetwork: return "отсутствует\nподключение к сети"
            case .preparing: return "подключение\nк серверу ..."
            case .alert: return "плохое\nсоединение"
            }
        }
    }
    
    fileprivate var color: UIColor {
        get {
            switch self {
            case .connected: return UIColor(hex: "1BDB77")
            case .disconnected: return .red
            case .error: return .red
            case .noNetwork: return .red
            case .preparing: return .lightGray
            case .alert: return UIColor(hex: "FFDD00")
            }
        }
    }
}


class ConnectionStateView: UIView {
    
    @IBOutlet weak var stateLabel: UILabel! {
        didSet {
            self.stateLabel?.text = connectionStates.preparing.title
        }
    }
    
    @IBOutlet weak var stateColorView: UIView! {
        didSet {
            self.stateColorView?.backgroundColor = connectionStates.preparing.color
        }
    }
    
    
    lazy var subscriptionBlock: ((connectionStates)->Void)? = { [weak self] (state: connectionStates) in
        self?.update(with: state)
    }

    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        if newSuperview == nil {
            subscriptionBlock = nil
        } else {
            ConnectionService.shared.subscribeForUpdates(subscriptionBlock)
        }
    }
    
    override func removeFromSuperview() {
        super.removeFromSuperview()
        subscriptionBlock = nil
    }
    
    
    private func update(with state: connectionStates) {
        asyncAfter {
            self.stateLabel?.text = state.title
            self.stateColorView?.backgroundColor = state.color
        }
    }
}

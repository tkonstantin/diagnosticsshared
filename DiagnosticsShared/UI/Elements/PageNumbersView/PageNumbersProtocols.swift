//
//  PageNumbersProtocols.swift
//  Diagnostics
//
//  Created by Константин on 13.09.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation

@objc protocol PageNumbersDataSource: class {
    func numberOfPages(_ pageNumbersView: PageNumbersView) -> Int
    func selectedPageNumber(_ pageNumbersView: PageNumbersView) -> Int
    func pageNumbersView(_ pageNumbersView: PageNumbersView, isAnsweredAt index: Int) -> Bool
}

@objc protocol PageNumbersDelegate: class {
    func pageNumbersView(_ pageNumbersView: PageNumbersView, didSelectItemAt index: Int)
}


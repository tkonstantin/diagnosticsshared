//
//  PageNumbersView.swift
//  Diagnostics
//
//  Created by Константин on 13.09.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

class PageNumbersView: UIView {
    
    @IBOutlet private weak var backButton: UIButton!
    @IBOutlet private weak var forwardButton: UIButton!
    
    @IBOutlet private weak var collectionView: PageNumberCollectionView! {
        didSet {
            collectionView?.register(UINib(nibName: "PageNumberCell", bundle: DiagnosticsShared.bundle), forCellWithReuseIdentifier: "item")
            collectionView?.register(UINib(nibName: "PageDotCell", bundle: DiagnosticsShared.bundle), forCellWithReuseIdentifier: "dots")
        }
    }
    
    weak var dataSource: PageNumbersDataSource!
    weak var delegate: PageNumbersDelegate!
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        self.updateVisibleQuestionsRange()
        self.collectionView?.reloadData()
    }
    
    fileprivate var numberOfPages: Int {
        get {
            guard dataSource != nil else {
                printAndLog("PageNumbersDataSource is not set")
                return 0
            }
            return dataSource.numberOfPages(self)
        }
    }
    
    private var startingQuestionIndex: Int = 1
    private var currentVisibleQuestions: [Int] = []
    
    
    @IBAction private func backTapped(_ sender: UIButton) {
        guard numberOfPages != 0 else { return }
        
        startingQuestionIndex = max(startingQuestionIndex - maxNumberOfItems + 2, 1)
        updateNavButtons()
        updateVisibleQuestionsRange()
    }
    
    @IBAction private func forwardTapped(_ sender: UIButton) {
        guard numberOfPages != 0 else { return }
        
        startingQuestionIndex = max(1, min(startingQuestionIndex + maxNumberOfItems - 2, numberOfPages - maxNumberOfItems + 1))
        
        updateNavButtons()
        updateVisibleQuestionsRange()
    }
    
    private func updateNavButtons() {
        forwardButton.isEnabled = startingQuestionIndex != numberOfPages - maxNumberOfItems + 1
        backButton.isEnabled = startingQuestionIndex != 1
    }
    
    func clearGroupData() {
        self.collectionView.indexes = []
        self.collectionView.setNeedsDisplay()
    }
    
    func reloadData() {
        self.updateVisibleQuestionsRange()
        if !self.currentVisibleQuestions.contains(dataSource.selectedPageNumber(self)) {
            startingQuestionIndex = max(1, min(dataSource.selectedPageNumber(self), numberOfPages - maxNumberOfItems + 1))
            updateNavButtons()
            updateVisibleQuestionsRange()
        }
    }
        
    private lazy var maxNumberOfItems: Int = {
        self.setNeedsLayout()
        self.layoutIfNeeded()
        let itemWidth = 46 + 10
        let collectionWidth = Int(collectionView.bounds.width)
        return collectionWidth/itemWidth
    }()

    private func updateVisibleQuestionsRange() {
        guard numberOfPages != 0 else { return }
        
        let previousRange = currentVisibleQuestions
        
        let withoutServices = min(maxNumberOfItems - 2, numberOfPages - 2)
        let leftPartMax = withoutServices + startingQuestionIndex - 1
        
        if leftPartMax >= startingQuestionIndex {
            currentVisibleQuestions = Array(startingQuestionIndex...leftPartMax)
        } else {
            currentVisibleQuestions = []
        }
        
        if leftPartMax >= numberOfPages - 2 {
            currentVisibleQuestions += [numberOfPages - 1, numberOfPages]
        } else {
            currentVisibleQuestions += [0, numberOfPages]
        }
        
        
        if previousRange != currentVisibleQuestions {
            self.backButton.isHidden = numberOfPages <= maxNumberOfItems
            self.forwardButton.isHidden = numberOfPages <= maxNumberOfItems
        }
        
        if numberOfPages >= maxNumberOfItems && previousRange != currentVisibleQuestions {
            if let flowLayout = self.collectionView.collectionViewLayout as?  UICollectionViewFlowLayout {
                let count = CGFloat(self.maxNumberOfItems)
                let spacing = self.collectionView.bounds.width/count - 46
                flowLayout.minimumInteritemSpacing = spacing
                flowLayout.minimumLineSpacing = spacing
                flowLayout.sectionInset.left = spacing / 2.0
                flowLayout.sectionInset.right = spacing / 2.0
                self.collectionView.reloadData()
                
                let test = SessionService.shared.currentTest
                var indexes: [(start: Int, end: Int, group: TaskGroup?)] = []
                var startIndex: Int?
                var startTaskGroup: TaskGroup?
                var endIndex: Int?
                for i in 0 ..< currentVisibleQuestions.count {
                    let item = currentVisibleQuestions[i]
                    if item == 0 {
                        //многоточие - всегда предпоследнее
                        if startIndex != nil {
                            if i > 0 {
                                let itemPrev = currentVisibleQuestions[i - 1]
                                let checkItem = itemPrev + 1
                                let checkQuestion = test.questions[checkItem - 1]
                                if checkQuestion.taskGroupId != nil && startTaskGroup != nil && checkQuestion.taskGroupId! == startTaskGroup!.id {
                                    endIndex = i
                                }
                            }
                        }
                        
                        //сразу проверить последний элемент
                        let itemLast = currentVisibleQuestions[i + 1]
                        let checkQuestion = test.questions[itemLast - 1]
                        if checkQuestion.taskGroupId != nil {
                            if endIndex != nil {
                                endIndex = i + 1
                            } else {
                                startIndex = i + 1
                                endIndex = i + 1
                            }
                        }
                        
                        break
                    }
                    let q = (item <= test.questions.count) ? test.questions[item - 1] : nil
                    if q is QuestionInfo {
                        if startIndex != nil {
                            indexes.append((start: startIndex!, end: endIndex!, group: startTaskGroup))
                        }
                        startIndex = i
                        startTaskGroup = (q as! QuestionInfo).taskGroup
                        endIndex = i
                        continue
                    } else {
                        if q?.taskGroupId != nil && startTaskGroup != nil && q?.taskGroupId! == startTaskGroup!.id {
                            endIndex = i
                        } else if startIndex != nil {
                            indexes.append((start: startIndex!, end: endIndex!, group: startTaskGroup))
                            startIndex = nil
                            startTaskGroup = nil
                            endIndex = nil
                        } else if q?.taskGroupId != nil && startIndex == nil { //случай когда кнопка i - впереди спрятана
                            startIndex = i
                            startTaskGroup = test.taskGroups.first(where: { $0.id == q?.taskGroupId! })
                            endIndex = i
                        }
                    }
                }
                if startIndex != nil {
                    indexes.append((start: startIndex!, end: endIndex!, group: startTaskGroup))
                    startIndex = nil
                    startTaskGroup = nil
                    endIndex = nil
                }
                
                self.collectionView.spacing = spacing / 2.0
                self.collectionView.indexes = indexes
                self.collectionView.setNeedsDisplay()
            }
        } else {
            self.collectionView.reloadData()
            
        }
        
    }
    
}


extension PageNumbersView: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return currentVisibleQuestions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard currentVisibleQuestions.count > indexPath.item else {
            return collectionView.dequeueReusableCell(withReuseIdentifier: "dots", for: indexPath)
        }
        
        let current = currentVisibleQuestions[indexPath.item]
        
        if current != 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PageNumberCell.id, for: indexPath) as! PageNumberCell
            cell.update(with: current, isSelected: dataSource.selectedPageNumber(self) == current, isAnswered: dataSource.pageNumbersView(self, isAnsweredAt: current - 1))
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dots", for: indexPath)
            return cell
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         guard currentVisibleQuestions.count > indexPath.item, currentVisibleQuestions[indexPath.item] != 0 else {
            self.forwardTapped(self.forwardButton)
            return
        }
        
        if dataSource.selectedPageNumber(self) != currentVisibleQuestions[indexPath.item] {
            delegate.pageNumbersView(self, didSelectItemAt: currentVisibleQuestions[indexPath.item])
        }
    }
    
    
}

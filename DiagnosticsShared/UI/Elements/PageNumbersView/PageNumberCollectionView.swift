//
//  PageNumberCollectionView.swift
//  DiagnosticsShared
//
//  Created by Pavel Kurta on 24.12.2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import UIKit

class PageNumberCollectionView: UICollectionView {

    var spacing: CGFloat = 10.0
    var indexes: [(start: Int, end: Int, group: TaskGroup?)] = []
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        if let context = UIGraphicsGetCurrentContext() {
            let color = UIColor(hex: "146CA7")
            context.setStrokeColor(color.cgColor)
            context.setLineWidth(2)
            
            for index in self.indexes {
                let delta: CGFloat = 46.0 + 2.0 * spacing
                let xStartPoint: CGFloat = spacing + CGFloat(index.start) * delta
                let xEndPoint: CGFloat = spacing + CGFloat(index.end) * delta + 46.0
                context.move(to: CGPoint(x: xStartPoint,
                                         y: bounds.height - 20.0))
                context.addLine(to: CGPoint(x: xEndPoint,
                                            y: bounds.height - 20.0))
                context.strokePath()
                
                //Надпись
                if index.group?.name != nil {
                    let paragraphStyle = NSMutableParagraphStyle()
                    paragraphStyle.alignment = .center
                    let attributes = [
                        NSAttributedString.Key.paragraphStyle: paragraphStyle,
                        NSAttributedString.Key.font: UIFont.systemFont(ofSize: 10.0),
                        NSAttributedString.Key.foregroundColor: color
                    ]
                    let atrStr = NSAttributedString(string: index.group!.name!, attributes: attributes)
                    if atrStr.size().width < (xEndPoint - xStartPoint) {
                        atrStr.draw(in: CGRect(x: xStartPoint,
                                               y: bounds.height - 20.0,
                                               width: xEndPoint - xStartPoint,
                                               height: 20.0))
                    }
                }
               
            }
        }
    }
    
}

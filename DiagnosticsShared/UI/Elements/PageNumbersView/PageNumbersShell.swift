//
//  PageNumbersSuperview.swift
//  Diagnostics
//
//  Created by Константин on 13.09.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

class PageNumbersShell: UIView {
    
    @IBOutlet weak var dataSource: PageNumbersDataSource!
    @IBOutlet weak var delegate: PageNumbersDelegate!
    
    private weak var view: PageNumbersView!
    
    func clearGroupData() {
        view?.clearGroupData()
    }
    
    func reloadData() {
        view?.reloadData()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard view == nil else {
            view.delegate = delegate
            view.dataSource = dataSource
            return
        }
        
        view = makeView(delegate: delegate, dataSource: dataSource)
        view.frame = self.bounds
        self.addSubview(view)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func makeView(delegate: PageNumbersDelegate, dataSource: PageNumbersDataSource) -> PageNumbersView {
        let new = UINib(nibName: "PageNumbersView", bundle: DiagnosticsShared.bundle).instantiate(withOwner: nil, options: nil).first as! PageNumbersView
        new.delegate = delegate
        new.dataSource = dataSource
        return new
    }
    
}





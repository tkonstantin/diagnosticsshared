//
//  TestItemCell.swift
//  Diagnostics
//
//  Created by Константин on 06.02.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

class PageNumberCell: UICollectionViewCell {
    
    static let id = "item"
    private var defaultColor = UIColor(hex: "999999")
    private var isAnswered = false
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var shadowedView: ShadowedView!
    
    
    func update(with item: Int, isSelected: Bool, isAnswered _isAnswered: Bool) {
        let test = SessionService.shared.currentTest
        let question = test.questions[item - 1]
        
        isAnswered = _isAnswered
        if test.taskGroups.count == 0 {
            label.text = String(question.numberInKim)
        } else {
            label.text = question.numberInGroup ?? String(question.numberInKim)
        }
        label.font = UIFont.systemFont(ofSize: 18, weight: isSelected ? .regular : .light)
        
        let customBlueColor = UIColor(hex: "146CA7")
        
        if isAnswered {
            shadowedView.borderWidth = isSelected ? 2 : 1
            shadowedView.borderColor = isSelected ? UIColor(hex: "30017D") : customBlueColor
            shadowedView.backgroundColor = customBlueColor
            label.textColor = .white
        } else {
            shadowedView.borderWidth = isSelected ? 1 : 0
            shadowedView.borderColor = isSelected ? customBlueColor : .white
            shadowedView.backgroundColor = .white
            label.textColor = isSelected ? customBlueColor : .gray
            
            if let work = SessionService.shared.currentExamWork,
                let user = Server.shared.currentUser {
                 if question is QuestionInfo {
                    label.text = "i"
                } else {
                    let htmlId = question.id
                    let isPublic = work.isPublicValues[htmlId]
                    if isPublic == 0 && user.IsAnonymous {
                        self.shadowedView.backgroundColor = UIColor(white: 0.85, alpha: 1.0)
                    }
                }
            }
        }
        
    }
}

//
//  CompletionStatusLabel.swift
//  Diagnostics
//
//  Created by Константин on 19/02/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import UIKit

class CompletionStatusLabel: UILabel {
    
    private(set) static var instance: CompletionStatusLabel?
    
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        CompletionStatusLabel.instance = self
        self.text = ConnectionService.shared.hasConnection ? nil : "Ожидание сети..."
    }
    
    class func set(status: String?, progress: Float?=nil) {
        asyncAfter { 
            if let instance = CompletionStatusLabel.instance {
                instance.text = status
            } else {
                DiagnosticsShared.instance.delegate?.diagnostics_shouldShowHUD(with: status, progress: progress)
            }
        }
        
    }
}

//
//  ProgressView.swift
//  UPS
//
//  Created by Константин on 10.07.2018.
//  Copyright © 2018 Константин Черкашин. All rights reserved.
//

import UIKit

public class ProgressView: UIView {
    
    private var progressView: UIView!
    
    override public func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.cornerRadius = self.bounds.height/2
    }
    
    public func set(progress: CGFloat, duration: TimeInterval) {
        
        UIView.commitAnimations()
        
        if progressView == nil {
           progressView = UIView(frame: self.bounds)
            progressView.frame.size.width = 0
            progressView.backgroundColor = self.tintColor
            progressView.clipsToBounds = true
            self.addSubview(progressView)
        }
        
        UIView.animate(withDuration: duration) {
            self.progressView.frame.size.width = self.bounds.width*progress
            self.progressView.setNeedsLayout()
            self.progressView.layoutIfNeeded()
        }
        
    }
}

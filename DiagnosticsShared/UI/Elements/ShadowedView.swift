//
//  ShadowedView.swift
//  Heloo2017
//
//  Created by Константин on 24.09.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

public class ShadowedView: UIView {
    
    
    @IBInspectable public var color: String = "36393C" {
        didSet {
            updateShadows()
        }
    }
    
    
    @IBInspectable public var shadowRadius: CGFloat = 10 {
        didSet {
            updateShadows()
        }
    }
    
    override public func awakeFromNib() {
        updateShadows()
    }
    
    private func updateShadows() {
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOpacity = 0.5
        self.layer.shadowColor = UIColor(hex: color).cgColor
        self.layer.masksToBounds = false
//        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: self.cornerRadius, height: self.cornerRadius)).cgPath
    }
}


extension UIColor {
    convenience init(hex: String) {
        let color: UIColor = {
            var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
            
            if (cString.hasPrefix("#")) {
                cString = String(cString.suffix(from: cString.index(cString.startIndex, offsetBy: 1)))
            }
            
            if ((cString.count) != 6) {
                return UIColor.gray
            }
            
            var rgbValue:UInt32 = 0
            Scanner(string: cString).scanHexInt32(&rgbValue)
            
            return UIColor(
                red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                alpha: CGFloat(1.0)
            )
        }()
        
        self.init(cgColor: color.cgColor)
    }
}


//
//  Web.swift
//  Diagnostics
//
//  Created by Константин on 30/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import WebKit

public class Web: WKWebView, WKNavigationDelegate, WKUIDelegate, WKScriptMessageHandler, UIScrollViewDelegate {
    
    public var isEscaping = false
    
    public class func make(frame: CGRect, changeMetas: Bool = true, onCookiesSet: @escaping (Web?)->Void) {
        let wkUController = WKUserContentController()
        
        if changeMetas {
            let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
            let userScript = WKUserScript(source: jscript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
            wkUController.addUserScript(userScript)
        }
        
        let config = WKWebViewConfiguration()
        
        config.userContentController = wkUController
        
        
        if #available(iOS 11.0, *) {
            var setCookiesCount = 0
            let allCookiesCount = Server.shared.cookies.count
            let store = WKWebsiteDataStore.nonPersistent()
            
            for cookie in Server.shared.cookies {
                store.httpCookieStore.setCookie(cookie) {
                    setCookiesCount += 1
                    if setCookiesCount == allCookiesCount {
                        config.websiteDataStore = store
                        
                        let newWeb = Web(frame: frame, configuration: config)
                        newWeb.configuration.userContentController.add(newWeb, name: "iFrameLoadCallback")
                        newWeb.configuration.userContentController.add(newWeb, name: "iFrameLoadingHandler")
                        newWeb.configuration.userContentController.add(newWeb, name: "iFrameErrorHandler")
                        newWeb.configuration.userContentController.add(newWeb, name: "iFrameBeginLoadProcess")
                        newWeb.configuration.userContentController.add(newWeb, name: "initializeVoiceAndHandwriting")
                        
                        newWeb.configuration.websiteDataStore.httpCookieStore.getAllCookies({ [weak newWeb] (allCookies) in
                            printAndLog("results allCookies \(allCookies)", isError: false)
                            guard let `newWeb` = newWeb else { onCookiesSet(nil); return }
                            asyncAfter {
                                newWeb.navigationDelegate = newWeb
                                newWeb.uiDelegate = newWeb
                                newWeb.scrollView.delegate = newWeb
                                newWeb.setZoomEnabled(enabled: false)
                                
                                onCookiesSet(newWeb)
                                return;
                            }
                        })
                        
                    }
                }
            }
        } else {
            var finalString = ""
            for cookie in Server.shared.cookies {
                finalString.append("document.cookie = '\(cookie.name)=\(cookie.value)';")
            }
            let cookieUserScript = WKUserScript(source: finalString, injectionTime: .atDocumentStart, forMainFrameOnly: false)
            config.userContentController.addUserScript(cookieUserScript)
            asyncAfter {
                let newWeb = Web(frame: frame, configuration: config)
                newWeb.configuration.userContentController.add(newWeb, name: "iFrameLoadCallback")
                newWeb.configuration.userContentController.add(newWeb, name: "iFrameLoadingHandler")
                newWeb.configuration.userContentController.add(newWeb, name: "iFrameErrorHandler")
                newWeb.configuration.userContentController.add(newWeb, name: "iFrameBeginLoadProcess")
                newWeb.configuration.userContentController.add(newWeb, name: "initializeVoiceAndHandwriting")
                newWeb.navigationDelegate = newWeb
                newWeb.uiDelegate = newWeb
                
                onCookiesSet(newWeb)
                return;
            }
            
        }
    }

    override public func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        if #available(iOS 11.0, *) {
            self.scrollView.subviews.forEach({$0.interactions.removeAll()})
        }
    }
    
    fileprivate var messageHandlers: [String: ()->Void] = [:]
    fileprivate var jsLoadHandler: ((String)->Void)?=nil
    fileprivate var jsLoadErrorHandler: ((String?, String?)->Void)?=nil
    
    func startListening(id: String, completion: @escaping ()->Void) {
        messageHandlers[id] = completion
    }
    
    func startListeningJSLoad(completion: @escaping (String)->Void) {
        jsLoadHandler = completion
    }
    
    func startListeningJSLoadErrors(completion: @escaping (String?, String?)->Void) {
        jsLoadErrorHandler = completion
    }
    
    
    public func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        
        if message.name == "iFrameLoadingHandler" {
            if let bodyDict = message.body as? [String: AnyObject] {
                let id = bodyDict["id"] as? String ?? String(bodyDict["id"] as? Int ?? 0)
                if id != "0" {
                    messageHandlers[id]?()
                }
            }
        } else if message.name == "iFrameBeginLoadProcess" {
            //debug
            printAndLog("!!! iFrameBeginLoadProcess called")
        } else if message.name == "iFrameErrorHandler" {
            printAndLog("!!! iFrameErrorHandler called")
            if let bodyDict = message.body as? [String: AnyObject] {
                let id = bodyDict["id"] as? String ?? String(bodyDict["id"] as? Int ?? 0)
                jsLoadErrorHandler?(bodyDict["error"] as? String, id)
                printAndLog("iFrameErrorHandler called", "body = \(message.body)", "name = \(message.name)", "frameInfo = \(message.frameInfo)")
            } else {
                jsLoadErrorHandler?((message.body as? [String: String])?.values.first, nil)
                printAndLog("iFrameErrorHandler called", "body = \(message.body)", "name = \(message.name)", "frameInfo = \(message.frameInfo)")
            }
        } else if message.name == "iFrameLoadCallback" {
            printAndLog("iFrameLoadCallback")
            if let bodyDict = message.body as? [String: AnyObject] {
                print("iFrameLoadCallback html = \(String(describing: bodyDict["html"] as? String))")
                let id = bodyDict["id"] as? String ?? String(bodyDict["id"] as? Int ?? 0)
                if id != "0" {
                    printAndLog("!!! iFrameLoadCallback ID == \(id) -> FINISH")
                    messageHandlers[id]?()
                    jsLoadHandler?(id)
                } else {
                    printAndLog("!!! iFrameLoadCallback ID == 0 -> STOP")
                }
            }
        } else if message.name == "initializeVoiceAndHandwriting" {
            if let bodyDict = message.body as? [String: AnyObject],
                let value = bodyDict["value"] as? String {
                
                let id = bodyDict["id"] as? String ?? String(bodyDict["id"] as? Int ?? 0)
                    
                var type = VoiceHandwritingAccessType.none
                switch value {
                case "1": type = .voice
                case "2": type = .handwriting
                case "3": type = .all
                default:
                    break
                }
                SessionService.shared.currentTest.initializeVoiceAndHandwritingInfo[id] = type
               
            }
        }
        
    }
    
    override public func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
    
    
    private(set) var pendingHTMLNavigation: [WKNavigation : (()->Void)] = [:]
    
   public func loadHTMLString(_ string: String, baseURL: URL?, completionHandler: @escaping ()->Void) {
        if let nav = super.loadHTMLString(string, baseURL: baseURL) {
            pendingHTMLNavigation[nav] = completionHandler
        }
    }
    
   public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        for (key, value) in pendingHTMLNavigation {
            if key == navigation {
                value()
            }
        }
    }
    
    
    @available(iOS 10.0, *)
    public func webView(_ webView: WKWebView, shouldPreviewElement elementInfo: WKPreviewElementInfo) -> Bool {
        return false
    }
    
    
    public func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        guard !isEscaping else {
            completionHandler();
            return
        }
        let alert = CustomAlertController.make(title: nil, body: message.htmlToString(), action: CustomAlertController.CustomAlertAction(title: "OK", type: .normal, action: {
            completionHandler()
            return
        }), needsCancel: false)
        UIApplication.presentOnTop(vc: alert, animated: true)
    }
    
    
    public func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
        guard !isEscaping else {
            completionHandler(true);
            return
        }
        
        let alert = CustomAlertController.make(title: nil, body: message.htmlToString(), action: CustomAlertController.CustomAlertAction(title: "OK", type: .normal, action: {
            completionHandler(true)
            return
        }), needsCancel: true, cancelAction: {
            completionHandler(false)
            return
        })
        
        UIApplication.presentOnTop(vc: alert, animated: true)
        
    }
    
    public func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
        
        guard !isEscaping else {
            completionHandler(nil);
            return
        }
        
        let alert = UIAlertController(title: nil, message: prompt.htmlToString(), preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.text = defaultText?.htmlToString()
        }
        alert.addAction(UIAlertAction(title: "ОК", style: .default, handler: { (_) in
            completionHandler(alert.textFields?.first?.text ?? defaultText)
        }))
        alert.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: { (_) in
            completionHandler(nil)
        }))
        
        UIApplication.presentOnTop(vc: alert, animated: true)
        
    }
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(.allow)
    }
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        decisionHandler(.allow)
    }
    
    private func _resizeWiris() {
        DispatchQueue.global(qos: .utility).async { [weak self] in
            (self as? DWKWebView)?.resizeWiris()
        }
    }
    
    
    public func setScrollEnabled(enabled: Bool) {
        self.scrollView.isScrollEnabled = enabled
        self.scrollView.panGestureRecognizer.isEnabled = enabled
        self.scrollView.pinchGestureRecognizer?.isEnabled = enabled
        self.scrollView.bounces = enabled
        
        if !enabled {
            for gesture in (self.gestureRecognizers ?? []) {
                if gesture is UIPanGestureRecognizer || gesture is UIPinchGestureRecognizer || gesture is UIRotationGestureRecognizer || gesture is UISwipeGestureRecognizer || gesture is UIScreenEdgePanGestureRecognizer || gesture is UILongPressGestureRecognizer {
                    self.removeGestureRecognizer(gesture)
                } else if gesture is UITapGestureRecognizer, let tap = gesture as? UITapGestureRecognizer, tap.numberOfTapsRequired == 2, tap.numberOfTouchesRequired == 1 {
                    self.removeGestureRecognizer(gesture)
                }
            }
            
            for subview in self.subviews {
                if let subview = subview as? UIScrollView {
                    subview.isScrollEnabled = enabled
                    subview.bounces = enabled
                    subview.panGestureRecognizer.isEnabled = enabled
                    subview.pinchGestureRecognizer?.isEnabled = enabled
                }
                
                for gesture in (subview.gestureRecognizers ?? []) {
                    if gesture is UIPanGestureRecognizer || gesture is UIPinchGestureRecognizer || gesture is UIRotationGestureRecognizer || gesture is UISwipeGestureRecognizer || gesture is UIScreenEdgePanGestureRecognizer || gesture is UILongPressGestureRecognizer {
                        subview.removeGestureRecognizer(gesture)
                    } else if gesture is UITapGestureRecognizer, let tap = gesture as? UITapGestureRecognizer, tap.numberOfTapsRequired == 2, tap.numberOfTouchesRequired == 1 {
                        subview.removeGestureRecognizer(gesture)
                    }
                }
                
                for subScrollView in subview.subviews {
                    for gesture in (subScrollView.gestureRecognizers ?? []) {
                        if gesture is UIPanGestureRecognizer || gesture is UIPinchGestureRecognizer || gesture is UIRotationGestureRecognizer || gesture is UISwipeGestureRecognizer || gesture is UIScreenEdgePanGestureRecognizer || gesture is UILongPressGestureRecognizer {
                            subScrollView.removeGestureRecognizer(gesture)
                        } else if gesture is UITapGestureRecognizer, let tap = gesture as? UITapGestureRecognizer, tap.numberOfTapsRequired == 2, tap.numberOfTouchesRequired == 1 {
                            subScrollView.removeGestureRecognizer(gesture)
                        }
                    }
                }
            }
        }
    }
    
    public func setZoomEnabled(enabled: Bool) {
        self.scrollView.pinchGestureRecognizer?.isEnabled = enabled
        
        if !enabled {
            for gesture in (self.gestureRecognizers ?? []) {
                if gesture is UIPinchGestureRecognizer || gesture is UIRotationGestureRecognizer || gesture is UILongPressGestureRecognizer  {
                    self.removeGestureRecognizer(gesture)
                } else if gesture is UITapGestureRecognizer, let tap = gesture as? UITapGestureRecognizer, tap.numberOfTapsRequired == 2, tap.numberOfTouchesRequired == 1 {
                    self.removeGestureRecognizer(gesture)
                }
            }
            
            for subview in self.subviews {
                if let subview = subview as? UIScrollView {
                    subview.pinchGestureRecognizer?.isEnabled = enabled
                }
                
                for gesture in (subview.gestureRecognizers ?? []) {
                    if  gesture is UIPinchGestureRecognizer || gesture is UIRotationGestureRecognizer || gesture is UILongPressGestureRecognizer {
                        subview.removeGestureRecognizer(gesture)
                    } else if gesture is UITapGestureRecognizer, let tap = gesture as? UITapGestureRecognizer, tap.numberOfTapsRequired == 2, tap.numberOfTouchesRequired == 1 {
                        subview.removeGestureRecognizer(gesture)
                    }
                }
                
                for subScrollView in subview.subviews {
                    for gesture in (subScrollView.gestureRecognizers ?? []) {
                        if gesture is UIPanGestureRecognizer || gesture is UIPinchGestureRecognizer || gesture is UILongPressGestureRecognizer {
                            subScrollView.removeGestureRecognizer(gesture)
                        } else if gesture is UITapGestureRecognizer, let tap = gesture as? UITapGestureRecognizer, tap.numberOfTapsRequired == 2, tap.numberOfTouchesRequired == 1 {
                            subScrollView.removeGestureRecognizer(gesture)
                        }
                    }
                }
            }
        }
    }
    
    
    public func clear() {
        pendingHTMLNavigation = [:]
        messageHandlers = [:]
        jsLoadHandler = nil
        jsLoadErrorHandler = nil
        uiDelegate = nil
        navigationDelegate = nil
        scrollView.delegate = nil
        configuration.userContentController.removeScriptMessageHandler(forName: "iFrameLoadCallback")
        configuration.userContentController.removeScriptMessageHandler(forName: "iFrameLoadingHandler")
        configuration.userContentController.removeScriptMessageHandler(forName: "iFrameErrorHandler")
        configuration.userContentController.removeScriptMessageHandler(forName: "iFrameBeginLoadProcess")
        configuration.userContentController.removeAllUserScripts()
        stopLoading()
        loadHTMLString("", baseURL: nil)
    }

    // disable zooming in webview
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }
    
}

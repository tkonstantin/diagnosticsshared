//
//  WebSuperview.swift
//  Diagnostics
//
//  Created by Константин on 14.09.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import WebKit

public class WebSuperview: UIView {

    //public private(set) static var webView: DWKWebView!
    
    var webView: DWKWebView!
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        
        self.webView?.removeFromSuperview()
        self.webView = nil
        
        DWKWebView.make(frame: UIScreen.main.bounds, onCookiesSet: { createdWeb in
            self.webView = createdWeb
            //createdWeb?.loadHTMLString("<html><head></head><body>Test2</body></html>", baseURL: nil)
            self.webView.translatesAutoresizingMaskIntoConstraints = false
            self.webView?.frame = self.bounds
            self.addSubview(self.webView)
            
            printAndLog("!!! Init WebView")
        })
    }
    
    override public func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        guard newSuperview != nil, self.webView != nil else { return  }
        
        if self.webView?.superview != self {
            if self.webView?.superview != nil {
                self.webView?.removeFromSuperview()
            }
            self.addSubview(self.webView)
        }
        self.webView?.frame = self.bounds
        
    }
    override public func didMoveToSuperview() {
        super.didMoveToSuperview()
        if self.webView?.superview == self {
            self.webView?.frame = self.bounds
        }
        
    }
    
    
    override public func layoutSubviews() {
        self.webView?.frame = self.bounds
        super.layoutSubviews()
    }
    
    fileprivate var isFailed = false
    fileprivate var isLoaded = false
    fileprivate var pendingLoadingCompletion: ((CGFloat, Bool, Bool)->Void)? = nil
    fileprivate var currentProgress: CGFloat?
    
    func subcsribeForLoading(_ completion: @escaping (CGFloat, Bool, Bool)->Void) {
        pendingLoadingCompletion = completion
        
        if isLoaded {
            completion(1, true, false)
        } else if isFailed {
            completion(0, false, true)
        } else {
            if let progress = self.currentProgress {
                completion(progress, false, false)
            }
        }
    }
    
    public func startLoadingWebView(test: Test!, completion: @escaping ()->Void) {

        self.webView?.removeFromSuperview()
        self.webView = nil
        
        DWKWebView.make(frame: UIScreen.main.bounds, onCookiesSet: { createdWeb in
            self.webView = createdWeb
            //createdWeb?.loadHTMLString("<html><head></head><body>Test3</body></html>", baseURL: nil)

            self.webView.translatesAutoresizingMaskIntoConstraints = false
            self.webView?.frame = self.bounds
            self.addSubview(self.webView)
            
            self.applyTest(test)
            completion()

        })
        
    }
    
    private func applyTest(_ test: Test!) {
        printAndLog("!!! applyTest")
        
        self.isFailed = false
        self.isLoaded = false
        
        self.webView?.apply(test: test, onProgress: { progress in
            self.currentProgress = self.pendingLoadingCompletion == nil ? progress  : nil
            self.pendingLoadingCompletion?(progress, false, false)
        }, onFinished: {
            printAndLog("!!! applyTest - onFinished")
            self.isLoaded = true
            self.isFailed = false
            self.pendingLoadingCompletion?(1, true, false)
        }, onFailed: {
            printAndLog("!!! applyTest - onFailed")
            self.isFailed = true
            self.isLoaded = false
            self.pendingLoadingCompletion?(0, false, true)
        })
    }
    
    public class func resetOnLogoutNotification() {
       NotificationCenter.default.post(Notification(name: kNotificationResetOnLogout))
    }
    
    public func resetOnLogout() {
        self.isFailed = false
        self.isLoaded = false
        
        self.webView?.removeFromSuperview()
        self.webView?.clear()
        self.webView = nil
    }

}

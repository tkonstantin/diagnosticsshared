//
//  DWKWebView.swift
//  Diagnostics
//
//  Created by Константин on 14.09.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import WebKit

public class DWKWebView: Web {
    
    override public class func make(frame: CGRect, changeMetas: Bool = true, onCookiesSet: @escaping (DWKWebView?)->Void) {
        let wkUController = WKUserContentController()
        
        if changeMetas {
            let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'); document.getElementsByTagName('head')[0].appendChild(meta);"
            let userScript = WKUserScript(source: jscript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
            wkUController.addUserScript(userScript)
        }
        
        let config = WKWebViewConfiguration()
        
        config.userContentController = wkUController
        
        
        if #available(iOS 11.0, *) {
            var setCookiesCount = 0
            let allCookiesCount = Server.shared.cookies.count
            let store = WKWebsiteDataStore.nonPersistent()
            
            for cookie in Server.shared.cookies {
                store.httpCookieStore.setCookie(cookie) {
                    setCookiesCount += 1
                    if setCookiesCount == allCookiesCount {
                        config.websiteDataStore = store
                        
                        let newWeb = DWKWebView(frame: frame, configuration: config)
                        //newWeb.alpha = 0.2
                        //newWeb.backgroundColor = UIColor.yellow
                        
                        newWeb.evaluateJavaScript("navigator.userAgent", completionHandler: { (response, error) in
                            if let original = response as? String {
                                newWeb.customUserAgent = original + " Safari/604.1 myskillsMobileAgent_v2"
                            }
                        })
                        
                        newWeb.configuration.userContentController.add(newWeb, name: "iFrameLoadCallback")
                        newWeb.configuration.userContentController.add(newWeb, name: "iFrameLoadingHandler")
                        newWeb.configuration.userContentController.add(newWeb, name: "iFrameErrorHandler")
                        newWeb.configuration.userContentController.add(newWeb, name: "iFrameBeginLoadProcess")
                        newWeb.configuration.userContentController.add(newWeb, name: "initializeVoiceAndHandwriting")
                                              
                        newWeb.configuration.websiteDataStore.httpCookieStore.getAllCookies({ (allCookies) in
                            printAndLog("websiteDataStore getAllCookies called", "allCookies: \(allCookies)", isError: false)
                            asyncAfter {
                                newWeb.navigationDelegate = newWeb
                                newWeb.uiDelegate = newWeb
                                
                                onCookiesSet(newWeb)
                            }
                        })
                    }
                }
            }
            
        } else {
            var finalString = ""
            for cookie in Server.shared.cookies {
                finalString.append("document.cookie = '\(cookie.name)=\(cookie.value)';")
            }
            let cookieUserScript = WKUserScript(source: finalString, injectionTime: .atDocumentStart, forMainFrameOnly: false)
            config.userContentController.addUserScript(cookieUserScript)
            DispatchQueue.main.async {
                let newWeb = DWKWebView(frame: frame, configuration: config)
                //newWeb.alpha = 0.4
                //newWeb.backgroundColor = UIColor.red
                
                newWeb.configuration.userContentController.add(newWeb, name: "iFrameLoadCallback")
                newWeb.configuration.userContentController.add(newWeb, name: "iFrameLoadingHandler")
                newWeb.configuration.userContentController.add(newWeb, name: "iFrameErrorHandler")
                newWeb.configuration.userContentController.add(newWeb, name: "iFrameBeginLoadProcess")
                newWeb.configuration.userContentController.add(newWeb, name: "initializeVoiceAndHandwriting")
                newWeb.navigationDelegate = newWeb
                newWeb.uiDelegate = newWeb
                onCookiesSet(newWeb)
            }
            
        }
    }


    
    
    override public func didMoveToSuperview() {
        super.didMoveToSuperview()
        backgroundColor = .white
        self.setScrollEnabled(enabled: false)
    }
    
    
    func change(question: Question!) {
        guard question != nil else { return }
        
        self.evaluate(localJS: .hideIframes)
        
        if let _ = question as? QuestionInfo {
            //ignore - обрабатывается в основном ViewController
        } else {
            self.evaluate(localJS: .showIframe, params: question.id) { [weak self] (_,_) in
                self?.applyCurrentRules(for: question)
            }
            
            if let answer = SessionService.shared.knownAnswers[question.id], !answer.isEmpty {
                self.setAnswer(answer, for: question, completion: { response, error in
                    printAndLog("setAnswer", "error: \(error?.localizedDescription ?? "no description")", "response: \(String(describing: response))", "answer: \(answer)", isError: error != nil)
                })
            } else {
                self.setAnswer("\"\"", for: question, completion: { response, error in
                    printAndLog("setAnswer", "error: \(error?.localizedDescription ?? "no description")", "response: \(String(describing: response))", "answer: \("\"\"")", isError: error != nil)
                })
            }
        }
    }
    
    public var currentLoadingQuestion: String?
    public var loadSent = false
    public var onfailedSent = false
    
    /*
    public func changeMainVC() -> TestViewController_New  {
        let testVC = TestViewController_New.make()
        let _ = DiagnosticsShared.instance.delegate?.diagnostics_shouldChangeRoot(to: testVC)
        testVC.view.isHidden = true
        
        return testVC
    }
     */
    
    public func apply(test: Test!, onProgress: @escaping (CGFloat)->Void, onFinished: @escaping ()->Void, onFailed: @escaping ()->Void) {
        //check
        //changeMainVC()
       
        loadSent = false
        onfailedSent = false
        
        var questions: [Question] = []
        for item in test.questions {
            if item is QuestionInfo {
                continue
            }
            questions.append(item)
        }
        
        let framesToLoad = questions.sorted(by: {$0.numberInKim < $1.numberInKim}).map({$0.id})
        
        printAndLog("Apply test", "questionIds: \(framesToLoad)", isError: false)
      
        
        if let path = Bundle(for: self.classForCoder).path(forResource: "Main", ofType: "js"),
            let data = FileManager.default.contents(atPath: path),
            let string = String(data: data, encoding: .utf8),
            test.answersJson != nil {
            
            NSLog("%@", Server.url_paths.baseURL.absoluteString)
            
            self.loadHTMLString("<html><head>" +
                "<script>\(string)</script>" +
                "</head><body></body></html>",
            baseURL: Server.url_paths.baseURL) { [weak self] in
                    printAndLog("HTML loaded", isError: false)

                let sortedQuestionIds = framesToLoad

                var loadedIds: [String] = []
                var currentProgress: CGFloat = 0

                var block: ((String)->Void)? = { [weak self] (loadedIframeID) in
                    loadedIds.append(loadedIframeID)
                    if sortedQuestionIds.count > loadedIds.count {
                        self?.currentLoadingQuestion = sortedQuestionIds[loadedIds.count]
                    } else {
                        self?.currentLoadingQuestion = nil
                    }

                    currentProgress = CGFloat(loadedIds.count)/CGFloat(sortedQuestionIds.count)
                    onProgress(currentProgress)
                    if loadedIds.count == sortedQuestionIds.count {
                        onFinished()
                        printAndLog("Test applied", isError: false)
                  
                        NotificationCenter.default.post(Notification(name: kNotificationShowTestViewController))
                    }
                }

                self?.startListeningJSLoad { [weak self] (loadedIframeID) in
                    if block == nil {
                        print("block is nil while loadedIframeID \(loadedIframeID)")
                    }
                    block?(loadedIframeID)
                    if loadedIds.count == sortedQuestionIds.count {
                        block = nil
                        self?.currentLoadingQuestion = nil

                    }
                }
                
                self?.startListeningJSLoadErrors(completion: { [weak self] (errorMessage, id) in
                    if id != nil {
                       self?.currentLoadingQuestion = id
                    }
                    if self?.currentLoadingQuestion != nil {
                        onFailed()
                    }
                    printAndLog("iFrame error sent", "errorMessage = \(String(describing: errorMessage))", "currentLoadingQuestion = \(String(describing: self?.currentLoadingQuestion))")
                })

                //let idsArrayString = "[" + sortedQuestionIds.joined(separator: ",") + "]"

                self?.currentLoadingQuestion = sortedQuestionIds.first
                
                ConnectionService.shared.subscribeForUpdates { [weak self] (_) in
                    guard ConnectionService.shared.hasConnection else {
                        if let `self` = self, self.loadSent && !self.onfailedSent {
                            self.onfailedSent = true
                            printAndLog("ConnectionService.shared.subscribeForUpdates - FAILED")
                            onFailed()
                        }
                        return
                    }
                    guard self?.loadSent == false else { return }
                    self?.loadSent = true
                    print(test.answersJson!)
                    self?.evaluate(localJS: .loadIframesByJS, params: "'\(Server.shared.htmlTaskURL())'",  "'\(test.answersJson!)'") { (response, error) in
                        printAndLog("loadIframesByJS", "error: \(error?.localizedDescription ?? "nil")", "response: \(String(describing: response))", isError: error != nil)
                        if error != nil {
                            onFailed()
                        }
                    }
                 }
                
            }
        } else {
            printAndLog("Can't parse Main.js")
            onProgress(0)
        }
        
    }


    func getAnswer(for question: Question, completion: @escaping (Any?, Error?)->Void) {
        self.evaluateJavaScript(self.iframeWindowPath(for: question) + JSRequests.getAnswer.rawValue, completionHandler: completion)
    }
    
    func getIsAnswerBlank(for question: Question, completion: @escaping (Any?, Error?)->Void) {
        let script = String(format: self.iframeWindowPath(for: question) + JSRequests.mobileHelperRaw + JSRequests.getIsAnswerBlank.rawValue)
        self.evaluateJavaScript(script, completionHandler: completion)
    }
    
    func getHint(for question: Question, completion: @escaping (Any?, Error?)->Void) {
        let script = String(format: self.iframeWindowPath(for: question) + JSRequests.mobileHelperRaw + JSRequests.getHint.rawValue)
        self.evaluateJavaScript(script, completionHandler: completion)
    }
    
    /*
    func instrumentsInit(for question: Question, completion: @escaping (Any?, Error?)->Void) {
        let script = String(format: self.iframeWindowPath(for: question) + JSRequests.initInstruments.rawValue)
        self.evaluateJavaScript(script, completionHandler: completion)
    }
    */
    
    func setAnswerVoiceInput(for question: Question, text: String, completion: @escaping (Any?, Error?)->Void) {
        let script = String(format: self.iframeWindowPath(for: question) + JSRequests.setAnswerVoiceInput.rawValue, text)
        self.evaluateJavaScript(script, completionHandler: completion)
    }
    
    func setAnswerHandwriting(for question: Question, text: String, completion: @escaping (Any?, Error?)->Void) {
        let arr = text.components(separatedBy: "\n")
        var outString = String()
        if arr.count > 0 {
            for item in arr {
                if outString.count > 0 {
                    outString += ","
                }
                outString += "'\(item)'"
            }
        }
        
        outString = "[" + outString + "].join('\\n')"
        
        let script = String(format: self.iframeWindowPath(for: question) + JSRequests.setAnswerHandwriting.rawValue, outString)
        self.evaluateJavaScript(script, completionHandler: completion)
    }
    
    func setAnswer(_ answer: String, for question: Question, completion: @escaping (Any?, Error?)->Void) {
        var script: String!
        if let shuffle = SessionService.shared.answerShuffleParams[question.id], !shuffle.isEmpty {
            script = String(format: self.iframeWindowPath(for: question) + JSRequests.setAnswerWithShuffleParam.rawValue, answer, shuffle)
        } else {
            if !answer.isEmpty {
                script = String(format: self.iframeWindowPath(for: question) + JSRequests.setAnswer.rawValue, answer)
            }
        }
        self.evaluateJavaScript(script, completionHandler: completion)
    }
    
    func emptyCheck(_ answer: String, for question: Question, completion: @escaping (Bool, Error?)->Void) {
        let script = String(format: self.iframeWindowPath(for: question) + JSRequests.mobileHelperRaw + JSRequests.isEmptyAnswer.rawValue, answer)
        evaluateJavaScript(script) { (response, error) in
            printAndLog("emptyCheck", "error: \(error?.localizedDescription ?? "no description")", "response: \(String(describing: response))", isError: error != nil)
            completion(response as? Bool ?? false, error)
        }
    }
    
    public func changeFont(to value: Int, direction: Int, in test: Test) {
        for question in test.questions {
            changeFont(to: value, direction: direction, in: question)
        }
    }
    
    func changeFont(to value: Int, direction: Int, in question: Question) {
        let script = String(format: self.iframeWindowPath(for: question) + JSRequests.mobileHelperRaw + JSRequests.ElementsZoom.rawValue, String(value), String(direction))
        evaluateJavaScript(script)
    }
    
    func reload(question: Question, complete: (()-> Void)? = nil) {
        self.startListening(id: question.id) {
            self.applyCurrentRules(for: question)
       
            self.change(question: question)
        }
        
        self.evaluate(localJS: .setIframeSrc, params: question.id, "\"\(question.link)\"") { response, error in
            printAndLog("setIframeSrc (task reload)", "error: \(error?.localizedDescription ?? "no description")", "response: \(String(describing: response))", isError: error != nil)
        
            
            complete?()
        }
        
    }
    
    public func resizeWiris() {
        asyncAfter { [weak self] in
            let request = String(format: JSRequests.resizeWiris.rawValue, "\(UIScreen.main.bounds.width-152-10)", "\(UIScreen.main.bounds.height-370-28)")
            self?.evaluateJavaScript(request)
        }
    }

}


extension DWKWebView {
    
    fileprivate func evaluate(js: JSRequests, params: Any..., completionHandler: ((Any?, Error?)->Void)?=nil) {
        var jsString = JSRequests.mobileHelper + js.rawValue
        if params.count != 0 {
            jsString = String(format: jsString, arguments: params.compactMap({"\($0)"}) as [CVarArg])
        }
        evaluateJavaScript(jsString, completionHandler: completionHandler)
        
    }
    
    fileprivate func evaluate(localJS js: JSLocalRequests, params: Any?..., completionHandler: ((Any?, Error?)->Void)?=nil) {
        var jsString = js.rawValue
        jsString = String(format: jsString, arguments: params.compactMap({"\($0 ?? "null")"}) as [CVarArg])
        evaluateJavaScript(jsString, completionHandler: completionHandler)
    }
    
    
    fileprivate func applyCurrentRules(for question: Question) {
        
        guard let currentRules = SessionService.shared.currentExamWork?.rulesByHTMLId[question.id] else { return }
        
        asyncAfter { [weak self] in
            self?.evaluate(localJS: .applyCurrentRules, params: question.id, currentRules[.AllowSymbolsCode] ?? "null", currentRules[.AnswerMinLength] ?? "null", currentRules[.AnswerMaxLength] ?? "null")
        }
        
    }
    
    fileprivate func iframePath(for question: Question) -> String {
        return "document.getElementsByName(\(question.id))."
    }
    
    fileprivate func iframeWindowPath(for question: Question) -> String {
        return "document.getElementsByName(\(question.id))[0].contentWindow."
    }
    
    
    
}


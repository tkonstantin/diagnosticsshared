//
//  CodeField.swift
//  Diagnostics
//
//  Created by Константин on 07.05.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

@objc public protocol CodeFieldDelegate: UITextFieldDelegate {
    func deleteBackward(in field: UITextField)
}

public class CodeField: UITextField {
    
    @IBOutlet public weak var codeFieldDelegate: CodeFieldDelegate! {
        didSet {
            self.delegate = codeFieldDelegate
        }
    }
    
    override public func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        switch action {
        case #selector(paste(_:)),
             #selector(copy(_:)),
             #selector(cut(_:)),
             #selector(select(_:)),
             #selector(selectAll(_:)): return false
        default: return super.canPerformAction(action, withSender: sender)
        }
    }

    override public var selectedTextRange: UITextRange? {
        get {
            if (super.selectedTextRange?.isEmpty ?? true) {
                return super.selectedTextRange
            }
            return nil
        } set {
            if (newValue?.isEmpty ?? true) {
                super.selectedTextRange = newValue
            } else {
                super.selectedTextRange = nil
            }
        }
    }
    
    override public func deleteBackward() {
        if self.text == nil || self.text!.isEmpty {
            codeFieldDelegate?.deleteBackward(in: self)
        }
        super.deleteBackward()
    }
}

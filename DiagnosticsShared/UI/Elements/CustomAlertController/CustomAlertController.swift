//
//  CustomAlertController.swift
//  Diagnostics
//
//  Created by Константин on 13.06.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit


class CustomAlertController: UIViewController {
    
    @IBOutlet weak var alertTitleLabel: UILabel!
    @IBOutlet weak var alertTitleBody: UILabel!
    
    @IBOutlet weak var shadowedView: ShadowedView!
    @IBOutlet weak var contentStack: UIStackView!
    @IBOutlet weak var buttonsStack: UIStackView!
    
    class func make(title: String?=nil, body: String?=nil, action: CustomAlertAction?=nil, needsCancel: Bool = true, cancelAction: (()->Void)?=nil) -> CustomAlertController {
        let new = UIStoryboard(name: "CustomAlertController", bundle: DiagnosticsShared.bundle).instantiateViewController(withIdentifier: "CustomAlertController") as! CustomAlertController
        new.modalTransitionStyle = .crossDissolve
        new.modalPresentationStyle = .overCurrentContext
        new.alertTitle = title
        new.alertBody = body
        new.action = action
        new.cancelAction.action = { [weak new] in
            cancelAction?();
            new?.dismiss(animated: true, completion: nil);
        }
        new.needsCancel = needsCancel
        return new
    }
    
    var alertTitle: String?
    var alertBody: String?
    var needsCancel = true
    
    struct CustomAlertAction {
        
        enum types {
            case cancel
            case destructive
            case normal
        }
        var title: String!
        var type: types!
        var action: (()->Void)?
    }
    
    var action: CustomAlertAction?

    
    lazy var cancelAction = {
        return CustomAlertAction(title: "Отменить", type: .cancel, action: { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        })
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        alertTitleLabel.text  = alertTitle
        alertTitleBody.text = alertBody
        
        let customBlueColor = UIColor(hex: "146CA7")
        
        //self.contentStack.backgroundColor = customBlueColor
        //self.contentStack.isHidden = true
        
        if needsCancel {
            let cancel = UIButton(frame: .zero)
            cancel.borderColor = customBlueColor
            cancel.borderWidth = 2
            cancel.setAttributedTitle(NSAttributedString(string: cancelAction.title, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18, weight: .semibold), NSAttributedString.Key.foregroundColor: customBlueColor]), for: .normal)
            cancel.addTarget(self, action: #selector(self.cancelTapped), for: .touchUpInside)
            buttonsStack.addArrangedSubview(cancel)
        }
        
        if action != nil {
            let actionButton = UIButton(frame: .zero)
            actionButton.borderColor = customBlueColor
            actionButton.borderWidth = 2
            actionButton.setAttributedTitle(NSAttributedString(string: action!.title, attributes:  [NSAttributedString.Key.foregroundColor: action!.type == .destructive ? UIColor.red : customBlueColor]), for: .normal)
            actionButton.addTarget(self, action: #selector(self.actionTapped), for: .touchUpInside)
            buttonsStack.addArrangedSubview(actionButton)

        }
    }
    
    @objc func cancelTapped() {
        cancelAction.action?()
    }
    
    @objc func actionTapped() {
        self.dismiss(animated: true, completion: { [weak self] in
            self?.action?.action?()
        });

    }
}

//
//  DUIButton.swift
//  Diagnostics
//
//  Created by Константин on 31.01.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

let blueColor = UIColor(hex: "419BD5")

@IBDesignable public class DUIButton: UIButton {
    
    @IBInspectable public var isOutline: Bool = true {
        didSet {
            update()
        }
    }
    
    @IBInspectable public var outlineTitle: String = "outlineTitle" {
        didSet {
            update()
        }
    }
    
    @IBInspectable public var normalTitle: String = "normalTitle" {
        didSet {
            update()
        }
    }
    
    
    @IBInspectable public var mainColor: UIColor = blueColor {
        didSet {
            update()
        }
    }
    
    @IBInspectable public var fontColor: UIColor = .white {
        didSet {
            update()
        }
    }
    
    
    
    private func update() {
        if isOutline {
            self.backgroundColor = .clear
            self.borderColor = mainColor
            self.borderWidth = 2
            self.setTitleColor(mainColor, for: .normal)
            self.setTitle(outlineTitle, for: .normal)
        } else {
            self.backgroundColor = mainColor
            self.borderColor = .clear
            self.borderWidth = 0
            self.setTitleColor(fontColor, for: .normal)
            self.setTitle(normalTitle, for: .normal)
        }
    }
    
}

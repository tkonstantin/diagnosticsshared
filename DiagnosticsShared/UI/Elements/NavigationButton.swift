//
//  NavigationButton.swift
//  Diagnostics
//
//  Created by Константин on 07.02.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

class NavigationButton: UIButton {

    /*
    var isActive: Bool = false {
        didSet {
            self.tintColor = isActive ? .white : UIColor.white.withAlphaComponent(0.3)
            self.setTitleColor(isActive ? .white : UIColor.white.withAlphaComponent(0.3), for: .normal)
        }
    }
 */
    
    enum states {
        case normal
        case loading
    }
    
    private lazy var activity: UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView()
        loader.color = .white
        loader.hidesWhenStopped = true
        loader.backgroundColor = self.backgroundColor
        loader.isUserInteractionEnabled = false
        loader.isExclusiveTouch = false
        self.addSubview(loader)
        return loader
    }()
    
    
    private var currentState: states = .normal  {
        didSet {
            asyncAfter {
                self.activity.frame = self.bounds
                if self.currentState == .loading {
                    self.isUserInteractionEnabled = false
                    self.activity.startAnimating()
                } else {
                    self.isUserInteractionEnabled = true
                    self.activity.stopAnimating()
                }
            }
        }
    }
    
    func set(_ newState: states) {
        currentState = newState
    }
}

//
//  ComplexTableCell.swift
//  Diagnostics
//
//  Created by Константин on 12/02/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import UIKit

class ComplexTableCell: UIView {
    
    
    enum styles {
        case headline
        case normal
        
        var font: UIFont {
            get {
                switch self {
                case .headline: return UIFont.systemFont(ofSize: 15, weight: .medium)
                case .normal: return UIFont.systemFont(ofSize: 14, weight: .regular)
                }
            }
        }
        
        var textColor: UIColor {
            get {
                switch self {
                case .headline: return UIColor.lightGray
                case .normal: return .black
                }
            }
        }
    }
    
    

    class func make(texts: [String?], style: styles) -> ComplexTableCell {
        let instance = UINib(nibName: "ComplexTableCell", bundle: DiagnosticsShared.bundle).instantiate(withOwner: self, options: nil).first as! ComplexTableCell
        instance.leftLabel.text = texts.first ?? nil
        instance.middleLabel.text = texts.dropFirst().first ?? nil
        instance.rightLabel.text = texts.dropFirst().dropFirst().first ?? nil
        
        for label  in [instance.leftLabel, instance.middleLabel, instance.rightLabel] {
            label?.font = style.font
            label?.textColor = style.textColor
        }
        return instance
    }

    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var middleLabel: UILabel!
    @IBOutlet weak var rightLabel: UILabel!
    
    
}

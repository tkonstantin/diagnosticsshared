//
//  ComplexTable.swift
//  Diagnostics
//
//  Created by Константин on 12/02/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import UIKit

@objc protocol ComplexTableDataSource: class {
    func numberOfRows(_ table: ComplexTable) -> Int
    func stringForCell(at row: Int, in column: Int, _ table: ComplexTable) -> String?
    func headline(for column: Int, _ table: ComplexTable) -> String?
}

class ComplexTable: UIScrollView {

    @IBOutlet weak var dataSource: ComplexTableDataSource!
    
    @IBOutlet weak var stack: UIStackView!
    
    private var currentCells: [ComplexTableCell] = []
    

    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        asyncAfter {
            self.reload()
        }
    }
    
    func reload() {
        guard numberOfRows != 0 else {
            clear();
            return
        }
        
        
        let headlineCell =
            ComplexTableCell.make(texts: [
            dataSource?.headline(for: 0, self), dataSource?.headline(for: 1, self), dataSource?.headline(for: 2, self)
            ], style: .headline)
        
        currentCells.append(headlineCell)
        stack?.addArrangedSubview(headlineCell)
        
        
        for index in 0..<numberOfRows {
            let texts: [String?] = (0..<3).reduce([], { res, column in return res + [text(for: IndexPath(row: index, column: column))] })
            let cell = ComplexTableCell.make(texts: texts, style: .normal)
            
            currentCells.append(cell)
            stack?.addArrangedSubview(cell)
        }
    }
    
    private func clear() {
        currentCells.forEach({ stack.removeArrangedSubview($0) })
        currentCells = []
    }
    
    
    
    private var numberOfRows: Int {
        get {
            return dataSource?.numberOfRows(self) ?? 0
        }
    }
    
    private func text(for indexPath: IndexPath) -> String? {
        return dataSource?.stringForCell(at: indexPath.row, in: indexPath.column, self)
    }

}


extension IndexPath {
    fileprivate init(row: Int, column: Int) {
        self.init(row: row, section: column)
    }
    
    fileprivate var column: Int {
        get {
            return self.section
        }
    }
    
}

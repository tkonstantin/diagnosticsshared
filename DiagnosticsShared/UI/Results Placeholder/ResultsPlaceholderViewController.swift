//
//  ResultsPlaceholderViewController.swift
//  Diagnostics
//
//  Created by Константин on 12/02/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import UIKit

class ResultsPlaceholderViewController: UIViewController {

    class func makeOne() -> ResultsPlaceholderViewController {
        let new = UIStoryboard(name: "ResultsPlaceholderView", bundle: DiagnosticsShared.bundle).instantiateViewController(withIdentifier: "ResultsPlaceholderViewController") as! ResultsPlaceholderViewController
        new.modalTransitionStyle = .crossDissolve
        new.modalPresentationStyle = .overCurrentContext
        return new
    }
    
    @IBOutlet weak var complexTable: ComplexTable!
    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            dateLabel.text = "Пройдено: " + Date().parseDateShort(dots: true)
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.text = results?.subtitle
        }
    }
    
    lazy var results: TestResult! = {
        return SessionService.shared.currentTest.results
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard let workId = SessionService.shared.currentExamWork?.id, SessionService.shared.currentExamWork?.isFinished == false else { return }
        let status = SessionService.shared.currentExamWork!.getStatus()
        
        Server.shared.finishExamWork(with: workId, status: status, completion: {
            asyncAfter {
                ResultsHTMLViewController.makeOne { resultsController in
                    guard let resultsController = resultsController else { return }
                     DiagnosticsShared.instance.delegate?.diagnostics_shouldChangeRoot(to: resultsController)
                }
            }
            return
        })
        
    }
    
    
}


extension ResultsPlaceholderViewController: ComplexTableDataSource {
    
    func headline(for column: Int, _ table: ComplexTable) -> String? {
        switch column {
        case 0: return "№ задания"
        case 1: return "Ваш ответ"
        case 2: return "Максимальный балл"
        default: return nil 
        }
    }
    
    
    func numberOfRows(_ table: ComplexTable) -> Int {
        return results?.tasks.count ?? 0
    }
    
    func stringForCell(at row: Int, in column: Int, _ table: ComplexTable) -> String? {
        guard results != nil, results.tasks.count > row else { return nil }
        let currentTask = results.tasks[row]
        switch column {
        case 0:
            return String(currentTask.number)
        case 1:
            return currentTask.answerResult
        case 2:
            return String(currentTask.maxScore)
        default:
            return nil
        }
    }
    
    
}

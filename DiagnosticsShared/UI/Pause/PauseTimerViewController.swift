//
//  PauseTimerViewController.swift
//  Diagnostics
//
//  Created by Константин on 12.02.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

class PauseTimerViewController: UIViewController {

        
    @IBOutlet weak var circle: ShadowedView!
    @IBOutlet weak var timeLabel: UILabel!
    
    private var pauseTimer: Timer!
    private var endDate: Date!
    var secondsLeft: Int {
        get {
            guard endDate != nil else { return 0 }
            return max(Int(endDate.timeIntervalSince1970 - Date().timeIntervalSince1970), 0)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        endDate = Date().addingTimeInterval(PauseManager.shared.duration + 1)
        timeLabel.text = String(seconds: Int(PauseManager.shared.duration))
        pauseTimer = Timer.init(timeInterval: 1, target: self, selector: #selector(self.tick), userInfo: nil, repeats: true)
        RunLoop.current.add(pauseTimer, forMode: .common)
        updateCirleProgress()
    }
    
    
    @objc private func tick() {
        updateCirleProgress()
        timeLabel.text = String(seconds: secondsLeft)
        if secondsLeft <= 0 {
            pauseTimer?.invalidate()
            pauseTimer = nil
            asyncAfter(1000, {
                let presenting = self.presentingViewController
                self.dismiss(animated: true, completion: {
                    presenting?.dismiss(animated: true, completion: nil)
                })
            })
        }
    }
    
    
    private var circleShape: CAShapeLayer!
    private var circleBgShape: CAShapeLayer!
    
    private var progress: CGFloat {
        get {
            return 1 - CGFloat(secondsLeft)/CGFloat(PauseManager.shared.duration)
        }
    }
    
    private func updateCirleProgress() {
        DispatchQueue.main.async {
            
            if self.circleShape == nil {
                self.circleShape = CAShapeLayer()
            }
            if self.circleBgShape == nil {
                self.circleBgShape = CAShapeLayer()
            }
            
            let center = CGPoint(x: self.circle.bounds.width/2, y: self.circle.bounds.height/2)
            let raduis: CGFloat = self.circle.bounds.width/2
            let bigP = UIBezierPath(arcCenter: center, radius: raduis, startAngle: -CGFloat.pi/2, endAngle: CGFloat.pi*3/2, clockwise: true)
            
            self.circleShape.path = bigP.cgPath
            self.circleShape.fillColor = UIColor.clear.cgColor
            self.circleShape.strokeColor = blueColor.cgColor
            self.circleShape.lineWidth = 2
            self.circleShape.lineCap = CAShapeLayerLineCap.round
            self.circleShape.strokeStart = 0
            
            
            self.circleBgShape.path = bigP.cgPath
            self.circleBgShape.fillColor = UIColor.clear.cgColor
            self.circleBgShape.strokeColor = UIColor(hex: "EDECEF").cgColor
            self.circleBgShape.lineWidth = 2
            self.circleBgShape.lineCap = CAShapeLayerLineCap.round
            self.circleBgShape.strokeStart = 0
            self.circleBgShape.strokeEnd = 1
            
            CATransaction.begin()
            
            CATransaction.setAnimationDuration(self.progress >= 1 ? 0.75 : 2)
            self.circleShape.strokeEnd = CGFloat(self.progress)
            CATransaction.commit()
      
            if !(self.circle.layer.sublayers ?? []).contains(self.circleBgShape) {
                self.circle.layer.addSublayer(self.circleBgShape)
            }
            
            if !(self.circle.layer.sublayers ?? []).contains(self.circleShape) {
                self.circle.layer.addSublayer(self.circleShape)
            }
        }
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
}

//
//  PauseViewController.swift
//  Diagnostics
//
//  Created by Константин on 12.02.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

class PauseViewController: UIViewController {

    class func makeOne() -> PauseViewController {
        let new = UIStoryboard(name: "Pause", bundle: DiagnosticsShared.bundle).instantiateViewController(withIdentifier: "PauseViewController") as! PauseViewController
        new.modalPresentationStyle = .overCurrentContext
        new.modalTransitionStyle = .crossDissolve
        return new
    }
    
    static var isPresented = false
    
    @IBOutlet weak var pauseButton: DUIButton!
    @IBOutlet weak var cancelButton: DUIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        PauseViewController.isPresented = true
        super.viewWillAppear(animated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        pauseButton.isHidden = true
        cancelButton.isHidden = true
        super.prepare(for: segue, sender: sender)
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        PauseViewController.isPresented = false
        super.dismiss(animated: flag, completion: completion)
    }
    
    @IBAction func confirmTapped(_ sender: DUIButton) {
        PauseManager.shared.pauseStarted()
    }
    
    @IBAction func cancelTapped(_ sender: DUIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
}

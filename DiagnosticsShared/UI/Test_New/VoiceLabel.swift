//
//  VoiceLabel.swift
//  DiagnosticsShared
//
//  Created by Pavel Kurta on 21/09/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import UIKit

class VoiceLabel: UILabel {

   override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        super.drawText(in: rect.inset(by: insets))
    }

}

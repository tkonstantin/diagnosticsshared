//
//  TestViewController_New.swift
//  Diagnostics
//
//  Created by Константин on 13.09.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import Reachability
import SCSiriWaveformView
import AVFoundation
import Speech
import UIViewController_KeyboardAnimation
import WebKit

let Notfi = "resetOnLogout"

let kNotificationResetOnLogout: NSNotification.Name = NSNotification.Name(rawValue: "notificationResetOnLogout")
let kNotificationStartCurrentTest: NSNotification.Name = NSNotification.Name(rawValue: "notificationStartCurrentTest")
let kNotificationShowTestViewController: NSNotification.Name = NSNotification.Name(rawValue: "notificationShowTestViewController")


class TestViewController_New: UIViewController {
    
    class func make() -> TestViewController_New {
        return UIStoryboard(name: "Tests_New", bundle: DiagnosticsShared.bundle).instantiateViewController(withIdentifier: "TestViewController") as! TestViewController_New
    }
    
    var recorder: AVAudioRecorder?
    var displaylink: CADisplayLink?
    
    @IBOutlet weak var pageNumbers: PageNumbersShell!
    @IBOutlet weak var backBottomButton: NavigationButton!
    @IBOutlet weak var forwardBottomButton: NavigationButton!
    @IBOutlet weak var backTopButton: NavigationButton!
    @IBOutlet weak var forwardTopButton: NavigationButton!
    @IBOutlet weak var topButtonsView: UIView!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var answersCountLabel: UILabel!
    
    @IBOutlet weak var webSuperview: WebSuperview!
    var underWebView: WKWebView?
    
    @IBOutlet weak var voiceBGView: UIView!
    @IBOutlet weak var waveformView: SCSiriWaveformView!
    @IBOutlet weak var voiceTextLabel: UILabel!
    
    @IBOutlet weak var myscriptBGView: UIView!
    
    @IBOutlet weak var buttonVoice: UIButton!
    @IBOutlet weak var buttonHandwriter: UIButton!
    
    @IBOutlet weak var inputTypeSegmentedControl: UISegmentedControl!
    
    @IBOutlet weak var widthPDFMaterials: NSLayoutConstraint!
    
    @IBOutlet weak var topWebview: NSLayoutConstraint!
    @IBOutlet weak var topBarView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var separatorLineView: UIView!
    @IBOutlet weak var bottomScrollView: NSLayoutConstraint!
    
    @IBOutlet weak var hintButton: UIButton!
    
    var hintViewController: HintViewController?
    var isHintInCompactMode: Bool = true
          
    let audioEngine = AVAudioEngine()
    let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "ru_RU"))
    let request = SFSpeechAudioBufferRecognitionRequest()
    var recognitionTask: SFSpeechRecognitionTask?
    
    var currentTest: Test! {
        get {
            return SessionService.shared.currentTest
        }
    }
    
    private var _selectedQuestionIndex: Int = 1
    fileprivate var notifiedAboutCompletion = false
    fileprivate var forwardTapped = false
    fileprivate var indexChangedFromNumbers = false
    
    var isIgnoreProcessPrevQuestion: Bool = false
    
    private var _actionHandwritingAccept: Bool?
    var actionHandwritingAccept: Bool? {
        get {
            if let text = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String{
                if let oldVersion = UserDefaults.standard.object(forKey: "actionHandwritingAccept") as? String {
                    return oldVersion == text
                }
            }
            return _actionHandwritingAccept
        }
        set {
            if newValue != nil && newValue! == true {
                if let text = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String{
                    UserDefaults.standard.set(text, forKey: "actionHandwritingAccept")
                }
            }
            _actionHandwritingAccept = newValue
        }
    }
    
    var selectedQuestionIndex: Int {
        get {
            return _selectedQuestionIndex
        } set {
            
            //PK
            let question = self.currentTest.questions[newValue - 1]
            
            if (question is QuestionInfo) == false &&
                isValidForUser2(newValue - 1) == false {
                
                self.commonProcessInvalidateQuestion("Выполнение заданий, проверяемых экспертами, для вас недоступно. Чтобы получить доступ ко всем возможностям сервиса, подтвердите статус московского школьника.")
                
                return
            }
            
            let prevQuestion = self.currentTest.questions[_selectedQuestionIndex - 1]
            if self.forwardTapped && prevQuestion.isAnswered == false && prevQuestion.taskGroupId != nil {
                //Проверить что нет ответа на другие вопросы в группе
                if let checkTaskGroup = self.currentTest.taskGroups.first(where: { $0.id == prevQuestion.taskGroupId! }) {
                    let checkCount = checkTaskGroup.numberMandatoryTask
                    var count = 0
                    for q in self.currentTest.questions {
                        if q.taskGroupId != nil && q.taskGroupId! == prevQuestion.taskGroupId! && q.isAnswered {
                            count += 1
                            
                            if count >= checkCount {
                                self.commonProcessInvalidateQuestion("Максимальное количество заданий в группе уже выполнено. Чтобы выполнить это задание, необходимо удалить ответ одного из выполненных заданий группы.")
                                  isIgnoreProcessPrevQuestion = true
                                
                                 self.forwardTapped = false
                                return
                            }
                        }
                    }
                }
            }
            
            indexChangedFromNumbers = false
            self.view.endEditing(true)
            
            self.hintButton.isHidden = true
            
            if (prevQuestion is QuestionInfo) ||
                isIgnoreProcessPrevQuestion {
                isIgnoreProcessPrevQuestion = false
                
                if self._selectedQuestionIndex == newValue && newValue == self.currentTest.questionsCount && self.forwardTapped {
                    self._selectedQuestionIndex = 1
                } else {
                    self._selectedQuestionIndex = newValue
                }
                                   
                self.setNormalUI()
                self.updateProcess()
                return
            }
            
            processCurrentAnswer { parsed_successfully in
                self.setNormalUI()
                
                if parsed_successfully {

                    if self._selectedQuestionIndex == newValue && newValue == self.currentTest.questionsCount && self.forwardTapped {
                        self._selectedQuestionIndex = 1
                    } else {
                        self._selectedQuestionIndex = newValue
                    }
                    
                    self.updateProcess()
                }
                
                self.forwardTapped = false
            }
            

        }
    }
    
    private func commonProcessInvalidateQuestion(_ str: String) {
        let alert =
            UIAlertController(title: "Внимание",
                              message: str,
                              preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
        self.setNormalUI()
    }
    
    private func setNormalUI() {
        self.view.isUserInteractionEnabled = true
        DiagnosticsShared.instance.delegate?.diagnostics_shouldHideHUD()

        asyncAfter {
            self.forwardTopButton?.set(.normal)
            self.forwardBottomButton?.set(.normal)
            self.backTopButton?.set(.normal)
            self.backBottomButton?.set(.normal)
        }
    }
    
    private func updateProcess() {
        self.checkForCompletion()
        self.updateInfoLabels()
        //self.instrumentsInit()

        [self.backBottomButton, self.backTopButton].forEach({
            let shouldBeEnabled = self.selectedQuestionIndex > 1
            $0?.isEnabled = shouldBeEnabled
            $0?.tintColor = .white
            //$0?.alpha = shouldBeEnabled ? 1 : 0.7
        })
        
        self.pageNumbers?.reloadData()
        
        let question = self.currentTest.questions[self.selectedQuestionIndex - 1]
        self.hintButton.isHidden = SessionService.shared.currentExamWork!.taskTextHintHtmlIds[question.id] == nil
        
        if let questionInfo = question as? QuestionInfo {
            if self.underWebView == nil {
                self.underWebView = WKWebView()
                self.underWebView!.customUserAgent = self.webSuperview.webView.customUserAgent
                self.underWebView!.frame = self.webSuperview.frame
                self.webSuperview.superview?.addSubview(self.underWebView!)
            }
            if let url = URL(string: Server.shared.htmlTaskURL(with: String(questionInfo.taskGroup?.informationTextId ?? 0))) {
                self.underWebView!.load(URLRequest(url: url))
            }
            
        } else {
            self.underWebView?.removeFromSuperview()
            self.underWebView = nil
            
            self.webSuperview.webView?.change(question: question)
        }
    }
    
    func isValidForUser(_ index: Int) -> Bool {
        if let work = SessionService.shared.currentExamWork,
            let user = Server.shared.currentUser {
            
            let test = SessionService.shared.currentTest
            let question = test.questions[index]
                          
            let htmlId = question.id
            let isPublic = work.isPublicValues[htmlId]
            if isPublic == 0 && user.IsAnonymous {
                return false
            }
        }
        
        return (currentTest.questions[index].isAnswered == false)
    }
    
    func isValidForUser2(_ index: Int) -> Bool {
        if let work = SessionService.shared.currentExamWork,
            let user = Server.shared.currentUser {
            
            let test = SessionService.shared.currentTest
            let question = test.questions[index]
                      
            let htmlId = question.id
            let isPublic = work.isPublicValues[htmlId]
            if isPublic == 0 && user.IsAnonymous {
                return false
            }
        }
        return true
    }
    
    func getExpertQustionForUser() -> Int {
        if let work = SessionService.shared.currentExamWork,
            let user = Server.shared.currentUser {
            if user.IsAnonymous {
                var count = 0
                for item in work.htmlIds {
                    let isPublic = work.isPublicValues[item]
                    if isPublic == 0 {
                        count = count + 1
                    }
                }
                return count
            }
        }
        return 0
    }
    
    func getValidNextIndex(_ index: Int) -> Int {
        if currentTest.questions.count <= index {
            //это последнее задание
            
            //проверить, что данны ответы на все задания
            if self.checkIsCompleteAllQuestion() {
                if isValidForUser(0) == false {
                    return getValidNextIndex(1)
                }
            } else {
                //даны ответы не на все задания - ищем первое неотвеченное
                for i in 0 ..< currentTest.questionsCount {
                    let q = currentTest.questions[i]
                    if q.isAnswered == false {
                        if isValidForUser(i)  {
                            return i
                        }
                    }
                }
                    
            }
            return 0
        } else {
            //проверить что все задания выполнены
            var isFinished = true
            for item in currentTest.questions {
                if item.isAnswered == false {
                    isFinished = false
                    break
                }
            }
            
            if isFinished == false {
                //проверить что оно не экспертное
                if isValidForUser(index) == false {
                    //задание экспертное
                    return getValidNextIndex(index + 1)
                }
            }
        }
        return index
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //subscribeToKeyboard()
        subscibeForTimer()

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(notificationResetOnLogout),
                                               name: kNotificationResetOnLogout,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(notificationStartCurrentTest),
                                               name: kNotificationStartCurrentTest,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(notificationShowTestViewController),
                                               name: kNotificationShowTestViewController,
                                               object: nil)
        
        //PK
        let audioSession = AVAudioSession.sharedInstance()
        try? audioSession.setCategory(AVAudioSession.Category.playAndRecord)
        //try? audioSession.setMode(AVAudioSession.Mode.measurement)
        try? audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        
        let inputNode = self.audioEngine.inputNode
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            self.request.append(buffer)
        }
        /*
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(.playback, mode: .default, options: [.mixWithOthers, .allowAirPlay])
            try audioSession.setActive(true)
            
            let inputNode = self.audioEngine.inputNode
            let recordingFormat = inputNode.outputFormat(forBus: 0)
            inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
                self.request.append(buffer)
            }
            
        } catch {
            print(error)
        }
        */
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "myScriptVC" {
            /* //disable Myscript
            self.editorViewController = segue.destination as! EditorViewController
            self.editorViewController.engine = self.engine
            self.editorViewController.inputMode = .forcePen
             */
        }
    }
    
    @objc func notificationResetOnLogout() {
        self.webSuperview.resetOnLogout()
    }
    
    @objc func notificationStartCurrentTest() {
        self.webSuperview.startLoadingWebView(test: SessionService.shared.currentTest) {
           //ignore
            self.pageNumbers?.reloadData()
        }
    }
    
    @objc func notificationShowTestViewController() {
        self.view.isHidden = false
        self.reload()
     
        self.widthPDFMaterials.constant = (SessionService.shared.currentExamWork!.materials.count > 0) ? 50.0 : 0.0
    }
    
    func reload() {
        if selectedQuestionIndex == 1, let first = self.currentTest.questions.first {
            self.hintButton.isHidden = SessionService.shared.currentExamWork!.taskTextHintHtmlIds[first.id] == nil
            
            if let questionInfo = first as? QuestionInfo {
                if self.underWebView == nil {
                    self.underWebView = WKWebView()
                    self.underWebView!.frame = self.webSuperview.frame
                    self.webSuperview.superview?.addSubview(self.underWebView!)
                }
                if let url = URL(string: Server.shared.getTaskURL(with: String(questionInfo.taskGroup?.informationTextId ?? 0))) {
                    self.underWebView!.load(URLRequest(url: url))
                }

            } else {
                self.underWebView?.removeFromSuperview()
                self.underWebView = nil
                
                self.webSuperview.webView?.change(question: first)
            }

        }
        self.updateInfoLabels()
        //self.instrumentsInit()
        self.pageNumbers?.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        printAndLog("!!! viewDidAppear TestViewController")
        
        reload()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.an_subscribeKeyboard(animations: { (keyboardRect, duration, isShowing) in
            self.bottomScrollView.constant = isShowing ? keyboardRect.height - 100.0 : 8.0
            
            //ignore
            //self.topButtonsView.isHidden = !isShowing
            self.topBarView.isHidden = isShowing
            self.headerView.isHidden = isShowing
            self.infoView.isHidden = isShowing
            self.separatorLineView.isHidden = isShowing
            self.topWebview.constant = isShowing ? -20.0 : 203.0
            
            let test = self.currentTest
            let question = test!.questions[self.selectedQuestionIndex - 1]
            let type = test!.initializeVoiceAndHandwritingInfo[question.id] ?? .none
            
            if type == .voice || type == .all {
                self.buttonVoice.isHidden = !isShowing
            } else {
                self.buttonVoice.isHidden = true
            }
            
            if type == .handwriting || type == .all {
                
                self.buttonHandwriter.isHidden = true
                
                /*
                if let user = Server.shared.currentUser {
                    self.buttonHandwriter.isHidden = !(user.IsAnonymous == false && isShowing)
                } else {
                    self.buttonHandwriter.isHidden = !isShowing
                }
                 */
            } else {
                self.buttonHandwriter.isHidden = true
            }
        }, completion: { (res) in
            if res {
                self.webSuperview?.webView?.scrollView.contentOffset.y = 0.0
            }
        })
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.an_unsubscribeKeyboard()
    }
    
    func alertNoInternetConnection() {
        let alert = UIAlertController(title: nil, message: "Прохождение работы невозможно. Проверьте соединение с интернетом.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.closeHintButton()
        
        /*
        if Reachability.forInternetConnection()?.isReachable() == false {
            alertNoInternetConnection()
            return
        }
        */

        view.isUserInteractionEnabled = false
        (sender as?  NavigationButton)?.set(.loading)
        selectedQuestionIndex = max(1, selectedQuestionIndex - 1)
    }
    
    @IBAction func forwardTapped(_ sender: UIButton) {
        self.closeHintButton()
        
        if Reachability.forInternetConnection()?.isReachable() == false {
            alertNoInternetConnection()
            return
        }
        
        view.isUserInteractionEnabled = false
        (sender as?  NavigationButton)?.set(.loading)
        forwardTapped = true
        selectedQuestionIndex = getValidNextIndex(selectedQuestionIndex) + 1 //min(currentTest.questionsCount, selectedQuestionIndex + 1)
    }
    
    @IBAction func hintButtonTapped(_ sender: UIButton) {
        if self.hintViewController == nil {
            self.isHintInCompactMode = true
            
            let hintStoryboard = UIStoryboard(name: "Hint", bundle: DiagnosticsShared.bundle)
            if let vc = hintStoryboard.instantiateViewController(withIdentifier: "hintViewController") as? HintViewController {
                self.addChild(vc)
                vc.view.frame = CGRect(origin: CGPoint.zero,
                                       size: CGSize(width: self.view.frame.width * 0.6 , height: self.view.frame.height * 0.6))
                vc.view.center = CGPoint(x: self.view.frame.width / 2.0, y: self.view.frame.height / 2.0)
                self.view.addSubview(vc.view)
                vc.didMove(toParent: self)
                
                vc.closeButton.addTarget(self, action: #selector(closeHintButton), for: UIControl.Event.touchUpInside)
                vc.inOutButton.addTarget(self, action: #selector(inOutHintButton), for: UIControl.Event.touchUpInside)
                
                vc.webView.customUserAgent = self.webSuperview.webView.customUserAgent
                
                let question = self.currentTest.questions[self.selectedQuestionIndex - 1]
                if let id = SessionService.shared.currentExamWork!.taskTextHintHtmlIds[question.id],
                    let url = URL(string: Server.shared.getTaskURL(with: String(id))) {
                    vc.webView.load(URLRequest(url: url))
                }
                self.hintViewController = vc
            }
        }
    }
    
    @objc func closeHintButton() {
        self.hintViewController?.willMove(toParent: nil)
        self.hintViewController?.view.removeFromSuperview()
        self.hintViewController?.removeFromParent()
        self.hintViewController = nil
    }
    
    @objc func inOutHintButton() {
        if self.isHintInCompactMode {
            self.isHintInCompactMode = false
            let image = UIImage(named: "hint_in", in: DiagnosticsShared.bundle, compatibleWith: nil)
            self.hintViewController?.inOutButton.setImage(image, for: UIControl.State.normal)
            
            self.hintViewController?.view.frame = CGRect(origin: CGPoint(x: 44.0, y: 90.0),
                                                         size: CGSize(width: self.view.frame.width - 44.0 * 2.0,
                                                                      height: self.view.frame.height - 90.0 - 85.0))
                           
        } else {
            self.isHintInCompactMode = true
            let image = UIImage(named: "hint_out", in: DiagnosticsShared.bundle, compatibleWith: nil)
            self.hintViewController?.inOutButton.setImage(image, for: UIControl.State.normal)
                      
            self.hintViewController?.view.frame = CGRect(origin: CGPoint.zero,
                                                            size: CGSize(width: self.view.frame.width * 0.6 , height: self.view.frame.height * 0.6))
            self.hintViewController?.view.center = CGPoint(x: self.view.frame.width / 2.0, y: self.view.frame.height / 2.0)
        }
    }
    
    
    private var currentFontDelta = 0
    
    @IBAction func increaseFontTapped(_ sender: UIButton) {
        guard currentFontDelta < 2 else { return }
        currentFontDelta += 1
        webSuperview.webView?.changeFont(to: currentFontDelta, direction: 1, in: currentTest)
    }
    
    @IBAction func decreaseFontTapped(_ sender: UIButton) {
        guard currentFontDelta > -2 else { return }
        currentFontDelta -= 1
        webSuperview.webView?.changeFont(to: currentFontDelta, direction: -1, in: currentTest)
    }
    
    @IBAction func reloadTaskTapped(_ sender: UIButton) {
        let question = self.currentTest.questions[self.selectedQuestionIndex - 1]
        self.webSuperview.webView?.reload(question: question) {
             //self.instrumentsInit()
        }
        
        guard self.currentFontDelta != 0 else { return }
        asyncAfter(300) {
            
            let isPositive = self.currentFontDelta > 0
            for value in 0..<abs(self.currentFontDelta) {
                self.webSuperview.webView?.changeFont(to: isPositive ? value : (-value), direction: isPositive ? 1 : -1, in: self.currentTest.questions[self.selectedQuestionIndex - 1])
            }
            
                      
        }
       
    }
    
    @IBAction func finishTapped(_ sender: UIButton) {
        if Reachability.forInternetConnection()?.isReachable() == false {
            let alert = UIAlertController(title: nil, message: "Не удалось завершить работу. Проверьте интернет соединение и попробуйте ещё раз", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.destructive, handler: nil))
            self.present(alert, animated: true, completion: nil)

            return
        }
        
        DiagnosticsShared.instance.delegate?.diagnostics_shouldShowHUD(with: nil, progress: nil)

        processCurrentAnswer(forced: true)  {_ in
            asyncAfter {
                DiagnosticsShared.instance.delegate?.diagnostics_shouldHideHUD()
                /*
                let alert = CustomAlertController.make(title: "Внимание!", body: self.textForFinishingAlert(), action: CustomAlertController.CustomAlertAction(title: "Завершить", type: .destructive, action: { [weak self] in
                    self?.view.isUserInteractionEnabled = false
                    UIApplication.shared.goToResults()
                    return;
                }), needsCancel: true)
                */
                let alert = UIAlertController(title: "Внимание!", message: self.textForFinishingAlert(), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Отменить", style: UIAlertAction.Style.destructive, handler: nil))
                alert.addAction(UIAlertAction(title: "Завершить", style: UIAlertAction.Style.default, handler: { (action) in
                    UIApplication.shared.goToResults()
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    /**
     * IINK Engine, lazy loaded.
     *
     * @return the iink engine.
     */
    weak var editorViewController: EditorViewController!
    
    var engineErrorMessage: String?

    /* //disable Myscript
    lazy var engine: IINKEngine? = {
        // Check that the MyScript certificate is present
        if myCertificate.length == 0
        {
            self.engineErrorMessage = "Please replace the content of MyCertificate.c with the certificate you received from the developer portal"
            return nil
        }
        
        // Create the iink runtime environment
        NSLog("Bundle: %@", Bundle.main.bundleIdentifier!)
        let data = Data(bytes: myCertificate.bytes, count: myCertificate.length)
        guard let engine = IINKEngine(certificate: data) else
        {
            self.engineErrorMessage = "Invalid certificate"
            return nil
        }
        
        // Configure the iink runtime environment
        let configurationPath = Bundle(for: self.classForCoder).bundlePath.appending("/recognition-assets/conf")
        do {
            try engine.configuration.setStringArray([configurationPath], forKey:"configuration-manager.search-path") // Tells the engine where to load the recognition assets from.
            try engine.configuration.setString("ru_RU", forKey: "lang");
            try engine.configuration.setBoolean(false, forKey: "text.guides.enable");
        } catch {
            print("Should not happen, please check your resources assets : " + error.localizedDescription)
            return nil
        }
        
        // Set the temporary directory
        do {
            try engine.configuration.setString(NSTemporaryDirectory(), forKey: "content-package.temp-folder")
        } catch {
            print("Failed to set temporary folder: " + error.localizedDescription)
            return nil
        }
        
        return engine
    }()
    */

}


extension TestViewController_New: PageNumbersDataSource, PageNumbersDelegate {
    
    func numberOfPages(_ pageNumbersView: PageNumbersView) -> Int {
        return currentTest.questions.count
    }
    
    func selectedPageNumber(_ pageNumbersView: PageNumbersView) -> Int {
        return selectedQuestionIndex
    }
    
    func pageNumbersView(_ pageNumbersView: PageNumbersView, isAnsweredAt index: Int) -> Bool {
        return currentTest.questions[index].isAnswered
    }
    
    func pageNumbersView(_ pageNumbersView: PageNumbersView, didSelectItemAt index: Int) {
        indexChangedFromNumbers = true
        selectedQuestionIndex = index
    }
    
    
}


extension TestViewController_New { //keyboard
    
    /*
    func subscribeToKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onKeyboardUP), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onKeyboardDown), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func onKeyboardUP() {
        topButtonsView.isHidden = false
    }
    
    @objc private func onKeyboardDown() {
        topButtonsView.isHidden = true
    }
    */
}


extension TestViewController_New {
    
    func processCurrentAnswer(forced: Bool=false, completion: @escaping (Bool)->Void) {
        webSuperview.webView?.isEscaping = forced
        
        if self.currentTest.questionsCount == 0 {
            return
        }
        
        let question = self.currentTest.questions[self.selectedQuestionIndex - 1]
        let questionId = question.id
        
        if forwardTapped {
            webSuperview.webView?.getIsAnswerBlank(for: question) { [weak self] (response, error) in
                printAndLog("getIsAnswerBlank", "error: \(error?.localizedDescription ?? "no description")", "response: \(response)", isError: error != nil)
                if let value = response as? Int {
                    if SessionService.shared.currentExamWork!.isBlankValues[questionId] == 1 {
                        if value == 1 {
                            SessionService.shared.currentExamWork!.isHandMadeWorks.insert(questionId)
                        } else {
                            SessionService.shared.currentExamWork!.isHandMadeWorks.remove(questionId)
                        }
                    }
                }
            }
            
            webSuperview.webView?.getAnswer(for: question) { [weak self] (response, error) in
                printAndLog("getAnswer", "error: \(error?.localizedDescription ?? "no description")", "response: \(response)", isError: error != nil)
                
                self?.parseGetAnswer(result: response, error: error, number: self!.selectedQuestionIndex, forced: forced, completion: completion)
            }
        } else {
            completion(true)
        }
    }
    
    
    private func parseGetAnswer(result: Any?, error: Error?, number: Int, forced: Bool=false, completion: @escaping ((Bool)->Void)) {
        var string: String?
        
        if let res = result as? [String: Any], let jsonData = try? JSONSerialization.data(withJSONObject: res, options: []) {
            string = String(data: jsonData, encoding: .utf8)
        }
        
        let currentQuestion = currentTest.questions[number - 1]
        SessionService.shared.knownAnswers[currentQuestion.id] = string
        
        let web = webSuperview.webView
        web?.emptyCheck(string ?? "", for: currentQuestion, completion: { [weak self] (isEmpty, _) in

            if error != nil || result == nil {
                if error != nil && !forced {
                    _ = showAlert(title: "Ошибка", body: error!.localizedDescription, from: self)
                }
                completion(false)
                self?.pageNumbers.reloadData()
                return
            }
            
            if let dict = result as? NSDictionary, self != nil {
                SessionService.shared.handleResultDict(for: self!.currentTest.questions[number - 1], isEmpty: isEmpty, dict: dict, number: number) {
                    completion(true)
                    asyncAfter {
                        self?.pageNumbers.reloadData()
                    }
                }
                return
            } else {
                completion(true)
                self?.pageNumbers.reloadData()
                return
            }
        })
    }
    
    /*
    fileprivate func instrumentsInit() {
        guard (currentTest?.questionsCount ?? 0) > selectedQuestionIndex - 1 else { return }
        let question = currentTest.questions[selectedQuestionIndex - 1]
        
        webSuperview.webView?.instrumentsInit(for: question, completion: { (response, error) in
            printAndLog("instrumentsInit", "error: \(error?.localizedDescription ?? "no description")", "response: \(String(describing: response))", isError: error != nil)
            
            DispatchQueue.main.async {
                if error != nil {
                    let alert = UIAlertController(title: "Ошибка", message: error!.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.destructive, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        })
    }
    */
    
    fileprivate func updateInfoLabels() {
        answersCountLabel?.text = "\(currentTest?.answeredQuestionsCount ?? 0)/\(currentTest?.questionsCount ?? 0)"

        guard (currentTest?.questions.count ?? 0) > selectedQuestionIndex - 1 else { return }
        let question = currentTest.questions[selectedQuestionIndex - 1]
        //PK - OLD
        //commentLabel?.text = question.hint
        
        //PK - New
        if question is QuestionInfo {
            self.commentLabel?.text = nil
        } else {
            webSuperview.webView?.getHint(for: question) { [weak self] (response, error) in
                printAndLog("getHint", "error: \(error?.localizedDescription ?? "no description")", "response: \(String(describing: response))", isError: error != nil)
                if let value = response as? String {
                    self?.commentLabel?.text = value
                }
            }
        }
    }
    
    
    fileprivate func subscibeForTimer() {
        
        SessionService.shared.currentTest.subscribeForTimer { [weak self] (seconds) in
            self?.timeLabel?.text = String(seconds: seconds)
            if seconds == 300 {
                _ = showAlert(title: "Внимание!", body: "Осталось 5 минут", needsOKAction: true, from: self)
            } else if seconds  <= 0 {
                
                self?.processCurrentAnswer(forced: true, completion: { [weak self] (_) in
                    guard let `self` = self else { return }
                    
                    _ = showAlert(title: "Время вышло!", body: nil, needsOKAction: true, from: self, completion: {
                            asyncAfter(300, {
                                UIApplication.shared.goToResults()
                                return
                            })
                    })
                })
            }
        }
    }
    
    fileprivate func checkIsCompleteAllQuestion() -> Bool {
        var taskGroupAnswerCount: [Int: Int] = [:] /* Id : Count */
        
        for q in self.currentTest.questions {
            if q is QuestionInfo {
                continue
            }
            if q.taskGroupId == nil {
                if q.isAnswered == false {
                    return false
                }
            } else {
                //группа
                if q.isAnswered {
                    var count = taskGroupAnswerCount[q.taskGroupId!] ?? 0
                    count += 1
                    taskGroupAnswerCount[q.taskGroupId!] = count
                }
            }
            
        }
        
        //Проверить ответы в группе
        if taskGroupAnswerCount.count != self.currentTest.taskGroups.count {
            return false
        }
        for taskGroup in self.currentTest.taskGroups {
            let taskGroupId = taskGroup.id
            let count = taskGroupAnswerCount[taskGroupId] ?? 0
            if count < taskGroup.numberMandatoryTask {
                return false
            }
        }
        
        return true
        
        //old
        //return self.currentTest.answeredQuestionsCount == (self.currentTest.questions.count - getExpertQustionForUser())
    }
    
    fileprivate func checkForCompletion() {
        if self.checkIsCompleteAllQuestion(),
            self.notifiedAboutCompletion == false {
            self.notifiedAboutCompletion = true
            _ = showAlert(title: "Внимание!", body: "Вы ответили на все вопросы!\nПроверьте введенные вами ответы либо завершите диагностику.", needsOKAction: true)
        }
    }
    
    fileprivate func textForFinishingAlert() -> String {
        return self.checkIsCompleteAllQuestion() ?
            "Время выполнения проверочной работы не истекло!\nЕсли вы хотите завершить выполнение работы, нажмите \"Завершить\".\nЕсли вы хотите  проверить введенные ответы или изменить их, нажмите \"Отменить\"" : "Вы ввели ответы не на все задания!\n\nЕсли вы хотите завершить выполнение работы, нажмите \"Завершить\".\nЕсли вы хотите продолжить ввод ответов, нажмите \"Отменить\"."
    }
    
    
    func getApplicationDocumentsDirectory() -> URL? {
        var docPathURL =
            FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory,
                                     in: FileManager.SearchPathDomainMask.userDomainMask).last
        docPathURL = docPathURL?.appendingPathComponent("Storage")
        do {
            //NSLog("%@", docPathURL!.path!)
            try FileManager.default.createDirectory(at: docPathURL!, withIntermediateDirectories: true, attributes: nil)
        } catch {
            
        }
        return docPathURL
    }
    
    @IBAction func actionVoiceInputButton() {
        self.view.endEditing(true)
        
        self.voiceTextLabel.text = ""
        
        self.voiceBGView.center = self.view.center
        self.view.addSubview(self.voiceBGView)
        
        let fileUrl = self.getApplicationDocumentsDirectory()!.appendingPathComponent("monolog.m4a")

        let params = [
            AVSampleRateKey : NSNumber(value: Float(44100.0)),
            AVFormatIDKey: NSNumber(value: Int32(kAudioFormatMPEG4AAC)),
            AVNumberOfChannelsKey: NSNumber(value: 1),
            AVEncoderAudioQualityKey: NSNumber(value: Int32(AVAudioQuality.min.rawValue))
        ]
        
        
        self.recorder = try? AVAudioRecorder(url: fileUrl, settings: params)
        
        self.displaylink = CADisplayLink(target: self, selector: #selector(updateMeters))
        self.displaylink!.add(to: RunLoop.current, forMode: RunLoop.Mode.common)
        
        
        recorder!.prepareToRecord()
        recorder!.isMeteringEnabled = true
        recorder!.record()
        
        self.requestTranscribePermissions {
           //PK
            /*
           let audioSession = AVAudioSession.sharedInstance()
           try? audioSession.setCategory(AVAudioSession.Category.playAndRecord)
           try? audioSession.setMode(AVAudioSession.Mode.measurement)
           try? audioSession.setActive(true, options: .notifyOthersOnDeactivation)
          */
            self.audioEngine.prepare()
            try? self.audioEngine.start()
 
            self.recognitionTask = self.speechRecognizer?.recognitionTask(with: self.request) {[unowned self] (result, error) in
                if error != nil {
                    print(error?.localizedDescription ?? "-")
                } else if let transcription = result?.bestTranscription {
                    self.voiceTextLabel.text = transcription.formattedString
                }
            }
        }

    }
    
    @IBAction func actionApplyVoiceInputButton() {
        stopRecording()
        self.voiceBGView.removeFromSuperview()
        
        
        guard (currentTest?.questions.count ?? 0) > selectedQuestionIndex - 1 else { return }
        let question = currentTest.questions[selectedQuestionIndex - 1]
        
        webSuperview.webView?.setAnswerVoiceInput(for: question, text: self.voiceTextLabel.text ?? "", completion: { (response, error) in
            printAndLog("setAnswerVoiceInput", "error: \(error?.localizedDescription ?? "no description")", "response: \(String(describing: response))", isError: error != nil)
        })
    }
    
    @IBAction func actionCancelVoiceInputButton() {
        stopRecording()
        self.voiceBGView.removeFromSuperview()
    }
    
    fileprivate func stopRecording() {
        self.displaylink?.invalidate()
        self.displaylink = nil
        
        self.audioEngine.stop()
        self.request.endAudio()
        self.recognitionTask?.cancel()
        self.recognitionTask = nil
        
        self.recorder?.stop()
        self.recorder = nil
        
        let fileUrl = self.getApplicationDocumentsDirectory()!.appendingPathComponent("monolog.m4a")
        try? FileManager.default.removeItem(at: fileUrl)
        
        /*
        let audioSession = AVAudioSession.sharedInstance()
        try? audioSession.setCategory(AVAudioSession.Category.playback)
        try? audioSession.setMode(AVAudioSession.Mode.default)
        */
    }
    
    @objc func updateMeters() {
        var normalizedValue: CGFloat = 0.0
        if recorder != nil && recorder!.isRecording {
            self.recorder!.updateMeters()
            normalizedValue = self._normalizedPowerLevelFromDecibels(decibels: self.recorder!.averagePower(forChannel: 0))
        } 
        self.waveformView.update(withLevel: normalizedValue)
    }
    
    private func _normalizedPowerLevelFromDecibels(decibels: Float) -> CGFloat {
        if decibels < -60.0 || decibels == 0.0 {
            return 0.0
        }
        return CGFloat(powf((powf(10.0, 0.05 * decibels) - powf(10.0, 0.05 * -60.0)) * (1.0 / (1.0 - powf(10.0, 0.05 * -60.0))), 1.0 / 2.0))
    }

    func requestTranscribePermissions(complete: @escaping () -> Void ) {
        SFSpeechRecognizer.requestAuthorization { [unowned self] authStatus in
            DispatchQueue.main.async {
                if authStatus == .authorized {
                    print("Good to go!")
                    complete()
                } else {
                    print("Transcription permission was declined.")
                    let alert = UIAlertController(title: "Внимание", message: "Вы наложили системный запрет на использование функционала диктовки",
                                                  preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.destructive, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    private func _actionHandwriting() {
        self.view.endEditing(true)
        
        //disable Myscript
        /*
        self.myscriptBGView.frame = self.view.bounds
        self.view.addSubview(self.myscriptBGView)
        
        let packageName = "New"
        
        do {
            if let package = try createPackage(packageName: packageName) {
                try self.editorViewController.editor.part = package.getPartAt(0)
            }
        } catch {
            print("Error while creating package : " + error.localizedDescription)
        }
         */
    }
    
    @IBAction func actionHandwriting() {
        
        if actionHandwritingAccept == nil || actionHandwritingAccept! == false {
            let alert = UIAlertController(title: "Внимание!", message: "Вы решили воспользоваться функцией распознавания рукописного ввода. Вы точно хотите воспользоваться данной функцией?", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Нет", style: UIAlertAction.Style.destructive, handler: nil))
            alert.addAction(UIAlertAction(title: "Да", style: UIAlertAction.Style.default, handler: { (action) in
                self.actionHandwritingAccept = true
                
                self._actionHandwriting()
            }))
            self.present(alert, animated: true, completion: nil)
        } else if actionHandwritingAccept! == true {
            self._actionHandwriting()
        }
    }
    
    /* // disable Myscript
    func openPackage(packageName: String) throws -> IINKContentPackage? {
        // Create a new content package with name
        var resultPackage: IINKContentPackage?
        let fullPath = FileManager.default.pathForFile(inDocumentDirectory: packageName) + ".iink"
        if let engine = self.engine {
            resultPackage = try engine.openPackage(fullPath.decomposedStringWithCanonicalMapping)
        }
        return resultPackage
    }
    
    func createPackage(packageName: String) throws -> IINKContentPackage? {
        // Create a new content package with name
        var resultPackage: IINKContentPackage?
        let fullPath = FileManager.default.pathForFile(inDocumentDirectory: packageName) + ".iink"
        if let engine = self.engine {
            resultPackage = try engine.createPackage(fullPath.decomposedStringWithCanonicalMapping)
            
            // Add a blank page type Text Document
            if let part = try resultPackage?.createPart("Text Document") /* Options are : "Diagram", "Drawing", "Math", "Text Document", "Text" */ {
                //self.title = "Type: " + part.type
            }
        }
        return resultPackage
    }
    */
    
    @IBAction func clearButtonWasTouchedUpInside(_ sender: Any) {
        editorViewController.editor.clear()
    }
    
    @IBAction func undoButtonWasTouchedUpInside(_ sender: Any) {
        editorViewController.editor.undo()
    }
    
    @IBAction func redoButtonWasTouchedUpInside(_ sender: Any) {
        editorViewController.editor.redo()
    }
    
    @IBAction func inputTypeSegmentedControlValueChanged(_ sender: UISegmentedControl) {
        guard let inputMode = InputMode(rawValue: UInt(sender.selectedSegmentIndex)) else {
            return
        }
        self.editorViewController.inputMode = inputMode
    }
    
    @IBAction func cancelHandWriteingButtonWasTouchedUpInside(_ sender: Any) {
        editorViewController.editor.part = nil
        editorViewController.editor.clear()
        self.myscriptBGView.removeFromSuperview()
    }
    
    @IBAction func exportButtonWasTouchedUpInside(_ sender: Any) {
        do {
            let text = try editorViewController.editor.export_(nil, mimeType: IINKMimeType.text)
            
             guard (currentTest?.questions.count ?? 0) > selectedQuestionIndex - 1 else { return }
             let question = currentTest.questions[selectedQuestionIndex - 1]
             
             webSuperview.webView?.setAnswerHandwriting(for: question, text: text, completion: { (response, error) in
                printAndLog("setAnswerHandwriting", "error: \(error?.localizedDescription ?? "no description")", "response: \(String(describing: response))", isError: error != nil)
             })
            
        } catch {
            print("Error while converting : " + error.localizedDescription)
        }
        editorViewController.editor.part = nil
        editorViewController.editor.clear()
        self.myscriptBGView.removeFromSuperview()
    }
    
    @IBAction func actionPDFMaterials() {
        if SessionService.shared.currentExamWork!.materials.count > 1 {
            let alert = UIAlertController(title: "Файл", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
            for item in SessionService.shared.currentExamWork!.materials {
                alert.addAction(UIAlertAction(title: item.name, style: UIAlertAction.Style.default, handler: { (action) in
                    self.showPDF(item)
                }))
            }
            alert.addAction(UIAlertAction(title: "Отмена", style: UIAlertAction.Style.destructive, handler: nil))
            alert.modalPresentationStyle = .popover
            
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = self.view //to set the source of your alert
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
                popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
            }
            
            self.present(alert, animated: true, completion: nil)
        } else if SessionService.shared.currentExamWork!.materials.count == 1 {
            showPDF(SessionService.shared.currentExamWork!.materials[0])
        }
    }
    
    private func showPDF(_ meterial: Material) {
        let vc = UIViewController()
        let webView = WKWebView()
        webView.frame = self.view.bounds
        vc.view.addSubview(webView)

        vc.title = meterial.name
       
        let anotherButton = UIBarButtonItem(title: "Закрыть", style: UIBarButtonItem.Style.plain, target: self, action: #selector(closePDF))
        vc.navigationItem.rightBarButtonItem = anotherButton

        webView.load(URLRequest(url: URL(string: DiagnosticsShared.instance.commonBaseUrl + "/api/files/\(meterial.fileId)")!))
        
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .fullScreen
        self.present(nav, animated: true, completion: nil)
        
    }
    
    @objc func closePDF() {
        self.dismiss(animated: true, completion: nil)
    }
    
}

//
//  JSRequests.swift
//  Diagnostics
//
//  Created by Константин on 30/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation

enum JSRequests: String {
    
    static let mobileHelper = "window.mobileHelper."
    static let mobileHelperRaw = "mobileHelper."
    
    ///Pass Answer (from getAnswer)
    case isEmptyAnswer = "isEmptyAnswer(%@);"
    ///Pass Answer (from getAnswer)
    case setAnswer = "Answer_Set(%@);"
    case setAnswerWithShuffleParam = "Answer_Set(%@, {shuffleParam:%@});"
    ///Pass fontsize delta and direction(1/-1)
    case ElementsZoom = "ElementsZoom(%@,%@);"
    ///Pass width and height
    case resizeWiris = "ResizeWiris(%@,%@);"
    case getAnswer = "Answer_Get();"
    case getIsAnswerBlank = "IsAnswerBlank();"
    
    case disableSelection = "Results_disableSelection();"
    case updateAppearance = "Results_updateAppearance();"
    case exitMessage = "ExitMessage();"
    ///Pass js params as string
    case applyRules = "Answer_setCheckRulesData({ %@ });"
    ///Hint
    case getHint = "GeHintText();"
    
    //
    //case initInstruments = "Instruments.init(Math.pow(2, 20)|Math.pow(2, 21));"
    case setAnswerVoiceInput = "Instruments.mobileVoiceInput.setLastAnswerText('%@')"
    case setAnswerHandwriting = "Instruments.mobileHandwriting.setLastAnswerText(%@)"
}

enum JSLocalRequests: String {
    case applyCurrentRules = "applyCurrentRules(%@,%@,%@,%@);"
    case addMobileHelper = "addMobileHelper(%@);"
    case setOnloadHandler = "setOnloadHandler(%@);"
    case setOnErrorHandler = "setOnErrorHandler(%@);"
    ///id,link
    case setIframeSrc = "setIframeSrc(%@,%@);"
    case hideIframes = "hideIframes();"
    case showIframe = "showIframe(%@);"
    case getAllFrames = "getAllFrames();";
    ///id, isVisible
    case getIframe = "getIframe(%@,%@);"
    
    ///src + array of ids in string
    case loadIframesByJS = "loadFrames(%@,%@);"
}

//
//  Answer.swift
//  Diagnostics
//
//  Created by Константин on 19.02.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation

 class Answer {
    
    var number: Int?
    var orderNumber: Int?
    var text: String?
    var type: String?
    var details: Any?
    var mark: Int?
    
    private var responseDict: NSDictionary!
    
    var isValid: Bool {
        get {
            return (text != nil && !text!.isEmpty) || details != nil
        }
    }
    
    init(with dict: NSDictionary) {
        responseDict = dict
        number = dict["Number"] as? Int
        orderNumber = dict["OrderNumber"] as? Int
        text = dict["Text"] as? String
        type = dict["Type"] as? String
        details = dict["Details"]
        mark = dict["Mark"] as? Int
    }
}

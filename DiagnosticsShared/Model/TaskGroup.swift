//
//  TaskGroup.swift
//  DiagnosticsShared
//
//  Created by Pavel Kurta on 21.12.2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation

class TaskGroup {
    
    var id: Int = 0
    var numberMandatoryTask: Int = 0
    var description: String?
    var name: String?
    var informationTextId: Int?
    var code: String?
    
    init(_ jsonDict: NSDictionary) {
        self.id = jsonDict["Id"] as? Int ?? 0
        self.numberMandatoryTask = jsonDict["NumberMandatoryTask"] as? Int ?? 0
        self.description = jsonDict["Description"] as? String
        self.name = jsonDict["Name"] as? String
        self.informationTextId = jsonDict["InformationTextId"] as? Int
        self.code = jsonDict["Code"] as? String
    }
    
}

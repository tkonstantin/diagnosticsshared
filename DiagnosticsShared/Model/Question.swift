//
//  Question.swift
//  Diagnostics
//
//  Created by Константин on 06.02.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import UIKit

 class Question {
    
    var id: String = ""
    var title = ""
    var subtitle = ""
    var isEmpty = true
    var answers: [Answer] {
        get {
            return (cachedAnswer?["Answers"] as? [NSDictionary] ?? []).map({Answer(with: $0)})
        }
    }
    var isAnswered: Bool {
        get  {
            return !isEmpty
            /*
            if !isEmpty {
                return answers.filter({$0.isValid}).count != 0
            } else {
                return !isEmpty
            }*/
        }
    }
    var html: String? = ""
    var link: String {
        get {
            return Server.shared.htmlTaskURL(with: self.id)
        }
    }
    var numberInKim: Int {
        get {
            return SessionService.shared.currentExamWork?.numbersInKim[self.id] ?? 0
        }
    }
    
    var cachedAnswer: NSDictionary?
    var cachedAnswerDisplayText: String? {
        get {
            guard let first = answers.first else { return nil }
            switch first.type {
            case "11", "9", "4":
                return "Ответ предоставлен"
            case "1", "2":
                if first.mark != nil && first.mark != 0 {
                    return first.text
                } else if let number = first.orderNumber ?? first.number {
                    return String(number)
                }
                
                return nil
            default:
                return nil
                
            }
        }
    }
    
    var taskGroupId: Int?
    var numberInGroup: String?
    
    init(html h: String?=nil, id _id: String) {
        self.id = _id
        self.html = h//.correctedHtml()
        
    }
    
    var hint: String? {
        get {
            return self.html?.getDiv(with: "hint")?.getStringValue()
        }
    }
    
    func clear() {
        self.isEmpty = true
        self.cachedAnswer = nil
    }
}

class QuestionInfo: Question {
    
    var taskGroup: TaskGroup?
    
    init(taskGroup: TaskGroup, id _id: String) {
        super.init(id: _id)
        self.taskGroup = taskGroup
    }
    
}


public extension String {
    
    static let allowedCharacters = "0123456789йцукенгшщзхъёэждлорпавыфячсмитьбюЙЦУКЕНГШЩЗХЪЁЭЖДЛОРПАВЫФЯЧСМИТЬБЮ"
    static let htmlConversionCharacters = "0123456789йцукенгшщзхъёэждлорпавыфячсмитьбюЙЦУКЕНГШЩЗХЪЁЭЖДЛОРПАВЫФЯЧСМИТЬБЮ,.-!?:   "
    
    
    public func htmlToString() -> String {
        return self.filter({String.htmlConversionCharacters.contains($0)})
    }
    
    fileprivate func getDiv(with id: String) -> String? {
        var originalString = self
        while let range = originalString.range(of: "<div") {
            originalString = String(originalString[range.lowerBound...])
            if let end = originalString.range(of: "</div>") {
                let divString = String(originalString[..<end.upperBound])
                if divString.contains("id=\"\(id)\"") {
                    return divString
                } else {
                    originalString = String(originalString[end.upperBound...])
                }
            } else {
                return nil
            }
        }
        return nil
    }
    
   fileprivate func getStringValue() -> String? {
        if let brackedEnd = self.range(of: ">") {
            let subString = self[brackedEnd.upperBound...]
            
            if let bracketBegin = subString.range(of: "<") {
                return String(subString[..<bracketBegin.lowerBound])
                
            }
        }
        return nil
    }

}




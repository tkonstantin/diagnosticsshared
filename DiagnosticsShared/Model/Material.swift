//
//  Material.swift
//  DiagnosticsShared
//
//  Created by Pavel Kurta on 23/09/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation

class Material {
    
    var fileId: Int = 0
    var id: Int = 0
    var name: String = ""
    
    init(with json: NSDictionary) {
        fileId = (json["FileId"] as? Int) ?? 0
        id = (json["Id"] as? Int) ?? 0
        name = (json["Name"] as? String) ?? ""
    }
    
}

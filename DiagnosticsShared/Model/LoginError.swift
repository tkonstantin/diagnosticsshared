//
//  LoginError.swift
//  Diagnostics
//
//  Created by Константин on 30/11/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation

public enum LoginError {
    
    case invalid_code
    case no_network
    case unknown
    
    public var displayString: String {
        get {
            switch self {
            case .invalid_code: return "Некорректный код диагностики"
            case .no_network: return "Отсутствует подключение к сети"
            case .unknown: return "Неизвестная ошибка"
            }
        }
    }
}

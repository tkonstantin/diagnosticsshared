//
//  Variant.swift
//  Diagnostics
//
//  Created by Константин on 13.02.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation


class Variant {
    var ExamWorkId = ""
    var ResultCode = ""
    var ResultMessage = ""
    var VariantId = ""
    
    init(with json: NSDictionary) {
        ExamWorkId = json["ExamWorkId"] as? String ?? String(json["ExamWorkId"] as? Int ?? 0)
        ResultCode = json["ResultCode"] as? String ?? ""
        ResultMessage = json["ResultMessage"] as? String ?? ""
        VariantId = json["VariantId"] as? String ?? String(json["VariantId"] as? Int ?? 0)

    }
}

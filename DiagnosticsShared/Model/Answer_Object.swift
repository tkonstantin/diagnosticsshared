//
//  Answer_Object.swift
//  Diagnostics
//
//  Created by Константин on 18/02/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation


class Answer_Object {
 
    var TaskType: _TaskType!
    
    
    var displayText: String {
        get {
            let placeholder: String = {
                let texts = answers.compactMap({ $0.text })
                if texts.count != 0 {
                    return texts.map({ NSAttributedString(html: $0).string.htmlToString() }).joined(separator: "; ")
                }
                return "Нет ответа"
            }()
            
            guard let code = TaskType?.Code else { return placeholder }
            switch code {
            case 9, 11:
                return answers.count == 0 ? placeholder : "Ответ предоставлен"
                
            case 1, 2:
                if case let marked = answers.filter({ $0.mark != nil && $0.mark != 0 }), marked.count != 0 {
                    return marked.compactMap({ String($0.mark!) }).joined(separator: ", ")
                } else if case let numbered = answers.filter({ $0.orderNumber != nil || $0.number != nil }), numbered.count != 0 {
                     return numbered.compactMap({ String($0.orderNumber ?? $0.number!) }).joined(separator: ", ")
                }
                return placeholder
                
            case 4:
                return answers.compactMap({ $0.text }).joined(separator: "; ")
            default:
                return placeholder
            }
        }
    }
    
    var answers: [Answer] = []
    
    class _TaskType: Decodable {
        var Code: Int!
        var Name: String!
        
    }
    
    init?(with json: NSDictionary!) {
        guard let json = json else { return nil }
        guard let type = json["TaskType"] as? NSDictionary, let typeData = try? JSONSerialization.data(withJSONObject: type, options: []) else { return nil }
        TaskType = try? JSONDecoder.init().decode(_TaskType.self, from: typeData)
        answers = (json["Answers"] as? [NSDictionary])?.compactMap({ Answer(with: $0) }) ?? []
    }
}



//
//  ExamWork.swift
//  Diagnostics
//
//  Created by Константин on 02.03.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import UIKit
import WebKit

///seconds
fileprivate(set) var globalTimeOffset: Double = 0

enum checkRules: String {
    case AllowSymbolsCode = "AllowSymbolsCode"
    case AnswerMinLength = "AnswerMinLength"
    case AnswerMaxLength = "AnswerMaxLength"
}


public class ExamWork {
    
    var id: String!
    var json: NSDictionary?
    var htmlIds: [String] = []
    var html_answer_ids: [String: String] = [:]
    var answersByHTMLId: [String: String] = [:]
    var numbersInKim: [String: Int] = [:]
    var rulesByHTMLId: [String: [checkRules: String?]] = [:]
    
    var isPublicValues: [String: Int] = [:]
    var isBlankValues: [String: Int] = [:]
    var isContainBlank = false
    var isHandMadeWorks: Set<String> = Set<String>()
    var taskTextHintHtmlIds: [String: Int] = [:]
    
    var resultsRouteURL: String = ""
    
    var isFinished = false
    
    var materials: [Material] = []
    
    init?(with i: String, json j: NSDictionary?) {
        id = i
        json = j
        
        if let viewType = (json?["TestSettings"] as? NSDictionary)?["resultsViewType"] as? String, !viewType.isEmpty {
            resultsRouteURL = viewType
        } else {
            resultsRouteURL = "preprof"
        }
        
        //PK
        let test = SessionService.shared.currentTest
        test.taskGroups = []
        test.questionsCount = 0
        test.questions = []
        
        if let taskGroups = json?["TaskGroups"] as? [NSDictionary] {
            for taskGroup in taskGroups {
                test.taskGroups.append(TaskGroup(taskGroup))
            }
        }

        if let answers = json?["Answers"] as? [NSDictionary] {
            if let jsonData = try? JSONSerialization.data(withJSONObject: answers, options: JSONSerialization.WritingOptions(rawValue: 0)) {
                test.answersJson = jsonData.base64EncodedString(options: .endLineWithLineFeed)
            }
            
            for answer in answers {
                let answer_id = answer["Id"] as? String ?? String(answer["Id"] as? Int ?? 0)
                let html_id = answer["TaskTextHtmlId"] as? String ?? String(answer["TaskTextHtmlId"] as? Int ?? 0)
                let number = answer["NumberInKim"] as? Int ?? Int(answer["NumberInKim"] as? String ?? "0") ?? 0
                let numberInGroup = answer["NumberInGroup"] as? String
                let taskGroupId = answer["TaskGroupId"] as? Int
                let isPublic = answer["IsPublic"] as? Int ?? 0
                let isBlank = answer["IsBlank"] as? Int ?? 0
                let taskTextHintHtmlId: Int? = answer["TaskTextHintHtmlId"] as? Int
                
                htmlIds.append(html_id)
                var rulesArr: [checkRules: String?] = [:]
                
                if let code = answer[checkRules.AllowSymbolsCode.rawValue] as? String {
                    rulesArr[checkRules.AllowSymbolsCode] = code
                } else if let code = answer[checkRules.AllowSymbolsCode.rawValue] as? Int {
                    rulesArr[checkRules.AllowSymbolsCode] = String(code)
                }
                
                if let min = answer[checkRules.AnswerMinLength.rawValue] as? String {
                    rulesArr[checkRules.AnswerMinLength] = min
                } else if let min = answer[checkRules.AnswerMinLength.rawValue] as? Int {
                    rulesArr[checkRules.AnswerMinLength] = String(min)
                }
                
                if let max = answer[checkRules.AnswerMaxLength.rawValue] as? String {
                    rulesArr[checkRules.AnswerMaxLength] = max
                } else if let max = answer[checkRules.AnswerMaxLength.rawValue] as? Int {
                    rulesArr[checkRules.AnswerMaxLength] = String(max)
                }
                
                rulesByHTMLId[html_id] = rulesArr
                
                if let text = answer["AnswerText"] as? String {
                    answersByHTMLId[html_id] = text
                }
                if let shuffle = answer["ShuffleParam"] as? String {
                     SessionService.shared.answerShuffleParams[html_id] = shuffle
                } else if let shuffle = answer["ShuffleParam"] as? Int {
                    SessionService.shared.answerShuffleParams[html_id] = String(shuffle)
                }
                
                SessionService.shared.knownAnswers = answersByHTMLId
                
                
                for id in htmlIds {
                    if !test.questions.contains(where: { $0.id == id }) {
                        if taskGroupId != nil &&
                            !test.questions.contains(where: { $0.taskGroupId == taskGroupId! }) {
                            
                            if let taskGroup = test.taskGroups.first(where: { $0.id == taskGroupId! }) {
                                test.questions.append(QuestionInfo(taskGroup: taskGroup, id: id))
                            }
                        }

                        let question = Question(id: id)
                        question.taskGroupId = taskGroupId
                        question.numberInGroup = numberInGroup
                        
                        test.questions.append(question)
                        test.questionsCount += 1
                    }
                }
                
                for question in test.questions {

                    if let answer = answersByHTMLId[question.id], let answerData = answer.data(using: .utf8) {
                        question.cachedAnswer = (try? JSONSerialization.jsonObject(with: answerData, options: [])) as? NSDictionary
                    }
                }
                html_answer_ids[html_id] = answer_id
                numbersInKim[html_id] = number
                isPublicValues[html_id] = isPublic
                isBlankValues[html_id] = isBlank
                
                if taskTextHintHtmlId != nil {
                    taskTextHintHtmlIds[html_id] = taskTextHintHtmlId!
                }
                
                if isBlank == 1 {
                    isContainBlank = true
                }
            }
        } else {
            return nil
        }
        
        PauseManager.shared.duration = (json?["PauseDuration"] as? Double ?? 0)*60
        PauseManager.shared.frequency = (json?["PauseFrequency"] as? Double ?? 0)*60
        PauseManager.shared.isEnabled = json?["PauseEnabled"] as? Bool ?? false
        
        
        
        if let dateString = json?["TimeTill"] as? String {
            let formatter = DateFormatter.init()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            if let date = formatter.date(from: dateString) {
                SessionService.shared.currentTest.setEndDate(date)
                
                if date < Date(timeIntervalSince1970: Date().timeIntervalSince1970 - globalTimeOffset) {
                    _ = showAlert(title: "Ошибка", body: "Время данной диагностики истекло") {
                        WebSuperview.resetOnLogoutNotification()
                        ConnectionService.shared.logout()
                        SessionService.shared.logout()
                        DiagnosticsShared.instance.delegate?.diagnostics_completed()
                    }
                }
            }
        } else {
            return nil
        }
        if let currentDateString = json?["CurrentTime"] as? String {
            let formatter = DateFormatter.init()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            if let date = formatter.date(from: currentDateString) {
                globalTimeOffset = Date().timeIntervalSince1970 - date.timeIntervalSince1970
            }
        }
        
        //PK
        self.materials = []
        if let materialsJson = json?["Materials"] as? [NSDictionary] {
            for item in materialsJson {
                self.materials.append(Material(with: item))
            }
        }
    }
    
    public func getStatus() -> Int {
        return isHandMadeWorks.count > 0 ? 5 : 10
    }
    
}


extension UIApplication {
    
    class func presentOnTop(vc: UIViewController, animated: Bool, completion: (()->Void)?=nil) {
        let top = UIApplication.shared.topController()
        if top is UIAlertController {
            top?.dismiss(animated: false, completion: {
                UIApplication.shared.topController()?.present(vc, animated: animated, completion: completion)
            })
        } else {
            UIApplication.shared.topController()?.present(vc, animated: animated, completion: completion)
        }
    }
    
    private func topController(for vc: UIViewController?=UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let modal = vc?.presentedViewController {
            return topController(for: modal)
        } else {
            return vc
        }
    }
}

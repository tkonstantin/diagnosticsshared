//
//  Test.swift
//  Diagnostics
//
//  Created by Константин on 06.02.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

enum VoiceHandwritingAccessType {
    case none
    case voice
    case handwriting
    case all
}

public class Test {
    
    private var isStarted = false {
        didSet {
            if isStarted {
                PauseManager.shared.testStarted()
                if timer == nil || !timer.isValid {
                    timer = Timer.init(timeInterval: 1, target: self, selector: #selector(self.decreaseTime), userInfo: nil, repeats: true)
                    RunLoop.current.add(timer, forMode: .common)
                }
            } else {
                PauseManager.shared.testEnded()
                timer?.invalidate()
                timer = nil
            }
        }
    }
    
    public var ParallelName = ""
    public var SubjectTestName = ""
    public var SubjectTestId = ""
    public var TestGroupName = ""
    public var HtmlText = ""
    public var htmlInstructionPages: [String] = []
    public var resultHTML = ""
    public var TimeToStart: Date? = nil
    var results: TestResult?
 
    var initializeVoiceAndHandwritingInfo: [String: VoiceHandwritingAccessType] = [:]
    
    init(with json: NSDictionary) {
        ParallelName = json["ParallelName"] as? String ?? ""
        SubjectTestName = json["SubjectTestName"] as? String ?? ""
        SubjectTestId = json["SubjectTestId"] as? String ?? String(json["SubjectTestId"] as? Int ?? 0)
        TestGroupName = json["TestGroupName"] as? String ?? ""
        HtmlText = json["HtmlText"] as? String ?? ""
        htmlInstructionPages = Array(((json["HtmlText"] as? String ?? "").components(separatedBy: "</h1>")).dropFirst())
        
        if let timeToStartString = json["TimeToStart"] as? String, !timeToStartString.isEmpty {
            let formatter = DateFormatter.init()
            formatter.dateFormat = "HH:mm:ss"
            if let secondsDate = formatter.date(from: timeToStartString) {
                let rawSeconds = secondsDate.timeIntervalSince1970 - secondsDate.dateToZero.timeIntervalSince1970
                TimeToStart = Date(timeIntervalSince1970: rawSeconds + Date().timeIntervalSince1970)
            }
        }
        
    }
    
   public var isConfirmed = false
    
    var answersJson: String?
    
    var taskGroups: [TaskGroup] = []
    
    
    var _questions: [Question] = []
    var questions: [Question] {
        get {
            return _questions.sorted(by: {$0.numberInKim < $1.numberInKim})
        } set {
            _questions = newValue
        }
    }
    
    var questionsCount: Int = 0
    
    var answeredQuestionsCount: Int {
        get {
           return questions.filter({$0.isAnswered}).count
        }
    }
    
    private(set) var endDate: Date!
    
    func setStarted() {
        self.isStarted = true
    }
    
    func setEnded() {
        self.isStarted = false
    }
    
    func setEndDate(_ end: Date) {
        self.endDate = end
    }
    
    var secondsLeft: Int {
        get {
            guard endDate != nil else { return 0 }
            return max(0, Int(endDate.timeIntervalSince1970 - (Date().timeIntervalSince1970 - globalTimeOffset)) )
        }
    }

    func setPaused() {
        timer?.invalidate()
    }
    
    @objc private func decreaseTime() {
        if secondsLeft <= 0 {
            timer.invalidate()
            timer = nil
        }
        timerSubscription?(secondsLeft)
    }
    
    private var timer: Timer!
    private var timerSubscription: ((Int)->Void)?
    
    func subscribeForTimer(_ seconds: @escaping (Int)->Void) {
        seconds(secondsLeft)
        timerSubscription = seconds
    }
}



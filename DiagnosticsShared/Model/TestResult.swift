//
//  TestResult.swift
//  Diagnostics
//
//  Created by Константин on 18/02/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation

class TestResult {
    
    var maxMark = 0
    var mark = 0
    
    var TestGroupName = ""
    var SubjectName = ""
    
    var subtitle: String {
        get {
            return TestGroupName + ". " + SubjectName
        }
    }
    
    var tasks: [ResultTask] = []
    
    init(with json: NSDictionary) {
        if let markInfo = json["MarkInfo"] as? NSDictionary {
            
            let grad_Max = markInfo["TestMaxMark"] as? Int
            let spec_max = markInfo["MaxMark"] as? Int
            maxMark = [grad_Max, spec_max].compactMap({$0}).first(where: {$0 != 0}) ?? (grad_Max ?? 0)
            
            let grad_res = markInfo["TestMark"] as? Int
            let spec_res = markInfo["PrimaryMark"] as? Int
            mark = [grad_res, spec_res].compactMap({$0}).first(where: {$0 != 0}) ?? (grad_res ?? 0)
        }
        
        if let tasksArr = json["Tasks"] as? [NSDictionary] {
            for task in tasksArr {
                tasks.append(ResultTask(with: task))
            }
        }
        
        if let info = json["Info"] as? NSDictionary {
            TestGroupName = info["TestGroupName"] as? String ?? ""
            SubjectName = info["SubjectName"] as? String ?? ""
        }
        
    }
}

class ResultTask {
    var number = 1
    var maxScore = 0
    var currentScore = 0
    var hasAnswer = false
    var answerResult: String? {
        get {
            if let questionID = SessionService.shared.currentExamWork?.numbersInKim.first(where: {$0.value == number})?.key {
                if let data = SessionService.shared.knownAnswers[questionID]?.data(using: .utf8), let dict = try? JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? NSDictionary {
                    if let answer = Answer_Object(with: dict) {
                        return answer.displayText
                    }
                }
            }
            return "Нет ответа"
        }
    }
    
    init(with json: NSDictionary) {
        number = json["NumberInKim"] as? Int ?? 0
        maxScore = json["MaxBall"]  as? Int ?? 0
        currentScore = json["TestMark"]  as? Int ?? json["PrimaryBall"]  as? Int ?? 0
        hasAnswer = json["HasAnswer"] as? Bool ?? false
    }
}

extension NSAttributedString {
    
    convenience init(html: String) {
        let defaultValue = NSAttributedString(string: html)
        guard let data = html.data(using: .utf8, allowLossyConversion: true) else {
            self.init(attributedString: defaultValue)
            return
        }
        let attrStr = try? NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue, NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        self.init(attributedString: attrStr ?? defaultValue)
    }
}

extension String {
    
    static func from(html: String) -> String? {
        guard let data = html.data(using: .utf8, allowLossyConversion: true) else {
            return nil
        }
        return String(data: data, encoding: .utf8)
    }
}

//
//  User.swift
//  Diagnostics
//
//  Created by Константин on 12.02.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation


public class User {
    
    enum fields {
        static let Address = "Address"
        static let BirthYear = "BirthYear"
        static let DocumentFrom = "DocumentFrom"
        static let DocumentIssuedBy = "DocumentIssuedBy"
        static let DocumentNumber = "DocumentNumber"
        static let DocumentSeries = "DocumentSeries"
        static let DocumentTill = "DocumentTill"
        static let DocumentType = "DocumentType"
        static let Education = "Education"
        static let EmailContact = "EmailContact"
        static let Experience = "Experience"
        static let Fio = "Fio"
        static let GUID = "GUID"
        static let Id = "Id"
        static let IsAnonymous = "IsAnonymous"
        static let Login = "Login"
        static let Name = "Name"
        static let OrganizationName = "OrganizationName"
        static let ParallelCode = "ParallelCode"
        static let ParallelId = "ParallelId"
        static let ParallelName = "ParallelName"
        static let ParticipantId = "ParticipantId"
        static let ParticipantTestId = "ParticipantTestId"
        static let PhoneContact = "PhoneContact"
        static let Position = "Position"
        static let Provider = "Provider"
        static let RegionCode = "RegionCode"
        static let RegionName = "RegionName"
        static let Roles = "Roles"
        static let SecondName = "SecondName"
        static let Sex = "Sex"
        static let SexLabel = "SexLabel"
        static let ShortFio = "ShortFio"
        static let Surname = "Surname"
        static let UserUid = "UserUid"
    }
    
    
    public var Address = ""
    public var BirthYear = ""
    public var DocumentFrom = ""
    public var DocumentIssuedBy = ""
   public var DocumentNumber = ""
   public var DocumentSeries = ""
   public var DocumentTill = ""
   public var DocumentType = ""
   public var Education = ""
   public var EmailContact = ""
   public var Experience = ""
   public var Fio = ""
   public var GUID = ""
   public var Id = ""
   public var IsAnonymous = false
   public var Login = ""
   public var Name = ""
   public var OrganizationName = ""
   public var ParallelCode = "" //int
   public var ParallelId = "" //int
   public var ParallelName = ""
   public var ParticipantId = "" //int
   public var ParticipantTestId = "" //int
   public var PhoneContact = ""
   public var Position = ""
   public var Provider = ""
   public var RegionCode = "" //int
   public var RegionName = ""
   public var Roles = ""
   public var SecondName = ""
   public var Sex = false
   public var SexLabel = ""
   public var ShortFio = ""
   public var Surname = ""
   public var UserUid = ""
    
    
    
    public init(with json: NSDictionary) {
        Address = json[fields.Address] as? String ?? ""
        BirthYear = json[fields.BirthYear] as? String ?? ""
        DocumentFrom = json[fields.DocumentFrom] as? String ?? ""
        DocumentIssuedBy = json[fields.DocumentIssuedBy] as? String ?? ""
        DocumentNumber = json[fields.DocumentNumber] as? String ?? ""
        DocumentSeries = json[fields.DocumentSeries] as? String ?? ""
        DocumentTill = json[fields.DocumentTill] as? String ?? ""
        DocumentType = json[fields.DocumentType] as? String ?? ""
        Education = json[fields.Education] as? String ?? ""
        EmailContact = json[fields.EmailContact] as? String ?? ""
        Experience = json[fields.Experience] as? String ?? ""
        Fio = json[fields.Fio] as? String ?? ""
        GUID = json[fields.GUID] as? String ?? ""
        Id = json[fields.Id] as? String ?? String(json[fields.Id] as? Int ?? 0)
        IsAnonymous = json[fields.IsAnonymous] as? Bool ?? false
        Login = json[fields.Login] as? String ?? ""
        Name = json[fields.Name] as? String ?? ""
        OrganizationName = json[fields.OrganizationName] as? String ?? ""
        ParallelCode = json[fields.ParallelCode] as? String ?? String(json[fields.ParallelCode] as? Int ?? 0)
        ParallelId = json[fields.ParallelId] as? String ?? String(json[fields.ParallelId] as? Int ?? 0)
        ParallelName = json[fields.ParallelName] as? String ?? ""
        ParticipantId = json[fields.ParticipantId] as? String ?? String(json[fields.ParticipantId] as? Int ?? 0)
        ParticipantTestId = json[fields.ParticipantTestId] as? String ?? String(json[fields.ParticipantTestId] as? Int ?? 0)
        PhoneContact = json[fields.PhoneContact] as? String ?? ""
        Position = json[fields.Position] as? String ?? ""
        Provider = json[fields.Provider] as? String ?? ""
        RegionCode = json[fields.RegionCode] as? String ?? String(json[fields.RegionCode] as? Int ?? 0)
        RegionName = json[fields.RegionName] as? String ?? ""
        Roles = json[fields.Roles] as? String ?? ""
        SecondName = json[fields.SecondName] as? String ?? ""
        Sex = json[fields.Sex] as? Bool ?? false
        SexLabel = json[fields.SexLabel] as? String ?? ""
        ShortFio = json[fields.ShortFio] as? String ?? ""
        Surname = json[fields.Surname] as? String ?? ""
        UserUid = json[fields.UserUid] as? String ?? ""
    }



}
